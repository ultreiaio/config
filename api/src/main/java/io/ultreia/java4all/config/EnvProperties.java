package io.ultreia.java4all.config;

/*-
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Properties;

/**
 * Created on 19/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since
 */
public class EnvProperties extends Properties {

    private static final long serialVersionUID = 1L;

    public EnvProperties() {
    }

    public EnvProperties(Properties defaults) {
        super(defaults);
    }

    /**
     * Environnement variables can't contains dot (bash, csh, ...). Dots are
     * replaced by underscore (_) to find property if property is not find
     * with dot
     */
    @Override
    public synchronized Object get(Object key) {
        Object result = super.get(key);
        if (result == null && key instanceof String) {
            String skey = (String) key;
            skey = skey.replace(".", "_");
            result = super.get(skey);
        }
        return result;
    }

    /**
     * override to use get(key) and not super.get(key) as in initial implementation :(
     */
    @Override
    public String getProperty(String key) {
        Object oval = get(key);
        String sval = (oval instanceof String) ? (String) oval : null;
        return ((sval == null) && (defaults != null)) ? defaults.getProperty(key) : sval;

    }

}
