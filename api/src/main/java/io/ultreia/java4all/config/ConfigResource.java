package io.ultreia.java4all.config;

/*-
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Supplier;

/**
 * Created on 07/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.18
 */
public class ConfigResource {
    private static final Logger log = LogManager.getLogger(ConfigResource.class);
    private final String location;
    private URL url;

    public static InputStream openInternalStream(URL resource) {
        try {
            return Objects.requireNonNull(resource.openStream(), String.format("Could not find internal resource %s", resource));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not treat internal resource %s", resource), e);
        }
    }

    public static void loadFile(URL url, Path target, Supplier<ApplicationConfig> configSupplier) {
        if (Files.notExists(target)) {
            List<String> content = new LinkedList<>();
            ApplicationConfig applicationConfig = configSupplier.get();
            try {
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8))) {
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        content.add(applicationConfig.replaceRecursiveOptions(line));
                    }
                }
                Files.write(target, content);
            } catch (IOException e) {
                throw new IllegalStateException(String.format("Could not copy resource %s to %s", url, target), e);
            }
        }
    }

    public static Properties load(File file) throws IOException {
        URL url = file.toURI().toURL();
        return load(url);
    }

    public static Properties load(URL url) throws IOException {
        Properties result;
        try (InputStream in = openInternalStream(url)) {
            result = new Properties();
            result.load(in);
        }
        return result;
    }

    public ConfigResource(String location) {
        this.location = location;
    }

    public Properties load() throws IOException {
        return load(getUrl());
    }

    public URL getResource(String location) {
        URL resource = getClass().getResource(location);
        Objects.requireNonNull(resource, String.format("Could not find internal resource %s", location));
        try (InputStream in = openInternalStream(resource)) {
            log.debug("opening: " + in);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not treat internal resource %s", resource), e);
        }
        return resource;
    }

    public boolean copyResource(Path sharedFile, File file) {
        if (Files.exists(sharedFile)) {
            try {
                Files.createSymbolicLink(file.toPath(), sharedFile);
                return false;
            } catch (IOException e) {
                throw new IllegalStateException(String.format("Could not make symbolic link from %s to %s", sharedFile, file), e);
            }
        }
        copyResource(file);
        return true;
    }

    public void copyResource(File file) {
        try (InputStream in = ConfigResource.openInternalStream(getUrl())) {
            Files.copy(in, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not copy resource %s to %s", this, file), e);
        }
    }

    public List<String> readLines() {
        try (InputStreamReader in = new InputStreamReader(ConfigResource.openInternalStream(getUrl()),StandardCharsets.UTF_8)) {
            return IOUtils.readLines(in);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not read resource %s ", this), e);
        }
    }
    public URL getUrl() {
        if (url == null) {
            url = getResource(location);
        }
        return url;
    }

    public boolean exists(File directory) {
        File file = getFile(directory);
        return file.exists();
    }

    public File getFile(File directory) {
        return new File(directory, location.substring(1));
    }

}
