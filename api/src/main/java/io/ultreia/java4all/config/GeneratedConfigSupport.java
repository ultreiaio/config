package io.ultreia.java4all.config;

/*-
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ApplicationConfigInit;
import io.ultreia.java4all.config.spi.ConfigOptionDef;
import io.ultreia.java4all.util.Zips;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created on 07/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.17
 */
public abstract class GeneratedConfigSupport<O extends ConfigOptionDef> implements Supplier<ApplicationConfig> {

    private static final Logger log = LogManager.getLogger(GeneratedConfigSupport.class);

    protected final ApplicationConfig applicationConfig;

    protected GeneratedConfigSupport(String fileName) {
        this(ApplicationConfigInit.defaultInit().setConfigFileName(fileName));
    }

    protected GeneratedConfigSupport(ApplicationConfigInit init) {
        this.applicationConfig = createApplicationConfig(init);
        // Always try to fix default values of options of type File if on Windows OS
        this.applicationConfig.fixFileOptionsForWindows(options());
        this.applicationConfig.loadDefaultOptions(options());
    }

    protected ApplicationConfig createApplicationConfig(ApplicationConfigInit init) {
        return new ApplicationConfig(init);
    }

    protected abstract O[] options();

    @Override
    public final ApplicationConfig get() {
        return applicationConfig;
    }

    public void createDirectories(Path path, String errorMessage) {
        try {
            if (Files.notExists(path)) {
                Files.createDirectories(path);
            }
        } catch (IOException e) {
            throw new IllegalStateException(String.format(errorMessage, path), e);
        }
    }

    protected void setOption(String key, Object attrName) {
        applicationConfig.setOption(key, String.valueOf(attrName));
    }

    protected <T> void setOptionArray(String key, T... attrName) {
        StringBuilder value = new StringBuilder();
        for (T o : attrName) {
            value.append(",").append(o);
        }
        applicationConfig.setOption(key, value.length() == 0 ? "" : value.substring(1));
    }

    protected <T> void setOptionList(String key, List<T> attrName) {
        StringBuilder value = new StringBuilder();
        for (T o : attrName) {
            value.append(",").append(o);
        }
        applicationConfig.setOption(key, value.length() == 0 ? "" : value.substring(1));
    }

    public String getConfigurationDescription() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n=====================================================================================================================");
        builder.append("\n=== Configuration ===================================================================================================");
        builder.append("\n=====================================================================================================================");
        builder.append("\n=== Os.name     = ").append(get().getOsName());
        builder.append("\n=== Os.arch     = ").append(get().getOsArch());
        builder.append("\n=== Os.username = ").append(get().getUsername());
        builder.append("\n=== Os.encoding = ").append(get().getEncoding());
        for (O option : orderedByKey()) {
            Object optionValue = get().getOption(option.getKey());
            builder.append(String.format("\n=== %1$-50s = %2$s", option.getKey(), optionValue == null ? "Not defined" : optionValue));
        }
        builder.append("\n=====================================================================================================================");
        return builder.toString();
    }

    protected List<O> orderedByKey() {
        List<O> values = Arrays.asList(options());
        values.sort(Comparator.comparing(ConfigOptionDef::getKey));
        return Collections.unmodifiableList(values);
    }

    /**
     * Créer le répertoire si nécessaire à partir le l'option donnée.
     *
     * @param option l'option qui représentent le répertoire.
     * @return le fichier
     */
    public File createDirectory(O option) {
        File dir = get().getOptionAsFile(option.getKey());
        createDirectory(dir);
        return dir;
    }

    /**
     * Créer tous les répertoires parents nécessaires à partir de l'option sur répertoire ou fichier.
     *
     * @param option l'option qui représentent des répertoires ou fichiers.
     */
    public void createParentDirectory(O option) {
        File dir = get().getOptionAsFile(option.getKey()).getParentFile();
        createDirectory(dir);
    }

    public void unzipToDirectory(ConfigResource resource, O option, String message) throws IOException {
        File dir = get().getOptionAsFile(option.getKey());
        createDirectory(dir);
        if (log.isDebugEnabled()) {
            log.info(message);
        }
        try (InputStream inputStream = ConfigResource.openInternalStream(resource.getUrl())) {
            Zips.uncompress(inputStream, dir);
        }
    }

    /**
     * Créer un répertoire s'il n'existe pas.
     *
     * @param dir le répertoire à créer
     */
    protected void createDirectory(File dir) {
        if (dir.exists()) {
            log.info(String.format("Directory %s exists.", dir));
        } else {
            log.info(String.format("Will create directory: %s", dir));
            try {
                Files.createDirectories(dir.toPath());
            } catch (IOException e) {
                throw new IllegalStateException(String.format("Could not create directory: %s", dir));
            }
        }
    }
}
