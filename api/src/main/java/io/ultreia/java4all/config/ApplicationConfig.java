package io.ultreia.java4all.config;

/*
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.io.RuntimeStorage;
import io.ultreia.java4all.config.spi.ApplicationConfigInit;
import io.ultreia.java4all.config.spi.ApplicationConfigScope;
import io.ultreia.java4all.config.spi.ConfigActionDef;
import io.ultreia.java4all.config.spi.ConfigOptionDef;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.RecursiveProperties;
import io.ultreia.java4all.util.RecursivePropertiesResolver;
import io.ultreia.java4all.util.SortedProperties;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.io.WriterException;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.collections4.EnumerationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.converter.ConverterUtil;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Writer;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Application configuration.
 * <h3>A finir...</h3>
 * <ul>
 * <li>Ajout d'annotations sur les méthodes
 * pour preciser plus de chose pour les options (pattern, min/max, alias,
 * description, ...)
 * <li>Trouver un moyen de document les options et actions pour automatiquement
 * generer l'aide en ligne. Pour eviter de devoir maintenir une méthode
 * dans lequel est écrit l'aide en plus des options.
 * <li>Prise en compte du flag {@link #useOnlyAliases}
 * <li>Vu qu'en java on ne peut pas pointer une méthode mais seulement une classe
 * il y a un bout des actions qui sont des chaînes (nom de la méthode). Il faudrait
 * faire un plugin maven qui check que l'action existe bien durant la compilation.
 * Il est simple de le faire a l'execution mais c trop tard :(
 * <li>Ajouter de la documentation pour {@link #getOptionAsList(String)}
 * </ul>
 * <h3>Bonnes pratiques</h3>
 * <p>
 * TODO A revoir en introduisant le nouveau constructeur avec ApplicationConfigInit
 * Vous devez créer une factory pour créer les instances d'{@link ApplicationConfig} qui contiendra par exemple une méthode :
 * </p>
 * <pre>
 *
 *   public static ApplicationConfig getConfig(
 *           Properties props, String configFilename, String ... args) {
 *
 *       ApplicationConfig conf = new ApplicationConfig(
 *               MyAppConfigOption.class, MyAppConfigAction.class,
 *               props, configFilename);
 *
 *       try {
 *           conf.parse(args);
 *       } catch (ArgumentsParserException eee) {
 *           if (log.isErrorEnabled()) {
 *               log.error("Can't load app configuration", eee);
 *           }
 *       }
 *       return conf;
 *   }
 *
 * </pre>
 * <ul>
 * <li>MyAppConfigOption doit étendre {@link ConfigOptionDef} et décrire les options de la configuration de l'application.
 * <li>MyAppConfigAction doit étendre {@link ConfigActionDef} et décrire la liste des actions
 * et de leur alias disponible pour l'application.
 * </ul>
 * <h3>Lecture des fichiers de configuration</h3>
 * <p>
 * La lecture des fichiers de configuration se fait durant l'appel de la méthode
 * {@link #parse(String...)} en utilisant la valeur de qui doit être définit
 * dans les options avec pour clef {@link ApplicationConfig#CONFIG_FILE_NAME} pour
 * trouver les fichiers (voir Les options de configuration pour l'ordre de
 * chargement des fichiers)
 * </p>
 * <h3>La sauvegarde</h3>
 * La sauvegarde des options se fait via une des trois méthodes disponibles :
 * <ul>
 * <li> {@link #save(File, boolean, String...)} sauve les données dans le fichier demandé
 * <li> {@link #saveForSystem(String...)} sauvegarde les données dans /etc
 * <li> {@link #saveForUser(String...)} sauvegarde les données dans $HOME
 * </ul>
 * <p>
 * Lors de l'utilisation de la methode {@link #saveForSystem(String...)} ou
 * {@link #saveForUser(String...)} seules les options lues dans un fichier ou modifiées par
 * programmation ({@link #setOption(String, String)} seront sauvegardées. Par exemple les
 * options passees sur la ligne de commande ne seront pas sauvées.
 * </p>
 * <h3>Les options de configuration</h3>
 * <p>
 * Cette classe permet de lire les fichiers de configuration, utiliser les
 * variable d'environnement et de parser la ligne de commande. L'ordre de prise
 * en compte des informations trouvées est le suivant (le premier le plus
 * important) :
 * <ul>
 * <li>options ajoutées par programmation: {@link #setOption(String, String)}</li>
 * <li>ligne de commande</li>
 * <li>variable d'environnement de la JVM: java -Dkey=value</li>
 * <li>variable d'environnement; export key=value</li>
 * <li>fichier de configuration du repertoire courant: $user.dir/filename</li>
 * <li>fichier de configuration du repertoire home de l'utilisateur: $user.home/.filename</li>
 * <li>fichier de configuration du repertoire /etc: /etc/filename</li>
 * <li>fichier de configuration trouve dans le classpath: $CLASSPATH/filename</li>
 * <li>options ajoutées par programmation: {@link #setDefaultOption(String, String)}</li>
 * </ul>
 * <p>
 * Les options sur la ligne de commande sont de la forme:
 * <pre>
 * --option key value
 * --monOption key value1 value2
 * </pre>
 * <ul>
 * <li>--option key value: est la syntaxe par defaut
 * <li>--monOption key value1 value2: est la syntaxe si vous avez ajouter une
 * méthode setMonOption(key, value1, value2) sur votre classe de configuration
 * qui hérite de {@link ApplicationConfig}. Dans ce cas vous pouvez mettre les
 * arguments que vous souhaitez du moment qu'ils soient convertibles de la
 * representation String vers le type que vous avez mis.
 * </ul>
 * <h3>Les actions</h3>
 * <p>
 * Les actions ne peuvent etre que sur la ligne de commande. Elles sont de la
 * forme:
 * <pre>
 * --le.package.LaClass#laMethode arg1 arg2 arg3 ... argN
 * </pre>
 * <p>
 * Une action est donc défini par le chemin complet vers la méthode qui traitera
 * l'action. Cette méthode peut-être une méthode static ou non. Si la méthode
 * n'est pas static lors de l'instanciation de l'objet on essaie de passer en
 * paramètre du constructeur la classe de configuration utilisée pour permettre
 * a l'action d'avoir a sa disposition les options de configuration. Si aucun
 * constructeur avec comme seul paramètre une classe héritant de
 * {@link ApplicationConfig} n'existe alors le constructeur par défaut est
 * utilise (il doit être accessible). Toutes methodes d'actions faisant
 * parties d'un meme objet utiliseront la meme instance de cette objet lors
 * de leur execution.
 * </p>
 * <p>
 * Si la méthode utilise les arguments variants alors tous les arguments
 * jusqu'au prochain -- ou la fin de la ligne de commande sont utilises. Sinon
 * Le nombre exact d'argument nécessaire a la méthode sont utilises.
 * </p>
 * <p>
 * Les arguments sont automatiquement converti dans le bon type réclamé par la
 * methode.
 * </p>
 * <p>
 * Si l'on veut des arguments optionnels le seul moyen actuellement est
 * d'utiliser une méthode avec des arguments variants
 * </p>
 * <p>
 * Les actions ne sont pas execute mais seulement parsées. Pour les exécuter
 * il faut utiliser la méthode {@link #doAction(int)} qui prend en argument un numéro
 * de 'step' ou {@link #doAllAction()} qui fait les actions dans l'ordre de leur step.
 * Par défaut toutes les actions sont de niveau 0 et sont exécutées
 * dans l'ordre d'apparition sur la ligne de commande. Si l'on souhaite
 * distinguer les actions il est possible d'utiliser l'annotation
 * {@link ApplicationConfig.Action.Step} sur la methode qui fera l'action en
 * precisant une autre valeur que 0.
 * <pre>
 * doAction(0);
 * ... do something ...
 * doAction(1);
 * </pre>
 * <p>
 * dans cette exemple on fait un traitement entre l'execution des actions
 * de niveau 0 et les actions de niveau 1.
 * </p>
 * <h3>Les arguments non parsées</h3>
 * <p>
 * Tout ce qui n'est pas option ou action est considèré comme non parse et peut
 * etre recupere par la methode {@link #getUnparsed}. Si l'on souhaite forcer
 * la fin du parsing de la ligne de commande il est possible de mettre --.
 * Par exemple:
 * <pre>
 * monProg "mon arg" --option k1 v1 -- --option k2 v2 -- autre
 * </pre>
 * <p>
 * Dans cet exemple seule la premiere option sera considère comme une option.
 * On retrouvera dans {@code unparsed}: "mon arg", "--option", "k2", "v2", "--",
 * "autre"
 * </p>
 * <h3>Les alias</h3>
 * <p>
 * On voit qu'aussi bien pour les actions que pour les options, le nom de la
 * méthode doit être utilise. Pour éviter ceci il est possible de définir
 * des alias ce qui permet de creer des options courtes par exemple. Pour cela,
 * on utilise la méthode {@link #addAlias(String, String...)}.
 * <pre>
 * addAlias("-v", "--option", "verbose", "true");
 * addAlias("-o", "--option", "outputfile");
 * addAlias("-i", "--mon.package.MaClass#MaMethode", "import");
 * </pre>
 * En faite avant le parsing de la ligne de commande tous les alias trouves sont
 * automatiquement remplacer par leur correspondance. Il est donc possible
 * d'utiliser ce mécanisme pour autre chose par exemple:
 * <pre>
 * addAlias("cl", "Code Lutin");
 * addAlias("bp", "Benjamin POUSSIN);
 * </pre>
 * <p>Dans le premier exemple on simplifie une option de flags l'option -v n'attend
 * donc plus d'argument. Dans le second exemple on simplifie une option qui
 * attend encore un argument de type File. Enfin dans le troisième exemple
 * on simplifie la syntaxe d'une action et on force le premier argument de
 * l'action a être "import".
 * </p>
 * <h3>Conversion de type</h3>
 * <p>
 * Pour la conversion de type nous utilisons common-beans. Les types supportes sont:
 * <ul>
 * <li> les primitif (byte, short, int, long, float, double, char, boolean)
 * <li> {@link String}
 * <li> {@link File}
 * <li> {@link URL}
 * <li> {@link Class}
 * <li> Sql{@link Date}
 * <li> Sql{@link Time}
 * <li> Sql{@link Timestamp}
 * <li> les tableaux d'un type primitif ou {@link String}. Chaque element doit
 * etre separe par une virgule.
 * </ul>
 * <p>
 * Pour supporter d'autre type, il vous suffit d'enregistrer de nouveau
 * converter dans commons-beans.
 * </p>
 * <h3>Les substitutions de variable</h3>
 * <p>
 * {@link ApplicationConfig} supporte les substituions de variables de la forme
 * <code>${xxx}</code> où {@code xxx} est une autre variable de la configuration.
 * </p>
 * <p>
 * Exemple (dans un fichier de configuration):
 * <pre>
 * firstname = John
 * lastname = Doe
 * fullname = ${firstname} ${lastname}
 * </pre>
 * <code>getOption("fullname")</code> retournera <code>"John Doe"</code>.
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.30
 */
public class ApplicationConfig {

    public static final String LIST_SEPARATOR = ",";
    /**
     * Configuration file key option.
     */
    public static final String CONFIG_FILE_NAME = "config.file";
    /**
     * Configuration file key option.
     */
    public static final String EXTRA_CONFIG_DIRECTORY = "extra.config.directyory";
    /**
     * Configuration encoding key option.
     */
    public static final String CONFIG_ENCODING = "config.encoding";
    /**
     * Permet d'associer un nom de contexte pour prefixer les options {@link #CONFIG_PATH} et {@link #CONFIG_FILE_NAME}.
     */
    public static final String APP_NAME = "app.name";
    /**
     * Configuration directory where config path in located.
     * <p>
     * Use default system configuration if nothing is defined:
     * <ul>
     * <li>Linux : /etc/xxx.properties
     * <li>Windows : C:\\Windows\\System32\\xxx.properties
     * <li>Mac OS : /etc/
     * </ul>
     */
    public static final String CONFIG_PATH = "config.path";
    /**
     * Used to know what is separator between class and method
     */
    public static final String CLASS_METHOD_SEPARATOR = "#";
    /**
     * Logger.
     */
    private static final Logger log = LogManager.getLogger(ApplicationConfig.class);
    /**
     * To resolve recursive properties.
     */
    private final RecursivePropertiesResolver recursivePropertiesResolver = new RecursivePropertiesResolver();
    /**
     * System os name. (windows, linux, max os x)
     */
    protected String osName;
    /**
     * TODO
     */
    protected boolean useOnlyAliases;
    /**
     * vrai si on est en train de parser les options de la ligne de commande.
     */
    protected boolean inParseOptionPhase;
    /**
     * Contient les fichiers de propriétés par scope.
     */
    protected EnumMap<ApplicationConfigScope, Properties> propertiesByScope = new EnumMap<>(ApplicationConfigScope.class);
    /**
     * TODO
     */
    protected Map<String, CacheItem<?>> cacheOption = new HashMap<>();
    /**
     * TODO
     */
    protected Map<Class<?>, Object> cacheAction = new HashMap<>();
    /**
     * contient apres l'appel de parse, la liste des arguments non utilises
     */
    protected List<String> unparsed = new ArrayList<>();
    /**
     * TODO
     */
    protected Map<String, List<String>> aliases = new HashMap<>();
    /**
     * TODO
     */
    protected Map<Integer, List<Action>> actions = new HashMap<>();
    /**
     * permet de conserver des objets associe avec ce ApplicationConfig
     */
    protected Map<String, Object> context = new HashMap<>();
    /**
     * To read/write config.
     */
    protected RuntimeStorage configStorage;
    private Map<String, String> propertiesMigration;

    /**
     * Defines a runtime action to be launched  via the {@link #doAction()}
     * method.
     *
     * @author poussin
     */
    public static class Action {

        protected int step;
        protected Object o;
        protected Method m;
        protected String[] params;

        public Action(int step, Object o, Method m, String... params) {
            this.step = step;
            this.o = o;
            this.m = m;
            this.params = params;
        }

        public void doAction() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
            ApplicationConfigReflectionHelper.call(o, m, params);
        }

        @Retention(RetentionPolicy.RUNTIME)
        @Target(ElementType.METHOD)
        public @interface Step {

            int value() default 0;
        }
    }

    /**
     * Item used for cacheOption
     *
     * @param <T> type of item
     */
    protected static class CacheItem<T> {

        /**
         * typed option value
         */
        public T item;

        /**
         * hash of string representation
         */
        public int hash;

        public CacheItem(T item, int hash) {
            this.item = item;
            this.hash = hash;
        }
    }

    /**
     * Get user home directory (system property {@code user.home}).
     *
     * @return user home directory
     */
    public static String getUserHome() {
        return System.getProperty("user.home");
    }

    /**
     * Init ApplicationConfig with current simple class name as config file.
     * <p>
     * Also init converters.
     *
     * @see ConverterUtil#initConverters()
     */
    public ApplicationConfig() {
        this(null, null);
    }

    /**
     * Create configuration for a particular configuration filename
     *
     * @param configFilename name of config to use
     */
    public ApplicationConfig(String configFilename) {
        this(null, configFilename);
    }

    /**
     * Init ApplicationConfig with current simple class name as config file
     * and use Properties parameter as defaults
     * <p>
     * Also init converters.
     *
     * @param defaults properties
     * @see ConverterUtil#initConverters()
     */
    public ApplicationConfig(Properties defaults) {
        this(defaults, null);
    }

    /**
     * All in one, this constructor allow to pass all necessary argument to
     * initialise ApplicationConfig and parse command line
     *
     * @param defaults       properties that override default value of optionClass, can be null
     * @param configFilename override default config filename, can be null
     * @since 2.4.8
     */
    public ApplicationConfig(Properties defaults, String configFilename) {
        this(ApplicationConfigInit.defaultInit().setConfigFileName(configFilename).setDefaults(defaults));
    }

    /**
     * All in one, this constructor allow to pass all necessary argument to
     * initialise ApplicationConfig and parse command line
     *
     * @param init configuration builder
     * @since 3.0
     */
    public ApplicationConfig(ApplicationConfigInit init) {
        init(init);
    }

    /**
     * On sépare l'initialisation du constructeur pour pouvoir ne pas exécuter ce code sur des classes surchargeant ApplicationConfig
     *
     * @param init l'objet d'initialisation de l'applicationConfig
     */
    protected void init(ApplicationConfigInit init) {
        if (init == null) {
            throw new NullPointerException("Can't init ApplicationConfig without an init parameter");
        }
        configStorage = RuntimeStorage.get(init.getReadFormat());
        Set<ApplicationConfigScope> scopes = init.getScopes();
        Properties lastProperties = null;
        // defaults
        if (scopes.contains(ApplicationConfigScope.DEFAULTS)) {
            lastProperties = new Properties();
            propertiesByScope.put(ApplicationConfigScope.DEFAULTS, lastProperties);
        }
        // class-path
        if (scopes.contains(ApplicationConfigScope.CLASS_PATH)) {
            lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.CLASS_PATH, lastProperties);
        }
        // system
        if (scopes.contains(ApplicationConfigScope.SYSTEM)) {
            lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.SYSTEM, lastProperties);
        }
        // home
        if (scopes.contains(ApplicationConfigScope.HOME)) {
            lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.HOME, lastProperties);
        }
        // current
        if (scopes.contains(ApplicationConfigScope.CURRENT)) {
            lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.CURRENT, lastProperties);
        }
        // extra
        String extraConfigDirectory = init.getExtraConfigDirectory();
        if (scopes.contains(ApplicationConfigScope.EXTRA)) {
            if (extraConfigDirectory == null) {
                log.warn("Won't use EXTRA scope, init does not define extraConfigDirectory");
            } else {
                setExtraConfigDirectory(extraConfigDirectory);
                lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
                propertiesByScope.put(ApplicationConfigScope.EXTRA, lastProperties);
            }
        }
        // env
        if (scopes.contains(ApplicationConfigScope.ENV)) {
            lastProperties = (lastProperties == null ? new EnvProperties() : new EnvProperties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.ENV, lastProperties);
        }
        // jvm
        if (scopes.contains(ApplicationConfigScope.JVM)) {
            lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.JVM, lastProperties);
        }
        // line
        if (scopes.contains(ApplicationConfigScope.LINE)) {
            lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.LINE, lastProperties);
        }
        // options
        if (scopes.contains(ApplicationConfigScope.OPTIONS)) {
            lastProperties = (lastProperties == null ? new Properties() : new Properties(lastProperties));
            propertiesByScope.put(ApplicationConfigScope.OPTIONS, lastProperties);
        }
        Properties defaults = init.getDefaults();
        String configFilename = init.getConfigFilename();
        if (defaults != null) {
            // iterate with Properties method and not with Hashtable method to
            // prevent missed value with chained Properties object
            for (String key : defaults.stringPropertyNames()) {
                setDefaultOption(key, defaults.getProperty(key));
            }
        }
        setEncoding(StandardCharsets.UTF_8.name());
        if (configFilename == null) {
            setConfigFileName(getClass().getSimpleName());
        } else {
            setDefaultOption(CONFIG_FILE_NAME, configFilename);
        }
        // init extra-converters
        ConverterUtil.initConverters();
        // get system os name
        osName = System.getProperty("os.name");
    }

    /**
     * Get user name (system property {@code user.name}).
     *
     * @return user name
     */
    public String getUsername() {
        return getOption("user.name");
    }

    /**
     * Get os name (system property {@code os.name}).
     *
     * @return os name
     * @since 2.6.6
     */
    public String getOsName() {
        return osName;
    }

    /**
     * Get os arch (system property {@code os.arch}).
     *
     * @return os arch
     * @since 2.6.6
     */
    public String getOsArch() {
        return getOption("os.arch");
    }

    /**
     * @return {@code true} if on Windows os, {@code false} otherwise.
     * @since 1.0.28
     */
    public boolean isWindowsOs() {
        return osName.toLowerCase().contains("windows");
    }

    /**
     * Load default given options.
     *
     * @param options options to load
     * @param <O>     type of enum extend {@link ConfigOptionDef}
     * @since 2.4.8
     */
    public <O extends ConfigOptionDef> void loadDefaultOptions(O[] options) {
        // load default option (included configuration file name : important)
        Properties defaults = getProperties(ApplicationConfigScope.DEFAULTS);
        for (ConfigOptionDef o : options) {
            String key = o.getKey();
            if (defaults.getProperty(key) == null && o.getDefaultValue() != null) {
                // only override if not already existing
                defaults.setProperty(key, o.getDefaultValue());
            }
        }
    }

    /**
     * Load given actions.
     *
     * @param actions actions to load
     * @param <A>     type of enum extend {@link ConfigActionDef}
     * @since 2.4.8
     */
    public <A extends ConfigActionDef> void loadActions(A[] actions) {
        // load actions
        for (A a : actions) {
            for (String alias : a.getAliases()) {
                addActionAlias(alias, a.getAction());
            }
        }
    }

    /**
     * Used to put default configuration option in config option. Those options
     * are used as fallback value.
     *
     * @param key   default property key
     * @param value default property value
     */
    public void setDefaultOption(String key, String value) {
        getProperties(ApplicationConfigScope.DEFAULTS).setProperty(key, value);
    }

    public void putAll(Properties prop, ApplicationConfigScope scope) {
        Properties properties = getProperties(scope);
        if (properties != null) {
            prop.putAll(properties);
        }
    }

    protected Properties getProperties(ApplicationConfigScope scope) {
        return propertiesByScope.get(scope);
    }

    /**
     * Get properties to save.
     *
     * @param forceAll    if true save all config option (with defaults, classpath, env, command line)
     * @param excludeKeys optional list of keys to exclude from
     */
    public Properties getPropertiesToSave(boolean forceAll, String... excludeKeys) {

        // store sorted in file
        Properties prop = new SortedProperties();

        if (forceAll) {
            putAll(prop, ApplicationConfigScope.DEFAULTS);
            putAll(prop, ApplicationConfigScope.CLASS_PATH);
        }
        putAll(prop, ApplicationConfigScope.SYSTEM);
        putAll(prop, ApplicationConfigScope.HOME);
        putAll(prop, ApplicationConfigScope.CURRENT);
        putAll(prop, ApplicationConfigScope.EXTRA);
        if (forceAll) {
            putAll(prop, ApplicationConfigScope.JVM);
            putAll(prop, ApplicationConfigScope.ENV);
            putAll(prop, ApplicationConfigScope.LINE);
        }
        putAll(prop, ApplicationConfigScope.OPTIONS);

        for (String excludeKey : excludeKeys) {
            prop.remove(excludeKey);
        }
        return prop;
    }

    /**
     * Save configuration, in specified file.
     *
     * @param file        file where config will be written
     * @param forceAll    if true save all config option (with defaults, classpath, env, command line)
     * @param excludeKeys optional list of keys to exclude from
     * @throws IOException if IO pb
     */
    public void save(File file, boolean forceAll, String... excludeKeys) throws IOException {

        // store sorted in file
        Properties prop = getPropertiesToSave(forceAll, excludeKeys);

        // Ano #687 : create parentFile before using it in FileWriter
        Files.createDirectories(file.getParentFile().toPath());
        log.debug(String.format("Creation of config directory %s", file.getParent()));
        saveResource(file, prop, "Last saved " + new Date());
    }

    /**
     * Save configuration, in system directory (/etc/) using the
     * {@link #getConfigFileName}. Default, env and command line note saved.
     *
     * @param excludeKeys optional list of keys to exclude from
     */
    public void saveForSystem(String... excludeKeys) throws ApplicationConfigSaveException {
        File file = getSystemConfigFile();
        log.debug(String.format("will save system configuration in %s", file));
        try {
            save(file, false, excludeKeys);
        } catch (IOException eee) {
            throw new ApplicationConfigSaveException(eee);
        }
    }

    /**
     * Save configuration, in user home directory using the
     * {@link #getConfigFileName}. Default, env and command line note saved
     *
     * @param excludeKeys optional list of keys to exclude from
     */
    public void saveForUser(String... excludeKeys) throws ApplicationConfigSaveException {
        File file = getUserConfigFile();
        log.debug(String.format("will save user configuration in %s", file));
        try {
            save(file, false, excludeKeys);
        } catch (IOException eee) {
            throw new ApplicationConfigSaveException(eee);
        }
    }

    /**
     * Save configuration, in user home directory using the
     * {@link #getConfigFileName}. Default, env and commande line note saved
     *
     * @param excludeKeys optional list of keys to exclude from
     */
    public void saveForExtra(String... excludeKeys) throws ApplicationConfigSaveException {
        File file = getExtraConfigFile();
        log.debug(String.format("will save extra configuration in %s", file));
        try {
            save(file, false, excludeKeys);
        } catch (IOException eee) {
            throw new ApplicationConfigSaveException(eee);
        }
    }

    /**
     * Save configuration, in current directory using the {@link #getConfigFileName}.
     * Default, env and command line note saved.
     *
     * @param excludeKeys optional list of keys to exclude from
     */
    public void saveForCurrent(String... excludeKeys) throws ApplicationConfigSaveException {
        File file = getCurrentConfigFile();
        log.debug(String.format("will save configuration in %s", file));
        try {
            save(file, false, excludeKeys);
        } catch (IOException eee) {
            throw new ApplicationConfigSaveException(eee);
        }
    }

    /**
     * Clean the user configuration file (The one in user home) and save it
     * in user config file.
     * <p>
     * All options with an empty value will be removed from this file.
     * <p>
     * Moreover, like {@link #saveForUser(String...)} the given
     * {@code excludeKeys} will never be saved.
     * <p>
     * This method can be useful when migrating some configuration from a
     * version to another one with deprecated options (otherwise they will stay
     * for ever in the configuration file with an empty value which is not
     * acceptable).
     *
     * <strong>Important note:</strong> Using this method can have some strange
     * side effects, since it could then allow to reuse default configurations
     * from other level (default, env, jvm,...). Use with care only!
     *
     * @param excludeKeys optional list of key to not treat in cleaning process,
     *                    nor save in user user config file.
     * @since 2.6.6
     */
    public void cleanUserConfig(String... excludeKeys) throws ApplicationConfigSaveException {
        Properties homeFile = getProperties(ApplicationConfigScope.HOME);
        if (homeFile != null) {
            Set<String> keys = new HashSet<>(homeFile.stringPropertyNames());
            List<String> toExclude = Arrays.asList(excludeKeys);
            for (String key : keys) {
                if (!toExclude.contains(key)) {
                    String property = homeFile.getProperty(key);
                    if (Strings.isEmpty(property)) {
                        log.info(String.format("Remove blank property: %s", key));
                        homeFile.remove(key);
                    }
                }
            }
            // can now save cleaned user config
            saveForUser(excludeKeys);
        }
    }

    /**
     * Obtain the system config file location.
     *
     * @return the system config file location
     * @throws ApplicationConfigFileNameNotInitializedException if no config file name found in configuration
     */
    public File getSystemConfigFile() throws ApplicationConfigFileNameNotInitializedException {
        String configFileName = getConfigFileName();
        if (configFileName == null) {
            throw new ApplicationConfigFileNameNotInitializedException();
        }
        return new File(getConfigPath(), configFileName);
    }

    /**
     * Obtain the user config file location.
     *
     * @return the user config file location
     * @throws ApplicationConfigFileNameNotInitializedException if no config file name found in configuration
     */
    public File getUserConfigFile() throws ApplicationConfigFileNameNotInitializedException {
        String configFileName = getConfigFileName();
        if (configFileName == null) {
            throw new ApplicationConfigFileNameNotInitializedException();
        }
        return new File(getUserConfigDirectory(), configFileName);
    }

    /**
     * Obtain the current directory config file location.
     *
     * @return the current directory config file location
     * @throws ApplicationConfigFileNameNotInitializedException if no config file name found in configuration
     */
    public File getCurrentConfigFile() throws ApplicationConfigFileNameNotInitializedException {
        String configFileName = getConfigFileName();
        if (configFileName == null) {
            throw new ApplicationConfigFileNameNotInitializedException();
        }
        return new File(new File("").getAbsolutePath(), configFileName);
    }

    /**
     * Obtain the extra directory config file location.
     *
     * @return the extra directory config file location
     * @throws ApplicationConfigFileNameNotInitializedException if no config file name found in configuration
     */
    public File getExtraConfigFile() throws ApplicationConfigFileNameNotInitializedException {
        String configFileName = getConfigFileName();
        if (configFileName == null) {
            throw new ApplicationConfigFileNameNotInitializedException();
        }
        String extraConfigDirectory = getExtraConfigDirectory();
        if (extraConfigDirectory == null) {
            throw new ApplicationConfigFileNameNotInitializedException("Can't find option: " + getExtraConfigDirectoryOption());
        }
        return Path.of(extraConfigDirectory).resolve(configFileName).toFile();
    }

    /**
     * Return list of unparsed command line argument
     *
     * @return list of unparsed arguments
     */
    public List<String> getUnparsed() {
        return unparsed;
    }

    /**
     * Add action to list of action to do.
     *
     * @param action action to add, can be null.
     */
    public void addAction(Action action) {
        if (action != null) {
            Integer step = action.step;
            List<Action> list = actions.computeIfAbsent(step, k -> new LinkedList<>());
            list.add(action);
        }
    }

    /**
     * Return ordered action step number.
     * example: 0,1,5,6
     *
     * @return ordered action step number
     * @since 2.4
     */
    public List<Integer> getActionStep() {
        List<Integer> result = new ArrayList<>(actions.keySet());
        Collections.sort(result);
        return result;
    }

    /**
     * Do all action in specified order step (first 0).
     *
     * @throws IllegalAccessException    if action invocation failed
     * @throws IllegalArgumentException  if action invocation failed
     * @throws InvocationTargetException if action invocation failed
     * @throws InstantiationException    if action invocation failed
     * @see Action.Step
     * @since 2.4
     */
    public void doAllAction() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
        for (int step : getActionStep()) {
            doAction(step);
        }
    }

    /**
     * Do action in specified step.
     *
     * @param step do action only defined in this step
     * @throws IllegalAccessException    if action invocation failed
     * @throws IllegalArgumentException  if action invocation failed
     * @throws InvocationTargetException if action invocation failed
     * @throws InstantiationException    if action invocation failed
     * @see Action.Step
     */
    public void doAction(int step) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
        List<Action> list = actions.get(step);
        if (list != null) {
            for (Action a : list) {
                a.doAction();
            }
        }
    }

    public boolean isUseOnlyAliases() {
        return useOnlyAliases;
    }

    public void setUseOnlyAliases(boolean useOnlyAliases) {
        this.useOnlyAliases = useOnlyAliases;
    }

    public Charset getCharset() {
        return Charset.forName(Objects.requireNonNull(getEncoding()));
    }

    /**
     * Get the encoding used to read/write resources.
     * <p>
     * This value is stored as an option using the
     * {@link #getEncodingOption()} key.
     *
     * @return the encoding used to read/write resources.
     * @since 2.3
     */
    public String getEncoding() {
        return getOption(getEncodingOption());
    }

    /**
     * Set the new encoding option.
     *
     * @param encoding the new value of the option encoding
     * @since 2.3
     */
    public void setEncoding(String encoding) {
        setDefaultOption(getEncodingOption(), encoding);
    }

    /**
     * All argument in aliases as key is substitued by target.
     *
     * @param alias  alias string as '-v'
     * @param target substitution as '--option verbose true'
     */
    public void addAlias(String alias, String... target) {
        aliases.put(alias, Arrays.asList(target));
    }

    /**
     * Add alias for action. This method put just -- front the actionMethod and
     * call {@link #addAlias(String, String...)}.
     *
     * @param alias        the alias to add for the given method action
     * @param actionMethod must be fully qualified method path:
     *                     package.Class#method
     */
    public void addActionAlias(String alias, String actionMethod) {
        addAlias(alias, "--" + actionMethod);
    }

    /**
     * Get name of file where options are read (in /etc, $HOME, $CURDIR).
     *
     * @return name of file
     */
    public String getConfigFileName() {
        return getOption(getConfigFileNameOption());
    }

    /**
     * Set name of file where options are read (in /etc, $HOME, $CURDIR)
     * This set used {@link #setDefaultOption(String, String)}.
     *
     * @param name file name
     */
    public void setConfigFileName(String name) {
        // put in defaults, this permit user to overwrite it on commande line
        setDefaultOption(getConfigFileNameOption(), name);
    }

    protected String getConfigFileNameOption() {
        String optionName = CONFIG_FILE_NAME;
        if (getOption(APP_NAME) != null) {
            optionName = getOption(APP_NAME) + optionName;
        }
        return optionName;
    }

    public String getExtraConfigDirectory() {
        return getOption(getExtraConfigDirectoryOption());
    }

    public void setExtraConfigDirectory(String name) {
        // put in defaults, this permit user to overwrite it on commande line
        setDefaultOption(getExtraConfigDirectoryOption(), name);
    }

    protected String getExtraConfigDirectoryOption() {
        String optionName = EXTRA_CONFIG_DIRECTORY;
        if (getOption(APP_NAME) != null) {
            optionName = getOption(APP_NAME) + optionName;
        }
        return optionName;
    }

    /**
     * Obtains the key used to store the option encoding.
     *
     * @return the encoding option'key
     * @since 2.3
     */
    protected String getEncodingOption() {
        String optionName = CONFIG_ENCODING;
        if (getOption(APP_NAME) != null) {
            optionName = getOption(APP_NAME) + optionName;
        }
        return optionName;
    }

    /**
     * Use appName to add a context in config.file and config.path options.
     * <p>
     * Ex for an application named 'pollen' : {@code config.file} option becomes
     * {@code pollen.config.file} and {@code config.path} becomes
     * {@code pollen.config.path}
     *
     * @param appName to use as application context
     * @since 1.2.1
     */
    public void setAppName(String appName) {
        // encoding option can use appName as prefix
        // so a side effect is to have
        String oldEncoding = getEncoding();

        // config gile option can use appName as prefix
        String oldConfigFile = getConfigFileName();
        setDefaultOption(APP_NAME, appName);

        String newEncoding = getEncoding();
        String newConfigFile = getConfigFileName();
        if (newEncoding == null) {
            // push back old encoding
            setEncoding(oldEncoding);
        }
        if (newConfigFile == null) {
            // push back old config file
            setConfigFileName(oldConfigFile);
        }
    }

    /**
     * Get configuration file path to use.
     * <p>
     * Use (in order) one of the following definition:
     * <ul>
     * <li>{@link #CONFIG_PATH} option</li>
     * <li>system dependant path</li>
     * </ul>
     *
     * @return path to use with endind {@link File#separator}
     * @since 1.2.1
     */
    public String getConfigPath() {
        // Concat appName to configPath option to specify context for application deployment
        String appName = getOption(APP_NAME) != null ? getOption(APP_NAME) : "";
        String result = getOption(appName + CONFIG_PATH);
        if (result == null) {
            result = getSystemConfigurationPath();
        }
        log.debug(String.format("Configuration path used : %s", result));
        return result;
    }

    /**
     * Get system configuration path.
     * <p>
     * Currently supported:
     * <ul>
     * <li>Windows : C:\Windows\System32</li>
     * <li>Unix : /etc/</li>
     * </ul>
     *
     * @return the system path
     * @since 1.2.1
     */
    protected String getSystemConfigurationPath() {

        String systemPath = null;

        // Windows
        if (osName.toLowerCase().contains("windows")) {
            // try 1 : %SystemDirectory%
            try {
                String systemDirectory = System.getenv("SystemDirectory");
                if (systemDirectory != null && systemDirectory.length() > 0) {
                    systemPath = systemDirectory;
                }
            } catch (SecurityException eee) {
                log.error("Can't read env property", eee);
            }
            // try 2 : %SystemRoot%
            if (systemPath != null) {
                try {
                    String systemRoot = System.getenv("SystemRoot");
                    if (systemRoot != null && systemRoot.length() > 0) {
                        systemPath = systemRoot + "\\System32";
                    }
                } catch (SecurityException eee) {
                    log.error("Can't read env property", eee);
                }
            } else {
                // default value
                systemPath = "C:\\Windows\\System32";
            }
            // %SystemDrive% exists too : C:
        } else {
            // All others are unix like
            // look for in /etc/
            systemPath = File.separator + "etc" + File.separator;
        }
        log.debug(systemPath);
        return systemPath;
    }

    /**
     * Get user configuration path.
     * <p>
     * Currently supported:
     * <ul>
     * <li>Windows : ${user.home}\\Application Data\\</li>
     * <li>Max os x : ${user.home}/Library/Application Support</li>
     * <li>Unix : ${user.home}/.config</li>
     * </ul>
     * <p>
     * Unix norm is based on freedesktop concept explained here :
     * http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
     *
     * @return the user configuration path
     * @since 1.2.1
     */
    public String getUserConfigDirectory() {

        String userPath = null;

        String userHome = null;
        try {
            userHome = getUserHome();
        } catch (SecurityException ignore) {
        }

        if (userHome != null) {
            // windows
            if (osName.toLowerCase().contains("windows")) {
                try {
                    String appDataEV = System.getenv("APPDATA");
                    if (appDataEV != null && appDataEV.length() > 0) {
                        userPath = appDataEV;
                    }
                } catch (SecurityException ignore) {
                }

                if (userPath == null || userPath.isEmpty()) {
                    // ${userHome}\Application Data\
                    userPath = userHome + File.separator + "Application Data";
                }
            } else if (osName.toLowerCase().contains("mac os x")) {
                // ${userHome}/Library/Application Support/${applicationId}
                userPath = userHome + File.separator + "/Library/Application Support";
            } else {
                // ${userHome}/.config/
                userPath = userHome + "/.config";
            }
        }

        // what if null ?

        return userPath;
    }

    /**
     * Teste si un option existe ou non.
     *
     * @param key la clef de l'option à tester
     * @return {@code true} si l'option existe, {@code false} sinon.
     */
    public boolean hasOption(String key) {
        // on est oblige de faire un get, car le containsKey n'est pas recursif
        // sur tous les properties
        return getProperties(ApplicationConfigScope.OPTIONS).getProperty(key) != null;
    }

    /**
     * Teste si un option existe ou non
     *
     * @param key la clef de l'option à tester
     * @return {@code true} si 'loption existe, {@code false} sinon.
     */
    public boolean hasOption(ConfigOptionDef key) {
        return hasOption(key.getKey());
    }

    /**
     * ajoute un objet dans le context, la classe de l'objet est utilise comme cle
     *
     * @param o l'objet à ajouter
     * @since 2.4.2
     */
    public void putObject(Object o) {
        putObject(o.getClass().getName(), o);
    }

    /**
     * ajoute un objet dans le context, 'name' est utilise comme cle
     *
     * @param name clef de l'option
     * @param o    value de l'option
     * @since 2.4.2
     */
    public void putObject(String name, Object o) {
        context.put(name, o);
    }

    /**
     * recupere un objet de la class&lt;E&gt;, s'il n'existe pas encore, il est cree
     * (il faut donc que class&lt;E&gt; soit instanciable).
     * <p>
     * E peut prendre en argument du contruteur un objet de type ApplicationConfig
     *
     * @param <E>   le type de l'option à récupérer
     * @param clazz le type de l'option à récupérer (ou créer)
     * @return l'objet requis
     * @since 2.4.2
     */
    public <E> E getObject(Class<E> clazz) {
        return getObject(clazz, clazz.getName());
    }

    /**
     * recupere un objet ayant le nom 'name', s'il n'existe pas encore, il est
     * cree en utilisant la class&lt;E&gt;, sinon il est simplement caster vers cette
     * classe.
     * <p>
     * E peut prendre en argument du contruteur un objet de type ApplicationConfig
     *
     * @param <E>   le type de l'option à récupérer
     * @param clazz le type de l'option à récupérer (ou créer)
     * @param name  le nom de l'option à récupérer (ou créer)
     * @return l'objet requis
     * @since 2.4.2
     */
    public <E> E getObject(Class<E> clazz, String name) {
        E result = clazz.cast(context.get(name));
        if (result == null) {
            result = ApplicationConfigReflectionHelper.newInstance(clazz, Collections.singleton(this), true);
            putObject(name, result);
        }
        return result;
    }

    /**
     * retourne une nouvelle instance d'un objet dont on recupere la la class
     * dans la configuration via la cle 'key'. Retourne null si la cle n'est pas
     * retrouve.
     *
     * @param key le nom de l'option à récupérer
     * @return l'objet requis
     * @since 2.4.2
     * @deprecated use {@link #getOption(Class, String)} instead.
     */
    @Deprecated
    public Object getOptionAsObject(String key) {
        Object result = null;
        if (hasOption(key)) {
            Class<?> clazz = getOptionAsClass(key);
            result = ApplicationConfigReflectionHelper.newInstance(clazz, Collections.singleton(this), true);
        }
        return result;
    }

    /**
     * retourne une nouvelle instance d'un objet dont on recupere la la class
     * dans la configuration via la cle 'key' et le cast en E. Retourne null
     * si la cle n'est pas retrouve
     * <p>
     * E peut prendre en argument du contruteur un objet de type ApplicationConfig
     *
     * @param <E>   le type de l'option à récupérer
     * @param clazz le type de l'option à récupérer
     * @param key   le nom de l'option à récupérer
     * @return l'objet requis
     * @since 2.4.2
     * @deprecated use {@link #getOption(Class, String)} instead.
     */
    @Deprecated
    public <E> E getOptionAsObject(Class<E> clazz, String key) {
        return clazz.cast(getOptionAsObject(key));
    }

    /**
     * retourne l'objet instancier via la classe recupere dans la configuration
     * via la cle 'key'. Une fois instancie, le meme objet est toujours retourne.
     * On null si key n'est pas retrouve.
     * <p>
     * La classe peut avoir un constructeur prenant un ApplicationConfig
     *
     * @param key le nom de l'option à récupérer
     * @return l'objet requis
     * @since 2.4.2
     */
    public Object getOptionAsSingleton(String key) {
        Object result = context.get(key);
        if (result == null) {
            result = getOptionAsObject(key);
            putObject(key, result);
        }
        return result;
    }

    /**
     * retourne l'objet caster en 'E', instancier via la classe recupere dans la
     * configuration via la cle 'key'. Une fois instancie, le meme objet est
     * toujours retourne. On null si key n'est pas retrouve
     * <p>
     * La classe peut avoir un constructeur prenant un ApplicationConfig
     *
     * @param <E>   le type de l'option à récupérer
     * @param clazz le type de l'option à récupérer
     * @param key   le nom de l'option à récupérer
     * @return l'objet requis
     * @since 2.4.2
     */
    public <E> E getOptionAsSingleton(Class<E> clazz, String key) {
        return clazz.cast(getOptionAsSingleton(key));
    }

    /**
     * Set option value.
     * If the value is null, then the option is removed.
     *
     * @param key   property key
     * @param value property value
     */
    public void setOption(String key, String value) {
        ApplicationConfigScope optionScope;
        if (inParseOptionPhase) {
            optionScope = ApplicationConfigScope.LINE;
        } else {
            optionScope = ApplicationConfigScope.OPTIONS;
        }
        boolean setOption = true;
        if (value == null) {
            setOption = false;
        } else {
            Properties defaults = getProperties(ApplicationConfigScope.DEFAULTS);
            if (defaults != null) {
                String defaultValue = defaults.getProperty(key);
                if (defaultValue != null) {
                    defaultValue = replaceRecursiveOptions(defaultValue);
                    if (value.equals(defaultValue)) {

                        // on supprime l'option, car c'est la valeur par défaut
                        setOption = false;
                    }
                }
            }
        }
        if (setOption) {
            getProperties(optionScope).setProperty(key, value);
        } else {
            // delete from any following scopes
            remove(key,
                   optionScope,
                   ApplicationConfigScope.CURRENT,
                   ApplicationConfigScope.EXTRA,
                   ApplicationConfigScope.HOME,
                   ApplicationConfigScope.SYSTEM,
                   ApplicationConfigScope.CLASS_PATH);
        }
    }

    /**
     * get option value as string.
     * <p>
     * Replace inner ${xxx} value.
     *
     * @param key the option's key
     * @return String representation value
     */
    public String getOption(String key) {
        Properties options = getProperties(ApplicationConfigScope.OPTIONS);
        String value = options == null ? null : options.getProperty(key);
        // replace ${xxx}
        value = replaceRecursiveOptions(value);
        return value;
    }

    /**
     * Replace included ${xxx} suboptions by their values.
     *
     * @param option option to replace into
     * @return replaced option
     * @since 1.1.3
     */
    public String replaceRecursiveOptions(String option) {
        if (option == null) {
            return null;
        }
        return recursivePropertiesResolver.resolve(this::getOption, option);
    }

    /**
     * @param overwrite overwrite properties
     * @return new ApplicationConfig with overwrite use as value for option
     * if found in it. Otherwise return value found in this config
     */
    public ApplicationConfig getConfig(Map<String, String> overwrite) {
        return new OverwriteApplicationConfig(this, overwrite);
    }

    /**
     * Returns a sub config that encapsulate this ApplicationConfig.
     *
     * @param prefix prefix to put automaticaly at beginning of all key
     * @return sub config that encapsulate this ApplicationConfig
     * @since 2.4.9
     */
    public SubApplicationConfig getSubConfig(String prefix) {
        return new SubApplicationConfig(this, prefix);
    }

    /**
     * Permet de recuperer l'ensemble des options commencant par une certaine
     * chaine.
     *
     * @param prefix debut de cle a recuperer
     * @return la liste des options filtrées
     */
    public Properties getOptionStartsWith(String prefix) {
        Properties result = new Properties();

        Properties options = getProperties(ApplicationConfigScope.OPTIONS);
        if (options != null) {
            for (String key : options.stringPropertyNames()) {
                if (key.startsWith(prefix)) {
                    result.setProperty(key, options.getProperty(key));
                }
            }
        }

        return result;
    }

    /**
     * Get option value from a option definition.
     *
     * @param key the definition of the option
     * @return the value for the given option
     */
    public Object getOption(ConfigOptionDef key) {
        return getOption(key.getType(), key.getKey());
    }

    /**
     * Get option value as typed value.
     *
     * @param <T>   type of the object wanted as return type
     * @param clazz type of object wanted as return type
     * @param key   the option's key
     * @return typed value
     */
    public <T> T getOption(Class<T> clazz, String key) {
        String value = getOption(key);
        T result = null;
        if (value != null) {
            result = clazz.cast(convertOption(clazz, key, value, false));
        }

        return result;
    }

    /**
     * Convert value in instance of clazz or List if asList is true
     * <p>
     * example:
     * <ul>
     * <li> convertOption(Boolean.class, "toto", "true,true", false) → false</li>
     * <li> convertOption(Boolean.class, "toto", null, false) → ? ConverterUtil dependant</li>
     * <li> convertOption(Boolean.class, "toto", "true,true", true) → [true, true]</li>
     * <li> convertOption(Boolean.class, "toto", null, true) → []</li>
     * </ul>
     *
     * @param <T>    result type expected
     * @param clazz  result type expected
     * @param key    option key
     * @param value  value to convert
     * @param asList value is string that represente a list
     * @return the converted option in the required type
     */
    protected <T> Object convertOption(Class<T> clazz, String key, String value, boolean asList) {
        String cacheKey = key + "-" + asList + "-" + clazz.getName();

        int hash = 0;
        if (value != null) {
            hash = value.hashCode();
        }

        CacheItem<?> cacheItem = cacheOption.get(cacheKey);
        // compute value if value don't exist in cacheOption or
        // if it's modified since last computation
        if (cacheItem == null || cacheItem.hash != hash) {
            if (asList) {
                List<T> list = new ArrayList<>();
                if (value != null) {
                    String[] values = Strings.split(value, LIST_SEPARATOR);
                    for (String valueString : values) {
                        // prefer use our convertert method (auto-register more converters)
                        T v = ConverterUtil.convert(clazz, valueString);
                        list.add(v);
                    }
                }
                cacheItem = new CacheItem<>(list, hash);
            } else {
                // prefer use our converter method (auto-register more converters)
                T v = ConverterUtil.convert(clazz, value);
                cacheItem = new CacheItem<>(v, hash);
            }
            // add new item to the cache
            cacheOption.put(cacheKey, cacheItem);
        }

        // take result in item
        return cacheItem.item;
    }

    /**
     * Help to convert value to list of object. If no option for this key
     * empty List is returned finaly
     *
     * @param key the key of searched option
     * @return value of option list
     */
    public OptionList getOptionAsList(String key) {
        String value = getOption(key);
        return new OptionList(this, key, value);
    }

    /**
     * Get option value as {@link File}.
     *
     * @param key the option's key
     * @return value as file
     */
    public File getOptionAsFile(String key) {
        File result = getOption(File.class, key);
        if (result != null) {
            result = result.getAbsoluteFile();
        }
        return result;
    }

    /**
     * Get option value as {@link Color}.
     *
     * @param key the option's key
     * @return value as color
     */
    public Color getOptionAsColor(String key) {
        return getOption(Color.class, key);
    }

    /**
     * Get option value as {@link Properties}, this property must be a filepath
     * and file must be a properties.
     * <p>
     * Returned Properties is {@link RecursiveProperties}.
     *
     * @param key the option's key
     * @return Properties object loaded with value pointed by file
     * @throws IOException if exception occured on read file
     */
    public Properties getOptionAsProperties(String key) throws IOException {
        File file = getOptionAsFile(key);
        Properties prop = new RecursiveProperties();
        try (FileReader reader = new FileReader(file)) {
            prop.load(reader);
        }
        return prop;
    }

    /**
     * Get option value as {@link URL}.
     *
     * @param key the option's key
     * @return value as URL
     */
    public URL getOptionAsURL(String key) {
        return getOption(URL.class, key);
    }

    /**
     * Get option value as {@link Class}.
     *
     * @param key the option's key
     * @return value as Class
     */
    public Class<?> getOptionAsClass(String key) {
        return getOption(Class.class, key);
    }

    /**
     * Get option value as {@link Date}.
     *
     * @param key the option's key
     * @return value as Date
     */
    public Date getOptionAsDate(String key) {
        return getOption(Date.class, key);
    }

    /**
     * Get option value as {@link Time}.
     *
     * @param key the option's key
     * @return value as Time
     */
    public Time getOptionAsTime(String key) {
        return getOption(Time.class, key);
    }

    /**
     * Get option value as {@link Timestamp}.
     *
     * @param key the option's key
     * @return value as Timestamp
     */
    public Timestamp getOptionAsTimestamp(String key) {
        return getOption(Timestamp.class, key);
    }

    /**
     * Get option value as {@code int}.
     *
     * @param key the option's key
     * @return value as {@code int}
     */
    public int getOptionAsInt(String key) {
        Integer result = getOption(Integer.class, key);
        if (result == null) {
            // primitive value can not be null
            result = 0;
        }
        return result;
    }

    /**
     * Get option value as {@code long}.
     *
     * @param key the option's key
     * @return value as {@code long}
     */
    public long getOptionAsLong(String key) {
        Long result = getOption(Long.class, key);
        if (result == null) {
            // primitive value can not be null
            result = 0L;
        }
        return result;
    }

    /**
     * Get option value as {@code float}.
     *
     * @param key the option's key
     * @return value as {@code float}
     * @since 2.2
     */
    public float getOptionAsFloat(String key) {
        Float result = getOption(Float.class, key);
        if (result == null) {
            // primitive value can not be null
            result = 0f;
        }
        return result;
    }

    /**
     * Get option value as {@code double}.
     *
     * @param key the option's key
     * @return value as {@code double}
     */
    public double getOptionAsDouble(String key) {
        Double result = getOption(Double.class, key);
        if (result == null) {
            // primitive value can not be null
            result = 0d;
        }
        return result;
    }

    /**
     * Get option value as {@code boolean}.
     *
     * @param key the option's key
     * @return value as {@code boolean}.
     */
    public boolean getOptionAsBoolean(String key) {
        Boolean result = getOption(Boolean.class, key);
        if (result == null) {
            // primitive value can not be null
            result = false;
        }
        return result;
    }

    /**
     * Get option value as {@link Locale}.
     *
     * @param key the option's key
     * @return value as {@link Locale}.
     * @since 2.0
     */
    public Locale getOptionAsLocale(String key) {
        return getOption(Locale.class, key);
    }

    /**
     * Get option value as {@link Version}.
     *
     * @param key the option's key
     * @return value as {@link Version}.
     * @since 2.0
     */
    public Version getOptionAsVersion(String key) {
        return getOption(Version.class, key);
    }

    /**
     * Get option value as {@link KeyStroke}.
     *
     * @param key the option's key
     * @return value as {@link KeyStroke}.
     * @since 2.5.1
     */
    public KeyStroke getOptionAsKeyStroke(String key) {
        return getOption(KeyStroke.class, key);
    }

    /**
     * Get all options from configuration.
     *
     * @return Properties which contains all options
     */
    public Properties getOptions() {
        return getProperties(ApplicationConfigScope.OPTIONS);
    }

    /**
     * Set manually options when you don't want to use parse method to check
     * properties file configured by {@link #setConfigFileName(String)}.
     *
     * @param options Properties which contains all options to set
     */
    public void setOptions(Properties options) {
        propertiesByScope.put(ApplicationConfigScope.OPTIONS, options);
    }

    /**
     * Get all options as flat {@link Properties} object (replace inner options).
     *
     * @return flat Properties object
     * @since 1.2.2
     */
    public Properties getFlatOptions() {
        return getFlatOptions(true);
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // C L A S S E S   D E C L A R A T I O N
    // 
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Get all options as flat {@link Properties} object.
     *
     * @param replaceInner if {@code true} replace imbricated options by theirs values
     * @return flat Properties object
     * @since 1.2.2
     */
    public Properties getFlatOptions(boolean replaceInner) {
        Properties props = new Properties();
        Properties options = getProperties(ApplicationConfigScope.OPTIONS);
        if (options != null) {
            for (String propertyKey : options.stringPropertyNames()) {
                String propertyValue;
                if (replaceInner) {
                    // replace ${xxx} option
                    propertyValue = getOption(propertyKey);
                } else {
                    // do not replace ${xxx} option
                    propertyValue = options.getProperty(propertyKey);
                }
                props.setProperty(propertyKey, propertyValue);
            }
        }
        return props;
    }

    /**
     * Get all set method on this object or super object.
     *
     * @return map with method name without set and in lower case as key, and
     * method as value
     */
    protected Map<String, Method> getMethods() {
        // looking for all methods set on ApplicationConfig
        Method[] allMethods = getClass().getMethods();
        Map<String, Method> methods = new HashMap<>();
        for (Method m : allMethods) {
            String methodName = m.getName();
            if (methodName.startsWith("set")) {
                methodName = methodName.substring(3).toLowerCase();
                methods.put(methodName, m);
            }
        }
        return methods;
    }

    /**
     * Take required argument for method in args. Argument used is removed from
     * args. If method has varArgs, we take all argument to next '--'
     *
     * @param m    the method to call
     * @param args iterator with many argument (equals or more than necessary
     * @return the arguments found for the given method
     */
    protected String[] getParams(Method m, ListIterator<String> args) {
        List<String> result = new ArrayList<>();
        if (m.isVarArgs()) {
            while (args.hasNext()) {
                String p = args.next();
                if (p.startsWith("--")) {
                    // stop search
                    args.previous();
                    break;
                } else {
                    result.add(p);
                    args.remove();
                }
            }
        } else {
            int paramLenght = m.getParameterTypes().length;
            for (int i = 0; i < paramLenght; i++) {
                String p = args.next();
                args.remove(); // remove this arg because is used now
                result.add(p);
            }
        }
        return result.toArray(new String[0]);
    }

    /**
     * Create action from string, string must be [package.][class][#][method]
     * if package, class or method missing, default is used
     *
     * @param name name of the action
     * @param args arguments for action invocation
     * @return the created action
     * @throws ArgumentsParserException  if parsing failed
     * @throws IllegalAccessException    if could not create action
     * @throws IllegalArgumentException  if could not create action
     * @throws InstantiationException    if could not create action
     * @throws InvocationTargetException if could not create action
     */
    @SuppressWarnings("unchecked")
    protected Action createAction(String name, ListIterator<String> args) throws ArgumentsParserException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Action result = null;

        if (name.startsWith("this#")) {

            // To use the application config class directly
            name = getClass().getName() + "#" + Strings.removeStart(name, "this#");
        }

        List<Method> methods = ApplicationConfigReflectionHelper.getMethod(name, true);

        Class clazz = null;
        Method method = null;
        if (methods.size() > 0) {
            if (methods.size() > 1) {
                log.warn(String.format("More than one method found, used the first: %s", methods));
            }
            method = methods.get(0);
            clazz = method.getDeclaringClass();
        }

        if (method != null) {
            // remove option from command line, because is used now
            args.remove();
            if (!name.startsWith(clazz.getName() + "#")) {
                // method declaring class is not real action class
                int sep = name.lastIndexOf("#");
                if (sep == -1) {
                    throw new IllegalArgumentException(String.format("Can't find class in %s", name));
                } else {
                    String className = name.substring(0, sep);
                    try {
                        clazz = Class.forName(className);
                    } catch (ClassNotFoundException e) {
                        throw new IllegalArgumentException(String.format("Can't find class %s", className), e);
                    }
                }
            }

            // creation de l'object sur lequel on fera l'appel
            Object o = cacheAction.get(clazz);
            if (o == null && !Modifier.isStatic(method.getModifiers())) {
                try {
                    o = ConstructorUtils.invokeConstructor(clazz, this);
                } catch (NoSuchMethodException eee) {
                    log.debug(String.format("Use default constructor, because no constructor" + " with Config parameter on class %s", clazz.getName()));
                    o = clazz.newInstance();
                }
                cacheAction.put(clazz, o);
            }

            // recherche du step de l'action
            int step = 0;
            Action.Step annotation = method.getAnnotation(Action.Step.class);
            if (annotation != null) {
                step = annotation.value();
            }

            String[] params = getParams(method, args);
            result = new Action(step, o, method, params);
        }

        return result;
    }

    /**
     * Parse option and call set necessary method, read jvm, env variable,
     * Load configuration file and prepare Action.
     *
     * @param args argument as main(String[] args)
     * @return ApplicationConfig instance
     * @throws ArgumentsParserException if parsing failed
     */
    public ApplicationConfig parse(String... args) throws ArgumentsParserException {
        if (args == null) {
            args = Strings.EMPTY_STRING_ARRAY;
        }
        try {
            Map<String, Method> methods = getMethods();
            List<String> arguments = new ArrayList<>(args.length);
            for (String arg : args) {
                if (aliases.containsKey(arg)) {
                    arguments.addAll(aliases.get(arg));
                } else {
                    arguments.add(arg);
                }
            }

            // first parse option
            inParseOptionPhase = true;
            for (ListIterator<String> i = arguments.listIterator(); i.hasNext(); ) {
                String arg = i.next();
                if (arg.equals("--")) {
                    // stop parsing
                    break;
                }
                if (arg.startsWith("--")) {
                    String optionName = arg.substring(2);
                    if (methods.containsKey(optionName)) {
                        i.remove(); // remove this arg because is used now
                        Method m = methods.get(optionName);
                        String[] params = getParams(m, i);
                        log.debug(String.format("Set option '%s' with method '%s %s'", optionName, m, Arrays.toString(params)));
                        ApplicationConfigReflectionHelper.call(this, m, params);
                    }
                }
            }
            inParseOptionPhase = false;

            //
            // second load options from all sources
            //
            // JVM
            Properties jvm = getProperties(ApplicationConfigScope.JVM);
            if (jvm != null) {
                jvm.putAll(System.getProperties());
            }
            // ENV
            Properties env = getProperties(ApplicationConfigScope.ENV);
            if (env != null) {
                env.putAll(System.getenv());
            }

            String filename = getConfigFileName();
            if (filename == null) {
                throw new ApplicationConfigFileNameNotInitializedException();
            }

            // classpath
            Properties classpath = getProperties(ApplicationConfigScope.CLASS_PATH);
            if (classpath != null) {
                Enumeration<URL> enumInClasspath = ClassLoader.getSystemClassLoader().getResources(filename);
                Set<URL> urlsInClasspath = new HashSet<>(EnumerationUtils.toList(enumInClasspath));
                enumInClasspath = ApplicationConfig.class.getClassLoader().getResources(filename);
                urlsInClasspath.addAll(EnumerationUtils.toList(enumInClasspath));
                log.debug(String.format("No configuration file found in classpath : /%s", filename));
                for (URL inClasspath : urlsInClasspath) {
                    log.info(String.format("Loading configuration file (classpath) : %s", inClasspath));
                    loadResource(inClasspath.toURI(), classpath);
                }
            }

            // system directory
            Properties etcFile = getProperties(ApplicationConfigScope.SYSTEM);
            if (etcFile != null) {
                File etcConfig = getSystemConfigFile();
                if (etcConfig.exists()) {
                    log.info(String.format("Loading configuration file (etc) : %s", etcConfig));
                    loadResource(etcConfig.toURI(), etcFile);
                } else {
                    log.debug(String.format("No configuration file found in system : %s", etcConfig.getAbsolutePath()));
                }
            }

            // user home directory
            Properties homeFile = getProperties(ApplicationConfigScope.HOME);
            if (homeFile != null) {
                File homeConfig = getUserConfigFile();
                log.debug(String.format("User configuration file : %s", homeConfig));
                if (homeConfig.exists()) {
                    log.info(String.format("Loading configuration file (home) : %s", homeConfig));
                    loadResource(homeConfig.toURI(), homeFile);
                } else {
                    log.debug(String.format("No configuration file found in user home : %s", homeConfig.getAbsolutePath()));
                }
            }

            // file $CURDIR/filename
            Properties curFile = getProperties(ApplicationConfigScope.CURRENT);
            if (curFile != null) {
                File config = getCurrentConfigFile();
                if (config.exists()) {
                    log.info(String.format("Loading configuration file (curr) : %s", config));
                    loadResource(config.toURI(), curFile);
                } else {
                    log.debug(String.format("No configuration file found in current directory : %s", config.getAbsolutePath()));
                }
            }

            // file extra
            Properties extraFile = getProperties(ApplicationConfigScope.EXTRA);
            if (extraFile != null) {
                String extraConfigDirectory = getExtraConfigDirectory();
                if (extraConfigDirectory != null) {
                    File config = getExtraConfigFile();
                    if (config.exists()) {
                        log.info(String.format("Loading configuration file (extra) : %s", config));
                        loadResource(config.toURI(), extraFile);
                    } else {
                        log.debug(String.format("No configuration file found in extra directory : %s", config.getAbsolutePath()));
                    }
                }
            }

            //
            // third parse action and do action
            //
            for (ListIterator<String> i = arguments.listIterator(); i.hasNext(); ) {
                String arg = i.next();
                if (arg.equals("--")) {
                    // stop parsing
                    break;
                }
                if (arg.startsWith("--")) {
                    String actionName = arg.substring(2);
                    Action action = createAction(actionName, i);
                    addAction(action);
                }
            }

            //
            // not used args added to unparsed
            //
            arguments.remove("--");
            unparsed.addAll(arguments);

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error(eee);
            }
            throw new ArgumentsParserException("Can't parse argument", eee);
        }
        return this;
    }

    /**
     * Fix all options of type File to respect Windows separator char which is {@code /}.
     *
     * @param options user options
     * @since 1.0.28
     */
    protected void fixFileOptionsForWindows(ConfigOptionDef[] options) {
        if (!isWindowsOs()) {
            return;
        }
        for (ConfigOptionDef option : options) {
            if (File.class.equals(option.getType())) {
                String value = option.getDefaultValue();
                if (value == null) {
                    log.warn("Option {} is null!", option);
                    continue;
                }
                String realValue = value.replaceAll("/", "\\\\");
                if (Objects.equals(value, realValue)) {
                    continue;
                }
                log.warn("Will transform for windows File option {} from {} to {}", option.getKey(), value, realValue);
                option.setDefaultValue(realValue);
            }
        }
    }

    /**
     * Load a resources given by his {@code uri} to the given
     * {@code properties} argument.
     *
     * @param uri        the uri to load
     * @param properties the properties file to load
     * @throws IOException if something occurs bad while loading resource
     * @see Properties#load(Reader)
     * @since 2.3
     */
    protected void loadResource(URI uri, Properties properties) throws IOException {
        Properties readProperties = configStorage.read(uri.toURL(), getCharset());
        if (log.isInfoEnabled()) {
            int size = readProperties.size();
            if (size == 0) {
                log.info("No property load.");
            } else {
                int index = 0;
                log.info(String.format("%d property(ies) loaded:", size));
                for (String key : readProperties.stringPropertyNames().stream().sorted().collect(Collectors.toList())) {

                    String property = readProperties.getProperty(key);
                    log.info(String.format("[ %1$-3d - %2$-3d] %3$s=%4$s", ++index, size, key, property));

                    String key2 = loadResourceProperty(key);
                    properties.put(key2, property);
                }
            }
        }
    }

    public void setPropertiesMigration(Map<String, String> propertiesMigration) {
        this.propertiesMigration = propertiesMigration;
    }

    protected String loadResourceProperty(String key) {
        if (propertiesMigration != null && propertiesMigration.containsKey(key)) {
            String newKey = propertiesMigration.get(key);
            log.warn(String.format("Migrate option %s to %s.", key, newKey));
            return newKey;
        }
        return key;
    }

    /**
     * Save the given {@code properties} into the given {@code file} with
     * the given {@code comment}.
     *
     * @param file       the location where to store the properties
     * @param properties the properties file to save
     * @param comment    the comment to add in the saved file
     * @see Properties#store(Writer, String)
     * @since 2.3
     */
    protected void saveResource(File file, Properties properties, String comment) {
        try {
            configStorage.write(properties, comment, file.toPath(), getCharset());
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    /**
     * For debugging.
     */
    public void printConfig() {
        System.out.println("-------------------Value-------------------------");
        printConfig(System.out);
        System.out.println("-------------------------------------------------");
    }

    /**
     * Print out current configuration in specified output.
     *
     * @param output output to write config to
     * @since 1.1.4
     */
    public void printConfig(PrintStream output) {
        for (Map.Entry<ApplicationConfigScope, Properties> entry : propertiesByScope.entrySet()) {
            output.println(entry.getKey().name() + " " + entry.getValue());
        }
    }

    /**
     * Return all configuration used with value, that respect includePattern
     *
     * @param includePattern null for all value, or config key pattern (ex: "wikitty.*")
     * @param padding        for better presentation, you can use padding to align '=' sign
     * @return string that represent config
     * @since 1.5.2
     */
    public String getPrintableConfig(String includePattern, int padding) {
        StringBuilder msg = new StringBuilder("Configuration:\n");
        for (String key : getFlatOptions().stringPropertyNames()) {
            if (includePattern == null || "".equals(includePattern) || key.matches(includePattern)) {
                String value = getOption(key);
                msg.append(String.format("\t%" + padding + "s = %s\n", key, value));
            }
        }
        return msg.toString();
    }

    protected void remove(String key, ApplicationConfigScope... scopes) {
        for (ApplicationConfigScope scope : scopes) {
            Properties properties = getProperties(scope);
            if (properties == null) {
                continue;
            }
            properties.remove(key);
        }
    }
}
