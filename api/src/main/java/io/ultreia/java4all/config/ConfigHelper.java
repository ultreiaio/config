package io.ultreia.java4all.config;

/*-
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ConfigOptionDef;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 21/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.25
 */
public class ConfigHelper {

    private static final Logger log = LogManager.getLogger(ConfigHelper.class);

    public static void save(ApplicationConfig applicationConfig, File file, Collection<String> doNotSave, ConfigResource resource, ConfigOptionDef[] options) {
        log.info(String.format("Save configuration to %s", file));
        Map<String, String> propertiesToSave = computePropertiesToSave(applicationConfig, doNotSave, options);
        List<String> propertyNames = propertiesToSave.keySet().stream().sorted().collect(Collectors.toList());

        log.info(String.format("Property names to save: %s", propertyNames));
        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath())) {
            List<String> lines = resource.readLines();
            for (String line : lines) {
                if (line.startsWith("#")) {
                    // we can only change an option if it was not already modified
                    for (String propertyName : propertyNames) {
                        if (line.startsWith("#" + propertyName + "=")) {
                            line = String.format("%s=%s", propertyName, propertiesToSave.get(propertyName));
                        }
                    }
                }
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new ApplicationConfigSaveException(e);
        }
    }

    private static Map<String, String> computePropertiesToSave(ApplicationConfig applicationConfig, Collection<String> doNotSave, ConfigOptionDef[] options) {
        Map<String, String> propertiesToSave = new LinkedHashMap<>();
        for (ConfigOptionDef option : options) {
            if (option.isTransient() || option.isFinal()) {
                // never save transient nor final option
                continue;
            }
            String optionKey = option.getKey();
            if (doNotSave.contains(optionKey)) {
                // not saving this one
                continue;
            }
            String optionValue = applicationConfig.getOption(optionKey);
            String optionDefaultValue = option.getDefaultValue();
            if (!Objects.equals(optionValue, optionDefaultValue) && !Objects.equals(optionValue, applicationConfig.replaceRecursiveOptions(optionDefaultValue))) {
                // the option value was modified
                if (option.getType().equals(File.class) && applicationConfig.isWindowsOs()) {
                    // See https://gitlab.com/ultreiaio/config/-/issues/44
                    optionValue = optionValue.replaceAll("\\\\", "\\\\\\\\");
                }
                propertiesToSave.put(optionKey, optionValue);
                log.debug("Option {} was modified (value: {})", optionKey, optionValue);
            }
        }
        return propertiesToSave;
    }

}
