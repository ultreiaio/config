package io.ultreia.java4all.config;

/*-
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 19/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.21
 */
public class ApplicationConfigReflectionHelper {
    private static final Logger log = LogManager.getLogger(ApplicationConfigReflectionHelper.class);

    /**
     * Invoke constructor on clazz to create new instance. Try to find argument
     * for constructor in args parameter.
     *
     * @param <E>           FIXME
     * @param clazz         class of object to instantiate
     * @param args          all possible parameter that constructor can used
     * @param nullIfMissing if no suitable class or object found in args,
     *                      use null value (no exception)
     * @return new instance
     * @throws IllegalArgumentException if something is wrong during instantiation
     */
    public static <E> E newInstance(Class<E> clazz, Collection<?> args, boolean nullIfMissing) {
        Constructor<E>[] constructors = (Constructor<E>[]) clazz.getConstructors();
        if (constructors.length != 1) {
            throw new IllegalArgumentException(String.format("Your class %s has more than one constructor", clazz));
        }
        // copy collection into modifiable list to add new object
        List<?> container = new LinkedList(args);
        Constructor<E> constructor = constructors[0];
        Class<?>[] paramTypes = constructor.getParameterTypes();
        Object[] params = new Object[paramTypes.length];
        for (int i = 0; i < paramTypes.length; i++) {
            Object o = choiceArgument(paramTypes[i], container, nullIfMissing);
            params[i] = o;
        }
        try {
            return constructor.newInstance(params);
        } catch (Exception eee) {
            throw new IllegalArgumentException(String.format("Class %s can't be instantiated with %s", clazz, Arrays.toString(params)), eee);
        }
    }

    /**
     * Permet de matcher un type d'argument attendu clazz parmi un ensemble
     * possible de candidat. Les candidats peuvent etre des classes qu'il faudra
     * instancier pour satisfaire le type demande.
     *
     * @param clazz         le type recherché
     * @param args          la liste des arguments ou des types
     * @param nullIfMissing pour retourner nulle si l'argument n'est pas trouvé
     * @return le type d'argument trouvé
     */
    public static Object choiceArgument(Class<?> clazz, List args, boolean nullIfMissing) {
        Object result = null;
        boolean addResult = false;
        for (Object o : args) {
            if (o != null) {
                if (o instanceof Class<?> && clazz.isAssignableFrom((Class<?>) o)) {
                    // cas on l'on trouve une class dans arg qui une fois instancier convient
                    result = newInstance((Class<?>) o, args, nullIfMissing);
                    addResult = true;
                    break;
                } else if (clazz.isInstance(o)) {
                    // cas on l'on retrouve un objet assignable pour ce type
                    result = o;
                    break;
                }
            }
        }
        if (addResult) {
            // on ajoute en tete pour qu'il soit retrouve dans les premiers
            // et non pas reinstancier une nouvelle fois si on en a besoin a nouveau
            args.add(0, result);
        }
        // si on ne retrouve rien, result est reste a null
        if (result == null && !nullIfMissing) {
            throw new IllegalArgumentException(String.format("Can't find assignable argument for %s in %s", clazz, args));
        }
        return result;
    }

    /**
     * Get all methods with name given in argument without check parameters.
     *
     * @param clazz      where to search method
     * @param methodName method name to search
     * @param ignoreCase if true, ignore difference in method name case
     * @return list of detected methods
     */
    public static List<Method> getMethod(Class<?> clazz, String methodName, boolean ignoreCase) {
        List<Method> result = new ArrayList<>();
        Method[] methods = clazz.getMethods();
        for (Method m : methods) {
            if (ignoreCase && methodName.equalsIgnoreCase(m.getName()) || methodName.equals(m.getName())) {
                result.add(m);
            }
        }
        return result;
    }

    /**
     * List method that match name, name must be [package.][class][#][method]
     * if package, class or method missing, exception throw
     *
     * @param name       name of the method
     * @param ignoreCase check exact method name if false
     * @return list of method that match name
     * @since 2.6.9
     */
    public static List<Method> getMethod(String name, boolean ignoreCase) {
        Class<?> clazz;
        String className;
        String methodName;
        // looking for method name
        int sep = name.lastIndexOf(ApplicationConfig.CLASS_METHOD_SEPARATOR);
        if (sep == -1) {
            throw new IllegalArgumentException(String.format("Can't find method in %s", name));
        } else {
            className = name.substring(0, sep);
            methodName = name.substring(sep + 1);
        }
        // looking for class name
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException eee) {
            throw new IllegalArgumentException(String.format("Can't find class %s", className));
        }
        return getMethod(clazz, methodName, ignoreCase);
    }

    /**
     * Call method m with params as String. Each param is converted to required type for
     * method with beanutils converter
     *
     * @param o      object where method must be call
     * @param m      method to call
     * @param params parameters for method call
     * @return returned method's value
     * @throws IllegalAccessException    FIXME
     * @throws IllegalArgumentException  FIXME
     * @throws InvocationTargetException FIXME
     * @throws InstantiationException    FIXME
     */
    public static Object call(Object o, Method m, String... params) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
        Class<?>[] types = m.getParameterTypes();
        if (!m.isVarArgs() && params.length != types.length) {
            throw new IllegalArgumentException(String.format("Bad number params we have %1$s parameters and waiting %2$s.", params.length, types.length));
        }
        int last = types.length;
        if (m.isVarArgs()) {
            // on traite le dernier differement
            last--;
        }
        Object[] parameters = new Object[types.length];
        for (int i = 0; i < last; i++) {
            String v = params[i];
            Class<?> clazz = types[i];
            Object t = convert(v, clazz);
            parameters[i] = t;
        }
        if (m.isVarArgs()) {
            Class<?> clazz = types[last]; // get var args type
            clazz = clazz.getComponentType(); // get array component type
            List<Object> tmp = new ArrayList<Object>();
            for (int i = last; i < params.length; i++) {
                String v = params[i];
                Object t = convert(v, clazz);
                tmp.add(t);
            }
            parameters[last] = tmp.toArray((Object[]) Array.newInstance(clazz, tmp.size()));
        }
        log.debug(String.format("Invoke %s with %s", m, Arrays.toString(parameters)));
        return m.invoke(o, parameters);
    }

    public static Object convert(String v, Class<?> clazz) {
        Object t = ConvertUtils.convert(v, clazz);
        if (t != null && !String.class.getName().equals(clazz.getName()) && String.class.getName().equals(t.getClass().getName())) {
            throw new IllegalArgumentException(String.format("Can convert argument to correct type. %s can't be" + " converted from String to %s conversion is done to %s", v, clazz.getName(), t.getClass().getName()));
        }
        return t;
    }
}
