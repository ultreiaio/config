//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package io.ultreia.java4all.config;

/*-
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.nuiton.converter.NuitonConverter;

//FIXME No more working ? hard to understand why ? can not instance processor
//FIXME Remove NuitonConverter API
//@AutoService(NuitonConverter.class)
public class VersionConverter implements NuitonConverter<Version> {
    public VersionConverter() {
    }

    public Class<Version> getType() {
        return Version.class;
    }

    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new IllegalArgumentException(String.format("No value specified for converter %s", this));
        } else {
            if (this.isEnabled(aClass)) {
                if (this.isEnabled(value.getClass())) {
                    return aClass.cast(value);
                }

                if (value instanceof String) {
                    try {
                        Object result = Version.create((String) value).build();
                        return aClass.cast(result);
                    } catch (IllegalArgumentException var5) {
                        throw new IllegalArgumentException(String.format("Could not convert version %1$s with converter %2$s for reason \\: %3$s", value, this, var5.getMessage()), var5);
                    }
                }
            }
            throw new IllegalArgumentException(String.format("no convertor found for type %2$s and objet '%1$s'", aClass.getName(), value));
        }
    }

    protected boolean isEnabled(Class<?> aClass) {
        return Version.class.equals(aClass);
    }
}
