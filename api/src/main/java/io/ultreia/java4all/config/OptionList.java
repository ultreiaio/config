package io.ultreia.java4all.config;

/*-
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 19/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since
 */
@SuppressWarnings("unused")
public class OptionList {

    protected ApplicationConfig config;

    protected String key;

    protected String value;

    public OptionList(ApplicationConfig config, String key, String value) {
        this.config = config;
        this.key = key;
        this.value = value;
    }

    @SuppressWarnings("unchecked")
    protected <T> List<T> convertListOption(Class<T> type) {
        return (List<T>) config.convertOption(type, key, value, true);
    }

    /**
     * Get option value as {@link String}.
     *
     * @param <T>  required type
     * @param type required type
     * @return value as given type
     */
    public <T> List<T> getOption(Class<T> type) {
        return convertListOption(type);
    }

    /**
     * Get option value as {@link String}.
     *
     * @return value as String
     */
    public List<String> getOption() {
        return convertListOption(String.class);
    }

    /**
     * Get option value as {@link File}.
     *
     * @return value as file
     */
    public List<File> getOptionAsFile() {
        List<File> tmp = convertListOption(File.class);
        List<File> result = new ArrayList<>(tmp.size());
        for (File file : tmp) {
            result.add(file.getAbsoluteFile());
        }
        return result;
    }

    /**
     * Get option value as {@link URL}.
     *
     * @return value as URL
     */
    public List<URL> getOptionAsURL() {
        return convertListOption(URL.class);
    }

    /**
     * Get option value as {@link Class}.
     *
     * @return value as Class
     */
    public List<Class> getOptionAsClass() {
        return convertListOption(Class.class);
    }

    /**
     * Get option value as {@link Date}.
     *
     * @return value as Date
     */
    public List<Date> getOptionAsDate() {
        return convertListOption(Date.class);
    }

    /**
     * Get option value as {@link Time}.
     *
     * @return value as Time
     */
    public List<Time> getOptionAsTime() {
        return convertListOption(Time.class);
    }

    /**
     * Get option value as {@link Timestamp}.
     *
     * @return value as Timestamp
     */
    public List<Timestamp> getOptionAsTimestamp() {
        return convertListOption(Timestamp.class);
    }

    /**
     * Get option value as {@code int}.
     *
     * @return value as {@code int}
     */
    public List<Integer> getOptionAsInt() {
        return convertListOption(Integer.class);
    }

    /**
     * Get option value as {@code double}.
     *
     * @return value as {@code double}
     */
    public List<Double> getOptionAsDouble() {
        return convertListOption(Double.class);
    }

    /**
     * Get option value as {@code boolean}.
     *
     * @return value as {@code boolean}.
     */
    public List<Boolean> getOptionAsBoolean() {
        return convertListOption(Boolean.class);
    }
}
