package io.ultreia.java4all.config;

/*
 * #%L
 * Config :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ConfigOptionDef;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 7/30/14.
 *
 * Check fix of https://forge.nuiton.org/issues/3292
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ChangeApplicationNameTest {

    private enum WaoApplicationConfigOption implements ConfigOptionDef {
        INSTANCE_URL("wao.instanceUrl",
                     "URL à laquelle on peut accéder à l'instance de WAO",
                     "http://localhost:8080/wao", String.class);

        private final String key;

        private final String description;

        private final Class<?> type;

        private String defaultValue;

        WaoApplicationConfigOption(String key, String description,
                                           String defaultValue, Class<?> type) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isTransient() {
            return false;
        }

        @Override
        public boolean isFinal() {
            return false;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public void setTransient(boolean isTransient) {
            // Nothing to do
        }

        @Override
        public void setFinal(boolean isFinal) {
            // Nothing to do
        }

    }

    @Test
    public void changeApplicationName() throws ArgumentsParserException {

        ApplicationConfig applicationConfig = new ApplicationConfig();

        // to allow using wao.config.path environment variable
        applicationConfig.setAppName("wao");

        applicationConfig.loadDefaultOptions(WaoApplicationConfigOption.values());
        applicationConfig.setConfigFileName("wao.properties");

        applicationConfig.parse();

        // This option is overridden by the config file
        String option = applicationConfig.getOption(String.class, WaoApplicationConfigOption.INSTANCE_URL.key);
        Assert.assertEquals("http://localhost:9090/wao", option);
    }
}
