# Config changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-07-04 10:38.

## Version [1.0.29](https://gitlab.com/ultreiaio/config/-/milestones/30)

**Closed at 2024-07-04.**


### Issues
  * [[bug 44]](https://gitlab.com/ultreiaio/config/-/issues/44) **Fix ConfigHelper.computePropertiesToSave method for windows (need to double the \ character)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 45]](https://gitlab.com/ultreiaio/config/-/issues/45) **To fix windows File options, recompute default value and nothing else** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 46]](https://gitlab.com/ultreiaio/config/-/issues/46) **ApplicationConfig.getOsName should use the dedicate field (and not an option)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.28](https://gitlab.com/ultreiaio/config/-/milestones/29)

**Closed at 2024-07-03.**


### Issues
  * [[bug 42]](https://gitlab.com/ultreiaio/config/-/issues/42) **Fix options of type File on Windows** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 43]](https://gitlab.com/ultreiaio/config/-/issues/43) **Add more informations about os in GeneratedConfigSupport.getConfigurationDescription method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.27](https://gitlab.com/ultreiaio/config/-/milestones/28)

**Closed at 2023-08-17.**


### Issues
  * [[enhancement 41]](https://gitlab.com/ultreiaio/config/-/issues/41) **Review io.ultreia.java4all.config.ConfigHelper#save method to produce a configuration file with all his modifications** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.26](https://gitlab.com/ultreiaio/config/-/milestones/27)

**Closed at 2023-08-17.**


### Issues
  * [[enhancement 40]](https://gitlab.com/ultreiaio/config/-/issues/40) **Open io.ultreia.java4all.config.ApplicationConfig#putAll method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.25](https://gitlab.com/ultreiaio/config/-/milestones/26)

**Closed at 2022-12-22.**


### Issues
  * [[bug 38]](https://gitlab.com/ultreiaio/config/-/issues/38) **Introduce CleanTemporaryFilesTask (application level for client or server)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 38]](https://gitlab.com/ultreiaio/config/-/issues/38) **Introduce CleanTemporaryFilesTask (application level for client or server)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 39]](https://gitlab.com/ultreiaio/config/-/issues/39) **Introduce ConfigHelper** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.24](https://gitlab.com/ultreiaio/config/-/milestones/25)

**Closed at 2022-08-22.**


### Issues
  * [[bug 37]](https://gitlab.com/ultreiaio/config/-/issues/37) **ApplicationConfig.setOption does not remove previous values from all scope when new option value is the default one, but just in the first scope found...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.23](https://gitlab.com/ultreiaio/config/-/milestones/24)

**Closed at 2022-01-26.**


### Issues
  * [[bug 35]](https://gitlab.com/ultreiaio/config/-/issues/35) **save method does not remove extra options** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 36]](https://gitlab.com/ultreiaio/config/-/issues/36) **Open GeneratedConfigSupport to be able to customize delegate ApplicationConfig** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.22](https://gitlab.com/ultreiaio/config/-/milestones/23)

**Closed at 2021-12-19.**


### Issues
  * [[bug 34]](https://gitlab.com/ultreiaio/config/-/issues/34) **Fix ApplicationConfig split in generated files...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.21](https://gitlab.com/ultreiaio/config/-/milestones/22)

**Closed at 2021-12-19.**


### Issues
  * [[enhancement 32]](https://gitlab.com/ultreiaio/config/-/issues/32) **Improve generate documentation mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 33]](https://gitlab.com/ultreiaio/config/-/issues/33) **Please remove from ApplicationConfig all static classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.20](https://gitlab.com/ultreiaio/config/-/milestones/21)

**Closed at 2021-12-18.**


### Issues
No issue.

## Version [1.0.19](https://gitlab.com/ultreiaio/config/-/milestones/20)

**Closed at *In progress*.**


### Issues
  * [[enhancement 30]](https://gitlab.com/ultreiaio/config/-/issues/30) **Improve generated documentation and example** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.18](https://gitlab.com/ultreiaio/config/-/milestones/19)

**Closed at 2021-12-10.**


### Issues
  * [[enhancement 27]](https://gitlab.com/ultreiaio/config/-/issues/27) **Improve generated API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 28]](https://gitlab.com/ultreiaio/config/-/issues/28) **Review IO API nearly from scratch** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.17](https://gitlab.com/ultreiaio/config/-/milestones/18)

**Closed at 2021-12-07.**


### Issues
  * [[enhancement 26]](https://gitlab.com/ultreiaio/config/-/issues/26) **Introduce GeneratedConfigSupport and improve Generated Config files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.16](https://gitlab.com/ultreiaio/config/-/milestones/17)

**Closed at 2021-12-06.**


### Issues
  * [[enhancement 25]](https://gitlab.com/ultreiaio/config/-/issues/25) **improve generate documentation mojos + add project.version.stable properties in generate mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.15](https://gitlab.com/ultreiaio/config/-/milestones/16)

**Closed at 2021-12-06.**


### Issues
  * [[enhancement 24]](https://gitlab.com/ultreiaio/config/-/issues/24) **Can inject in option default value some maven project properties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.14](https://gitlab.com/ultreiaio/config/-/milestones/15)

**Closed at 2021-11-13.**


### Issues
  * [[enhancement 23]](https://gitlab.com/ultreiaio/config/-/issues/23) **Replace nuiton-utils and nuiton-version by java-util** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.13](https://gitlab.com/ultreiaio/config/-/milestones/14)

**Closed at 2021-08-27.**


### Issues
  * [[enhancement 21]](https://gitlab.com/ultreiaio/config/-/issues/21) **Move to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 22]](https://gitlab.com/ultreiaio/config/-/issues/22) **Add new mojo to generate markdown documentation and example file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.12](https://gitlab.com/ultreiaio/config/-/milestones/13)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.11](https://gitlab.com/ultreiaio/config/-/milestones/12)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.10](https://gitlab.com/ultreiaio/config/-/milestones/11)

**Closed at 2018-11-18.**


### Issues
  * [[bug 19]](https://gitlab.com/ultreiaio/config/-/issues/19) **Fix report generation when not using i18n** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.9](https://gitlab.com/ultreiaio/config/-/milestones/10)

**Closed at 2018-11-18.**


### Issues
  * [[enhancement 18]](https://gitlab.com/ultreiaio/config/-/issues/18) **Make config reports works again and make goals threadsafe** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.8](https://gitlab.com/ultreiaio/config/-/milestones/9)

**Closed at *In progress*.**


### Issues
  * [[bug 15]](https://gitlab.com/ultreiaio/config/-/issues/15) **Fix new usage of I18n (got a NPE...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 16]](https://gitlab.com/ultreiaio/config/-/issues/16) **Clean code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 17]](https://gitlab.com/ultreiaio/config/-/issues/17) **Rename config.useNuitonI18n to config.useI18n** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.7](https://gitlab.com/ultreiaio/config/-/milestones/8)

**Closed at *In progress*.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/config/-/issues/14) **Use new I18n api to detect keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.6](https://gitlab.com/ultreiaio/config/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.5](https://gitlab.com/ultreiaio/config/-/milestones/6)

**Closed at 2018-07-31.**


### Issues
  * [[enhancement 13]](https://gitlab.com/ultreiaio/config/-/issues/13) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.4](https://gitlab.com/ultreiaio/config/-/milestones/5)

**Closed at *In progress*.**


### Issues
  * [[enhancement 12]](https://gitlab.com/ultreiaio/config/-/issues/12) **Permits to use gson to persist some options (using in type gson: prefix)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/ultreiaio/config/-/milestones/4)

**Closed at *In progress*.**


### Issues
  * [[enhancement 10]](https://gitlab.com/ultreiaio/config/-/issues/10) **Let&#39;s use a getter file in generate mojo (using i18n-api)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 11]](https://gitlab.com/ultreiaio/config/-/issues/11) **No more use ApplicationConfig.getOptionAsObject as it is really a mess (and deprecates those methods)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/config/-/milestones/3)

**Closed at 2018-04-15.**


### Issues
  * [[bug 8]](https://gitlab.com/ultreiaio/config/-/issues/8) **Can&#39;t save in current** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 9]](https://gitlab.com/ultreiaio/config/-/issues/9) **Improve getConfigurationDescription method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/config/-/milestones/2)

**Closed at 2018-04-15.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/config/-/issues/3) **Can use yaml and toml format in ApplicationConfigInit** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 4]](https://gitlab.com/ultreiaio/config/-/issues/4) **Remove deprecated code from ApplicationConfig** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 5]](https://gitlab.com/ultreiaio/config/-/issues/5) **Can save config in current directory** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 6]](https://gitlab.com/ultreiaio/config/-/issues/6) **Improve io API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 7]](https://gitlab.com/ultreiaio/config/-/issues/7) **Upgrade java to level 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/ultreiaio/config/-/milestones/1)

**Closed at 2018-04-14.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/config/-/issues/1) **Migrate to ultreia.io** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 2]](https://gitlab.com/ultreiaio/config/-/issues/2) **Implements toml and yaml ApplicaitonConfig format** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

