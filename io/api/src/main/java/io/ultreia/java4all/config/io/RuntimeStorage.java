package io.ultreia.java4all.config.io;

/*-
 * #%L
 * Config :: IO :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.io.StorageContract;

import java.util.Map;
import java.util.Properties;

/**
 * Created on 10/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.18
 */
public interface RuntimeStorage extends StorageContract<Properties> {

    /**
     * Cache.
     */
    SingletonSupplier<Map<String, RuntimeStorage>> CACHE = StorageContract.load(RuntimeStorage.class);

    /**
     * Get the {@link RuntimeStorage} for the given {@code format}.
     *
     * @param format format of storage to get
     * @return the found storage
     * @throws IllegalArgumentException if no storage found for the given format
     * @throws NullPointerException     if format is null
     */
    static RuntimeStorage get(String format) {
        return StorageContract.get(CACHE.get(), format);
    }

}
