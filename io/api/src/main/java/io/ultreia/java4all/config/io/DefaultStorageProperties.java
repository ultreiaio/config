package io.ultreia.java4all.config.io;

/*-
 * #%L
 * Config :: IO :: API
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

/**
 * Created on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
@AutoService(RuntimeStorage.class)
public class DefaultStorageProperties implements RuntimeStorage {
    @Override
    public String getFormat() {
        return "properties";
    }

    @Override
    public Properties read(Reader reader, String fileName) throws IOException {
        Properties properties = new Properties();
        properties.load(reader);
        return properties;
    }

    @Override
    public void write(Properties properties, String comment, BufferedWriter writer) throws IOException {
        properties.store(writer, comment);
    }
}
