package io.ultreia.java4all.config.io.toml;

/*-
 * #%L
 * Config :: IO :: Impl :: Toml
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.moandjiezana.toml.Toml;
import com.moandjiezana.toml.TomlWriter;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.io.SpiStorage;
import io.ultreia.java4all.config.spi.OptionModel;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;

/**
 * Implementation using {@code toml} format.
 * <p>
 * Created on 02/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@AutoService(SpiStorage.class)
public class SpiStorageToml implements SpiStorage {

    @Override
    public ConfigModel read(Reader reader, String fileName) {
        Toml toml = new Toml().read(reader);
        ConfigModel configModel = toml.to(ConfigModel.class);
        // re-set the type to get real java class and not your alias
        for (OptionModel optionModel : configModel.getOptions()) {
            optionModel.setType(optionModel.getType());
        }
        return configModel;
    }

    @Override
    public void write(ConfigModel configModel, String comment, BufferedWriter writer) throws IOException {
        new TomlWriter().write(configModel, writer);
    }

    @Override
    public String getFormat() {
        return "toml";
    }

}
