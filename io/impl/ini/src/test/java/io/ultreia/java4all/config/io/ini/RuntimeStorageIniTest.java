package io.ultreia.java4all.config.io.ini;

/*-
 * #%L
 * Config :: IO :: Impl :: Ini
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.io.tests.RuntimeStorageTest;

/**
 * Created by tchemit on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class RuntimeStorageIniTest extends RuntimeStorageTest {

    @Override
    public RuntimeStorageIni createConfigStorage() {
        return new RuntimeStorageIni();
    }

}
