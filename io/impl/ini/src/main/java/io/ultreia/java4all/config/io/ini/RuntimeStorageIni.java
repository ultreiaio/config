package io.ultreia.java4all.config.io.ini;

/*-
 * #%L
 * Config :: IO :: Impl :: Ini
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.ultreia.java4all.config.io.RuntimeStorage;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * Created on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
@AutoService(RuntimeStorage.class)
public class RuntimeStorageIni implements RuntimeStorage {

    @Override
    public String getFormat() {
        return "ini";
    }

    @Override
    public Properties read(Reader reader, String fileName) throws ConfigurationException, IOException {
        Properties properties = new Properties();
        INIConfiguration iniConfiguration = new INIConfiguration();
        iniConfiguration.read(reader);
        Iterator<String> keysIterator = iniConfiguration.getKeys();
        while (keysIterator.hasNext()) {
            String key = keysIterator.next();
            properties.put(key, iniConfiguration.getProperty(key));
        }
        for (String sectionName : iniConfiguration.getSections()) {
            if (sectionName == null) {
                continue;
            }
            SubnodeConfiguration section = iniConfiguration.getSection(sectionName);
            Iterator<String> sectionKeysIterator = section.getKeys();
            while (sectionKeysIterator.hasNext()) {
                String key = sectionKeysIterator.next();
                properties.put(sectionName + "." + key, section.getProperty(key));
            }
        }
        return properties;
    }

    @Override
    public void write(Properties properties, String comment, BufferedWriter writer) throws ConfigurationException, IOException {
        INIConfiguration iniConfiguration = new INIConfiguration();
        Multimap<String, String> splits = ArrayListMultimap.create();
        for (String key : properties.stringPropertyNames()) {
            List<String> split = new ArrayList<>(Arrays.asList(key.split("\\.")));
            if (split.size() == 1) {
                // simple property
                iniConfiguration.addProperty(key, properties.getProperty(key));
            } else {
                String lastKey = split.remove(split.size() - 1);
                splits.put(String.join(".", split), lastKey);
            }
        }
        for (String sectionName : splits.keySet()) {
            Collection<String> keyNames = splits.get(sectionName);
            if (keyNames.size() > 1) {
                // use section
                SubnodeConfiguration section = iniConfiguration.getSection(sectionName);
                for (String keyName : keyNames) {
                    section.addProperty(keyName, properties.getProperty(sectionName + "." + keyName));
                }
            } else {
                // simple property
                String keyName = keyNames.iterator().next();
                iniConfiguration.addProperty(sectionName + "." + keyName, properties.getProperty(sectionName + "." + keyName));
            }
        }
        iniConfiguration.write(writer);
    }
}
