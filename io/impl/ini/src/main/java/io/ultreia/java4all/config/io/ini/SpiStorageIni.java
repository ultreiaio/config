package io.ultreia.java4all.config.io.ini;

/*-
 * #%L
 * Config :: IO :: Impl :: Ini
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.config.io.SpiStorage;
import io.ultreia.java4all.config.spi.ActionModel;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.spi.OptionModel;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;

/**
 * Implementation using {@code ini} format.
 * <p>
 * Created on 02/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@AutoService(SpiStorage.class)
public class SpiStorageIni implements SpiStorage {

    private static final String DESCRIPTION = "description";
    private static final String KEY = "key";
    private static final String TYPE = "type";
    private static final String DEFAULT_VALUE = "defaultValue";
    private static final String TRANSIENT = "transient";
    private static final String FINAL = "final";
    private static final String ACTION = "action";
    private static final String ALIASES = "aliases";

    @Override
    public ConfigModel read(Reader reader, String fileName) throws ConfigurationException, IOException {
        INIConfiguration iniConfiguration = new INIConfiguration();
        iniConfiguration.read(reader);
        ConfigModel configModel = new ConfigModel();
        configModel.setName(fileName.substring(0, fileName.lastIndexOf('.')));
        configModel.setDescription(iniConfiguration.getString(DESCRIPTION));
        LinkedList<OptionModel> options = new LinkedList<>();
        LinkedList<ActionModel> actions = new LinkedList<>();
        configModel.setOptions(options);
        configModel.setActions(actions);
        for (String section : iniConfiguration.getSections()) {
            if (section == null) {
                continue;
            }
            if (section.startsWith("option ")) {
                SubnodeConfiguration optionSection = iniConfiguration.getSection(section);
                OptionModel optionModel = new OptionModel();
                String optionName = Strings.removeStart(section, "option ");
                optionModel.setName(optionName);
                String description = optionSection.getString(DESCRIPTION, "");
                optionModel.setDescription(description);
                String key = optionSection.getString(KEY);
                optionModel.setKey(key);
                String type = optionSection.getString(TYPE);
                optionModel.setType(type);
                String defaultValue = optionSection.getString(DEFAULT_VALUE);
                optionModel.setDefaultValue(defaultValue);
                String _transient = optionSection.getString(TRANSIENT);
                if (Strings.isNotEmpty(_transient)) {
                    optionModel.setTransient(Boolean.parseBoolean(_transient));
                }
                String _final = optionSection.getString(FINAL);
                if (Strings.isNotEmpty(_final)) {
                    optionModel.setFinal(Boolean.parseBoolean(_final));
                }
                options.add(optionModel);
            } else if (section.startsWith("action ")) {
                SubnodeConfiguration actionSection = iniConfiguration.getSection(section);
                ActionModel actionModel = new ActionModel();
                String actionName = Strings.removeStart(section, "action ");
                actionModel.setName(actionName);
                String description = actionSection.getString(DESCRIPTION, "");
                actionModel.setDescription(description);
                String action = actionSection.getString(ACTION);
                actionModel.setAction(action);
                String aliases = actionSection.getString(ALIASES);
                if (Strings.isNotEmpty(aliases)) {
                    actionModel.setAliases(aliases.split("\\s*,\\s*"));
                }
                actions.add(actionModel);
            }
        }
        return configModel;
    }

    @Override
    public void write(ConfigModel configModel, String comment, BufferedWriter writer) throws ConfigurationException, IOException {
        INIConfiguration iniConfiguration = new INIConfiguration();
        iniConfiguration.setCommentLeadingCharsUsedInInput(";");
        iniConfiguration.addProperty(DESCRIPTION, configModel.getDescription());
        for (OptionModel optionModel : configModel.getOptions()) {
            SubnodeConfiguration section = iniConfiguration.getSection("option " + optionModel.getName());
            String description = optionModel.getDescription();
            if (Strings.isNotEmpty(description)) {
                section.addProperty(DESCRIPTION, description);
            }
            section.addProperty(KEY, optionModel.getKey());
            section.addProperty(TYPE, (optionModel.isUseGson() ? "gson:" : "") + optionModel.getRawType());
            if (optionModel.getDefaultValue() != null) {
                section.addProperty(DEFAULT_VALUE, optionModel.getDefaultValue());
            }
            if (optionModel.isFinal()) {
                section.addProperty(FINAL, "true");
            }
            if (optionModel.isTransient()) {
                section.addProperty(TRANSIENT, "true");
            }
        }
        for (ActionModel actionModel : configModel.getActions()) {
            SubnodeConfiguration section = iniConfiguration.getSection("action " + actionModel.getName());
            String description = actionModel.getDescription();
            if (Strings.isNotEmpty(description)) {
                section.addProperty(DESCRIPTION, description);
            }
            section.addProperty(ACTION, actionModel.getAction());
            if (actionModel.getAliases().length > 0) {
                section.addProperty(ALIASES, String.join(",", actionModel.getAliases()));
            }
        }
        iniConfiguration.write(writer);
    }

    @Override
    public String getFormat() {
        return "ini";
    }

}
