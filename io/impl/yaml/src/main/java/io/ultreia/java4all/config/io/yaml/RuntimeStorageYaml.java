package io.ultreia.java4all.config.io.yaml;

/*-
 * #%L
 * Config :: IO :: Impl :: Yaml
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.google.auto.service.AutoService;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.ultreia.java4all.config.io.RuntimeStorage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
@AutoService(RuntimeStorage.class)
public class RuntimeStorageYaml implements RuntimeStorage {

    @Override
    public String getFormat() {
        return "yaml";
    }

    @Override
    public Properties read(Reader reader, String fileName) throws YamlException {
        Properties properties = new Properties();
        YamlReader yamlReader = new YamlReader(reader, createConfig());
        Map<?, ?> read = yamlReader.read(Map.class);
        for (Map.Entry<?, ?> o : read.entrySet()) {
            Object key = o.getKey();
            Object value = o.getValue();
            if (value instanceof Map) {
                // section
                for (Map.Entry<?, ?> subEntry : ((Map<?, ?>) value).entrySet()) {
                    properties.put(key + "." + subEntry.getKey(), subEntry.getValue());
                }
            } else {
                // simple property
                properties.put(key, value);
            }
        }
        return properties;
    }

    @Override
    public void write(Properties properties, String comment, BufferedWriter writer) throws IOException {
        Multimap<String, String> splits = ArrayListMultimap.create();
        Map<String, Object> map = new LinkedHashMap<>();
        for (String key : properties.stringPropertyNames()) {
            List<String> split = new ArrayList<>(Arrays.asList(key.split("\\.")));
            if (split.size() == 1) {
                // simple property
                map.put(key, properties.getProperty(key));
            } else {
                String lastKey = split.remove(split.size() - 1);
                splits.put(String.join(".", split), lastKey);
            }
        }
        for (String sectionName : splits.keySet()) {
            Collection<String> keyNames = splits.get(sectionName);
            if (keyNames.size() > 1) {
                Map<String, Object> subMap = new LinkedHashMap<>();
                map.put(sectionName, subMap);
                // section
                for (String keyName : keyNames) {
                    subMap.put(keyName, properties.getProperty(sectionName + "." + keyName));
                }
            } else {
                // simple property
                String keyName = keyNames.iterator().next();
                map.put(sectionName + "." + keyName, properties.getProperty(sectionName + "." + keyName));
            }
        }
        YamlWriter yamlWriter = new YamlWriter(writer, createConfig());
        yamlWriter.write(map);
        writer.flush();
        yamlWriter.close();
    }

    private YamlConfig createConfig() {
        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.writeConfig.setIndentSize(2);
        yamlConfig.writeConfig.setWriteRootTags(false);
        return yamlConfig;
    }
}
