package io.ultreia.java4all.config.io.tests;

/*-
 * #%L
 * Config :: IO :: Tests
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.io.SpiStorage;
import io.ultreia.java4all.config.spi.ConfigModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Created on 01/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public abstract class SpiStorageTest {

    private SpiStorage modelStorage;
    private ConfigModelStorageFixtures fixtures;

    @Before
    public void setUp() {
        modelStorage = createModelStorage();
        fixtures = new ConfigModelStorageFixtures();
    }

    public abstract SpiStorage createModelStorage();

    @Test
    public void read() {
        Path path = ConfigModelStorageFixtures.getResource(modelStorage, "spi");
        ConfigModel configModel = modelStorage.read(path);
        fixtures.assertConfigModel(configModel);
    }

    @Test
    public void write() {
        Path path = ConfigModelStorageFixtures.getResource(modelStorage, "spi");
        ConfigModel configModel = modelStorage.read(path);
        Assert.assertNotNull(configModel);

        Path path2 = ConfigModelStorageFixtures.getGeneratedResource(modelStorage, "spi");
        modelStorage.write(configModel, null, path2, StandardCharsets.UTF_8);

        ConfigModel configModel2 = modelStorage.read(path);
        fixtures.assertConfigModel(configModel2);
    }

}
