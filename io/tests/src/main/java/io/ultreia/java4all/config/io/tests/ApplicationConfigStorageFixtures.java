package io.ultreia.java4all.config.io.tests;

/*-
 * #%L
 * Config :: IO :: Tests
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.io.RuntimeStorage;
import org.junit.Assert;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by tchemit on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ApplicationConfigStorageFixtures {

    static void assertProperties(Properties properties) {
        Assert.assertNotNull(properties);
        Assert.assertNotNull(properties.getProperty("identity.firstName"));
        Assert.assertEquals("Joshua", properties.getProperty("identity.firstName"));
        Assert.assertEquals("Bloch", properties.getProperty("identity.lastName"));
        Assert.assertEquals("56", properties.getProperty("identity.age"));
        Assert.assertEquals("jbloch", properties.getProperty("identity.twitter"));
    }

    static Properties createFixtures() {
        Properties properties = new Properties();
        properties.put("identity.firstName", "Joshua");
        properties.put("identity.lastName", "Bloch");
        properties.put("identity.age", "56");
        properties.put("identity.twitter", "jbloch");
        return properties;
    }

    public static Path getResource(RuntimeStorage io, String classifier) {
        return Paths.get(new File("").getAbsolutePath(), "src", "test", "resources", io.getFilename(classifier));
    }

    public static Path getGeneratedResource(RuntimeStorage io, String classifier) {
        return Paths.get(new File("").getAbsolutePath(), "target", "surefire-workdir", io.getFilename(classifier + "-" + System.nanoTime()));
    }
}
