package io.ultreia.java4all.config.io.tests;

/*-
 * #%L
 * Config :: IO :: Tests
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.io.RuntimeStorage;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Created by tchemit on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class RuntimeStorageTest {

    public abstract RuntimeStorage createConfigStorage();

    @Test
    public void readProperties() {
        RuntimeStorage io = createConfigStorage();
        Path file = ApplicationConfigStorageFixtures.getResource(io, "runtime");
        Properties properties = io.read(file);
        ApplicationConfigStorageFixtures.assertProperties(properties);
    }

    @Test
    public void readPropertiesFromHelper() {
        RuntimeStorage io = RuntimeStorage.get(createConfigStorage().getFormat());
        Path file = ApplicationConfigStorageFixtures.getResource(io, "runtime");
        Properties properties = io.read(file);
        ApplicationConfigStorageFixtures.assertProperties(properties);
    }

    @Test
    public void writeProperties() {
        RuntimeStorage io = createConfigStorage();
        Properties properties = ApplicationConfigStorageFixtures.createFixtures();

        Path file = ApplicationConfigStorageFixtures.getGeneratedResource(io, "runtime");
        io.write(properties, null, file, StandardCharsets.UTF_8);

        Properties properties2 = createConfigStorage().read(file);
        ApplicationConfigStorageFixtures.assertProperties(properties2);
    }

    @Test
    public void writePropertiesFromHelper() {
        Properties properties = ApplicationConfigStorageFixtures.createFixtures();

        RuntimeStorage io = RuntimeStorage.get(createConfigStorage().getFormat());
        Path file = ApplicationConfigStorageFixtures.getGeneratedResource(io, "runtime");
        io.write(properties, null, file, StandardCharsets.UTF_8);

        Properties properties2 = createConfigStorage().read(file);
        ApplicationConfigStorageFixtures.assertProperties(properties2);
    }
}
