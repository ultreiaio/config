package io.ultreia.java4all.config.plugin;

/*
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ApplicationConfigProvider;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.i18n.runtime.boot.I18nBootLoader;
import io.ultreia.java4all.i18n.spi.I18nModuleDefinition;
import io.ultreia.java4all.lang.Strings;
import org.apache.maven.RepositoryUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.doxia.siterenderer.Renderer;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.AbstractMavenReport;
import org.apache.maven.reporting.MavenReportException;
import org.codehaus.plexus.i18n.I18N;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.collection.CollectResult;
import org.eclipse.aether.graph.DependencyNode;
import org.eclipse.aether.impl.ArtifactResolver;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Abstract application config report used by normal and aggregate report.
 *
 * @author Tony - dev@tchemit.fr
 * @since 3.0
 */
abstract class ReportMojoSupport extends AbstractMavenReport {

    /**
     * Report output directory. Note that this parameter is only relevant if the goal is run from the command line or
     * from the default build lifecycle. If the goal is run indirectly as part of a site generation, the output
     * directory configured in the Maven Site Plugin is used instead.
     */
    @Parameter(property = "config.outputDirectory", defaultValue = "${project.reporting.outputDirectory}", required = true)
    private File outputDirectory;

    /**
     * List of application config to include in report (separated by comma).
     * <p>
     * <strong>Note:</strong> If not filled then will use all config found in class-path.
     */
    @Parameter(property = "config.include")
    private String include;

    /**
     * List of application config to exclude from report (separated by comma).
     * <p>
     * <strong>Note:</strong> If not filled no config will be exclude.
     */
    @Parameter(property = "config.exclude")
    private String exclude;

    /**
     * Flag to activate verbose mode.
     * <p>
     * <b>Note:</b> Verbose mode is always on if you starts a debug maven instance
     * (says via {@code -X}).
     */
    @Parameter(property = "config.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * To use nuiton-i18n API to translate descriptions in generate code.
     */
    @Parameter(property = "config.useI18n")
    private boolean useI18n;
    /**
     * Skip to generate the report.
     */
    @Parameter(property = "config.skip")
    private boolean skip;

    /**
     * The Maven Project.
     */
    @Parameter(defaultValue = "${project}", required = true)
    private MavenProject project;

    /**
     * Doxia Site Renderer component.
     */
    @Component
    private Renderer siteRenderer;

    /**
     * Internationalization component.
     */
    @Component
    private I18N i18n;

    /**
     * Class loader with all compile dependencies of the module in class-path
     * (used to init i18n) and obtain config provider over compile class-path.
     */
    private ClassLoader newClassLoader;

    /**
     * Set of ApplicationConfigProvider detected from configuration.
     */
    private Set<ApplicationConfigProvider> configProviders;

    @Parameter(defaultValue = "${session}", readonly = true)
    private MavenSession session;

    @Component
    private RepositorySystem repositorySystem;
    @Component
    private ArtifactResolver resolver;

    @Override
    public String getOutputName() {
        return "config-report";
    }

    public String getDescription(Locale locale) {
        return i18n.getString(getOutputName(), locale, "report.description");
    }

    public String getName(Locale locale) {
        return i18n.getString(getOutputName(), locale, "report.title");
    }

    @Override
    public String getCategoryName() {
        return CATEGORY_PROJECT_REPORTS;
    }

    @Override
    public boolean canGenerateReport() {
        return !skip;
    }

    @Override
    protected Renderer getSiteRenderer() {
        return siteRenderer;
    }

    @Override
    protected String getOutputDirectory() {
        return outputDirectory.getAbsolutePath();
    }

    @Override
    protected MavenProject getProject() {
        return project;
    }

    protected abstract ClassLoader createClassLoader() throws MavenReportException;

    @Override
    protected void executeReport(Locale locale) throws MavenReportException {

        if (newClassLoader == null) {

            // not init
            init();
        }

        if (useI18n) {
            // get i18n initializer
            I18nConfiguration defaultConfiguration = I18nConfiguration.createDefaultConfiguration();
            defaultConfiguration.setClassLoader(newClassLoader);
            I18nBootLoader initializer = new DefaultI18nBootLoader(defaultConfiguration);
            // init i18n
            try {
                I18n.init(initializer, locale);
            } catch (Error e) {
                e.printStackTrace();
            }

        }
        ReportRenderer renderer = new ReportRenderer(getSink(),
                                                     i18n,
                                                     locale,
                                                     getOutputName(),
                                                     getOutputName(),
                                                     useI18n,
                                                     configProviders);
        renderer.render();
    }

    private void init() throws MavenReportException {
        if (getLog().isDebugEnabled()) {

            // debug mode set verbose flag to true
            verbose = true;
        }

        if (newClassLoader == null) {

            newClassLoader = createClassLoader();
        }

        if (configProviders == null) {

            Set<String> includes = null;
            if (Strings.isNotEmpty(include)) {
                includes = new HashSet<>(Arrays.asList(include.split("\\s*,\\s*")));
                if (verbose) {
                    getLog().info("config - includes : " + includes);
                }
            }

            Set<String> excludes = null;
            if (Strings.isNotEmpty(exclude)) {
                excludes = new HashSet<>(Arrays.asList(exclude.split("\\s*,\\s*")));
                if (verbose) {
                    getLog().info("config - excludes : " + excludes);
                }
            }

            configProviders = ApplicationConfigProvider.getProviders(
                    newClassLoader,
                    includes,
                    excludes,
                    verbose
            );
        }
    }

    /**
     * Init mojo classLoader.
     *
     * @param projects the maven projects
     * @return the new classLoader
     */
    @SuppressWarnings({"unchecked"})
    URLClassLoader initClassLoader(Collection<MavenProject> projects) {

        URLClassLoader result;
        try {

            Set<String> dones = new HashSet<>();
            List<URL> lUrls = new ArrayList<>();
            List<Artifact> projectArtifacts = projects.stream().map(MavenProject::getArtifact).collect(Collectors.toList());

            for (MavenProject project : projects) {

                RepositorySystemSession repositorySession = session.getRepositorySession();
                org.eclipse.aether.graph.Dependency dependency = RepositoryUtils.toDependency(project.getArtifact(), null);
                CollectResult collectResult = repositorySystem.collectDependencies(repositorySession, new CollectRequest(dependency, project.getRemotePluginRepositories()));
                LinkedHashSet<Artifact> artifacts1 = new LinkedHashSet<>();
                getArtifacts(collectResult.getRoot(), artifacts1);
                for (Artifact artifact : artifacts1) {
                    if (projectArtifacts.contains(artifact)) {
                        continue;
                    }
                    File file = artifact.getFile();
                    addDirectoryToUrlsList(file, lUrls, dones);
                }

                addDirectoryToUrlsList(new File(project.getBuild().getOutputDirectory()), lUrls, dones);
                for (String runtimeClasspathElement : project.getRuntimeClasspathElements()) {
                    addUrlToUrlsList(new File(runtimeClasspathElement).toURI().toURL(), lUrls, dones);
                }

//                getLog().info("use project compile scope class-path");
                // add also all dependencies of the project in compile
                // scope
                Set<?> artifacts = project.getArtifacts();
                for (Object o : artifacts) {
                    Artifact a = (Artifact) o;
                    addDirectoryToUrlsList(a.getFile(), lUrls, dones);
                }

            }
            result = new URLClassLoader(lUrls.toArray(new URL[0]), I18nModuleDefinition.class.getClassLoader());

        } catch (Exception e) {
            throw new RuntimeException("Can't create ClassLoader for reason " + e.getMessage(), e);
        }
        if (getLog().isDebugEnabled()) {
            for (URL entry : result.getURLs()) {
                getLog().debug("classpath : " + entry);
            }
        }
        return result;
    }

    private void getArtifacts(DependencyNode root, Set<Artifact> artifacts) throws ArtifactResolutionException {
        org.eclipse.aether.artifact.Artifact artifact = root.getArtifact();
        if (!"pom".equals(artifact.getExtension())) {
            if (artifact.getFile() == null) {
                ArtifactResult artifactResult = resolver.resolveArtifact(session.getRepositorySession(), new ArtifactRequest(root));
                artifact = artifactResult.getArtifact();
            }
            artifacts.add(RepositoryUtils.toArtifact(artifact));
        }
        for (DependencyNode child : root.getChildren()) {
            getArtifacts(child, artifacts);
        }

    }

    /**
     * Add the given {@code directory} in {@code urls} if not already included.
     *
     * <b>Note:</b> We use a extra list to store file string representation,
     * since we do NOT want any url resolution and the
     * {@link URL#equals(Object)} is doing some...
     *
     * @param directory the directory to insert in {@code urls}
     * @param urls      list of urls
     * @param done      list of string representation of urls
     * @throws MalformedURLException if pb while converting file to url
     * @since 1.1.0
     */
    protected void addDirectoryToUrlsList(
            File directory,
            List<URL> urls,
            Set<String> done) throws MalformedURLException {
        // do the comparaison on a String to avoid url to be resolved (in URL.equals method)
        addUrlToUrlsList(directory.toURI().toURL(), urls, done);
    }

    /**
     * Add the given {@code url} in {@code urls} if not already included.
     *
     * <b>Note:</b> We use a extra list to store file string representation,
     * since we do NOT want any url resolution and the
     * {@link URL#equals(Object)} is doing some..
     *
     * @param url  the url to insert in {@code urls}
     * @param urls list of urls
     * @param done list of string representation of urls
     * @since 1.1.0
     */
    protected void addUrlToUrlsList(URL url, List<URL> urls, Set<String> done) {
        // do the comparaison on a String to avoid url to be resolved (in URL.equals method)
        String u = url.toString();
        if (!done.contains(u)) {
            done.add(u);
            urls.add(url);
        }
    }

}
