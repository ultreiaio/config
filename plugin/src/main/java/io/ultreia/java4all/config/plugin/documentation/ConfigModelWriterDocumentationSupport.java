package io.ultreia.java4all.config.plugin.documentation;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.io.WriterContract;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.spi.OptionModel;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import org.codehaus.plexus.i18n.I18N;

import java.util.Locale;

/**
 * Created on 27/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.13
 */
public abstract class ConfigModelWriterDocumentationSupport implements WriterContract<ConfigModel> {
    /**
     * Internationalization component.
     */
    private final I18N i18n;

    /**
     * The locale we are rendering for.
     */
    private final Locale locale;

    /**
     * Are we using I18n API?
     */
    private final boolean useI18n;
    /**
     * The name of the bundle containing our I18n resources.
     */
    private final String bundleName;

    public ConfigModelWriterDocumentationSupport(I18N i18n, Locale locale, boolean useI18n, String bundleName) {
        this.i18n = i18n;
        this.locale = locale;
        this.useI18n = useI18n;
        this.bundleName = bundleName;
    }

    String getDescription(String key) {
        return useI18n ? I18n.l(locale, key) : key;
    }


    String getText(String key) {
        return i18n.getString(bundleName, locale, key);
    }

    String getText(boolean key) {
        return getText("report." + key);
    }


    String getDefaultValue(OptionModel option) {
        String defaultValue = option.getDefaultValue();
        if (Strings.isEmpty(defaultValue)) {
            defaultValue = getText("report.noDefaultValue");
        }
        return defaultValue;
    }

}
