package io.ultreia.java4all.config.plugin;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.plugin.documentation.ConfigModelWriterDocumentation;
import io.ultreia.java4all.config.plugin.documentation.ConfigModelWriterDocumentationSupport;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.plexus.i18n.I18N;

import java.util.Locale;

/**
 * To generate documentation from java files.
 * <p>
 * Result use {@code markdown} format.
 * <p>
 * Created on 27/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.13
 */
@Mojo(name = "generate-documentation", defaultPhase = LifecyclePhase.COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateDocumentationMojo extends GenerateDocumentationMojoSupport {
    @Override
    protected String getTargetFileExtension() {
        return "md";
    }

    @Override
    protected ConfigModelWriterDocumentationSupport createTemplate(I18N i18n, Locale locale, boolean useI18n) {
        return new ConfigModelWriterDocumentation(i18n, locale, useI18n);
    }
}
