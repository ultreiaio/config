package io.ultreia.java4all.config.plugin;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import io.ultreia.java4all.config.io.SpiStorage;
import io.ultreia.java4all.lang.Strings;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created on 02/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
abstract class ConfigMojoSupport extends AbstractPlugin {

    /**
     * Configuration description format {@code ini}, {@code toml} or {@code yaml}.
     * <p>
     * Default value is {@code ini}.
     */
    @Parameter(property = "config.format", defaultValue = "ini")
    private String format;

    /**
     * The path of model file.
     *
     * <p>Default value is </p>
     * <pre>${modelDirectory}/${modelName}.${format}</pre>
     */
    @Parameter(property = "config.modelFile")
    private File modelFile;

    /**
     * The source directory where to scan model file.
     */
    @Parameter(property = "config.modelDirectory", defaultValue = "${basedir}/src/main/config", required = true)
    private File modelDirectory;

    /**
     * Pour activer le mode verbeux.
     */
    @Parameter(property = "config.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * Maven project.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    private SpiStorage configStorage;

    @Override
    protected void init() throws Exception {
        if (isVerbose()) {
            getLog().info("Use format: " + format);
        }
        configStorage = SpiStorage.get(format);
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    SpiStorage getConfigStorage() {
        return configStorage;
    }

    String getFormat() {
        return format;
    }

    File getModelDirectory() {
        return modelDirectory;
    }

    File getModelFile() {
        return modelFile;
    }

    void setModelFile(String name) {
        setModelFile(name, format);
    }

    void setModelFile(String name, String format) {
        modelFile = PluginHelper.getFile(modelDirectory, name + "." + format);
    }


    protected void initModelName(String modelName, Consumer<String> setter) throws MojoExecutionException {
        if (modelName == null) {
            List<String> artifactIdPaths = new ArrayList<>();
            for (String artifactIdPath : getProject().getArtifactId().replaceAll("-", ".").split("\\.")) {
                artifactIdPaths.add(Strings.capitalize(artifactIdPath));
            }
            modelName = Joiner.on("").join(artifactIdPaths);
            setter.accept(modelName);
            if (isVerbose()) {
                getLog().info("Use model name: " + modelName);
            }
        }

        if (getModelFile() == null) {
            setModelFile(modelName);
        }

        File modelFile = getModelFile();
        if (!modelFile.exists()) {
            throw new MojoExecutionException("No model file found at: " + modelFile);
        }
    }

}
