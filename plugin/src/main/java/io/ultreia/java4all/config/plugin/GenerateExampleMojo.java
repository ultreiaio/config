package io.ultreia.java4all.config.plugin;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.plugin.documentation.ConfigModelWriterDocumentationSupport;
import io.ultreia.java4all.config.plugin.documentation.ConfigModelWriterExample;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.plexus.i18n.I18N;

import java.util.Locale;

/**
 * To generate an example properties file with all options and their default values (commented).
 * <p>
 * Created on 27/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.13
 */
@Mojo(name = "generate-example", defaultPhase = LifecyclePhase.COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateExampleMojo extends GenerateDocumentationMojoSupport {
    /**
     * Target file extension.
     */
    @Parameter(property = "config.example.targetFileExtension", defaultValue = "properties", required = true)
    protected String targetFileExtension;

    @Override
    protected String getTargetFileExtension() {
        return targetFileExtension;
    }

    @Override
    protected ConfigModelWriterDocumentationSupport createTemplate(I18N i18n, Locale locale, boolean useI18n) {
        return new ConfigModelWriterExample(i18n, locale, useI18n);
    }
}
