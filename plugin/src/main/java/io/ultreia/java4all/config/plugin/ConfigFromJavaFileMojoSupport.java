package io.ultreia.java4all.config.plugin;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ApplicationConfigProvider;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.lang.Strings;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Support for mojos that use generated java file to load {@link ConfigModel}.
 * <p>
 * Created on 27/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.13
 */
public abstract class ConfigFromJavaFileMojoSupport extends ConfigMojoSupport {

    ClassLoader classLoader;
    /**
     * The name of provider to describe.
     * <p>
     * By default, will use artifactId in camelCase format ( example: provider name for artifact id
     * {@code config-example} is {@code ConfigExample} ).
     *
     * @see ApplicationConfigProvider
     */
    @Parameter(property = "config.modelName")
    private String modelName;
    private ConfigModel configModel;
    private String providerName;

    @Override
    protected void init() throws Exception {

        super.init();

        if (modelName == null) {

            List<String> artifactIdPaths = Arrays.stream(getProject().getArtifactId().replaceAll("-", ".").split("\\.")).map(Strings::capitalize).collect(Collectors.toList());
            modelName = String.join("", artifactIdPaths);

        }

        if (isVerbose()) {
            getLog().info("Use model name: " + modelName);
        }
        providerName = modelName + "Config";
        if (isVerbose()) {
            getLog().info("Use provider name: " + providerName);
        }
        classLoader = initClassLoader(getProject(), new File(getProject().getBuild().getOutputDirectory()), true, false, true, true, true);

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();

        ApplicationConfigProvider provider;
        try {
            Thread.currentThread().setContextClassLoader(classLoader);
            provider = ApplicationConfigProvider.getProvider(classLoader, providerName);
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }

        if (provider == null) {
            throw new MojoExecutionException("Could not find provider with name: " + providerName);
        }

        if (getModelFile() == null) {
            setModelFile(modelName);
        }

        configModel = provider.toModel(Locale.FRANCE);

    }

    public String getModelName() {
        return modelName;
    }

    public String getProviderName() {
        return providerName;
    }

    public ConfigModel getConfigModel() {
        return configModel;
    }
}
