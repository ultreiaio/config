package io.ultreia.java4all.config.plugin;

/*
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ApplicationConfigProvider;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.util.List;

/**
 * Generates a report for declared application config via the
 * {@link ApplicationConfigProvider} mechanism on a aggregate mojo.
 * <p>
 * For each configuration, you can find all his options and actions.
 *
 * @author Tony - dev@tchemit.fr
 * @since 3.0
 */
@Mojo(name = "aggregate-report", aggregator = true, requiresReports = true, requiresDependencyCollection = ResolutionScope.COMPILE_PLUS_RUNTIME,requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME, threadSafe = true)
public class AggregateReportMojo extends ReportMojoSupport {

    /**
     * The projects in the reactor.
     */
    @Parameter(property = "reactorProjects", readonly = true, required = true)
    private List<MavenProject> reactorProjects;

    @Override
    protected ClassLoader createClassLoader() {
        return initClassLoader(reactorProjects);
    }
}
