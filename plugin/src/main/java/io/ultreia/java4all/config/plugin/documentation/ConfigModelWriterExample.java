package io.ultreia.java4all.config.plugin.documentation;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.spi.OptionModel;
import org.codehaus.plexus.i18n.I18N;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * To generate a properties file with all options commented with default values.
 * <p>
 * Created on 27/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.13
 */
public class ConfigModelWriterExample extends ConfigModelWriterDocumentationSupport {

    public ConfigModelWriterExample(I18N i18n, Locale locale, boolean useI18n) {
        super(i18n, locale, useI18n, "config-report");
    }

    @Override
    public void write(ConfigModel configModel, String comment, BufferedWriter writer) throws IOException {
        writeComment(writer, getDescription(configModel.getDescription()));
        writer.newLine();
        LinkedList<OptionModel> options = configModel.getOptions();

        String descriptionLabel = String.format("%s (%s)", getText("report.config.option.description"), getText("report.config.option.type"));

        String keyLabel = getText("report.config.option.key");
        String valueLabel = getText("report.config.option.value");

        List<OptionModel> constants = options.stream().filter(v -> v.isFinal() && v.isTransient()).collect(Collectors.toList());
        List<OptionModel> variables = options.stream().filter(v -> !(v.isFinal() && v.isTransient())).collect(Collectors.toList());

        if (!constants.isEmpty()) {
            int keyLength = Math.max(keyLabel.length(), constants.stream().mapToInt(o -> o.getKey().length()).max().orElse(0));
            int descriptionLength = Math.max(descriptionLabel.length(), constants.stream().map(this::getOptionDescription).mapToInt(String::length).max().orElse(0));

            writeSection(writer, String.format(getText("report.config.constant.list"), constants.size()));

            String pattern = "%1$-" + keyLength + "s # %2$-" + descriptionLength + "s # %3$s";

            writeComment(writer, String.format(pattern,
                                               keyLabel,
                                               descriptionLabel,
                                               valueLabel));
            for (OptionModel optionModel : constants) {
                writeComment(writer, String.format(pattern,
                                                   optionModel.getKey(),
                                                   getOptionDescription(optionModel),
                                                   getDefaultValue(optionModel)));
            }
            writer.newLine();
        }

        writeSection(writer, String.format(getText("report.config.option.list"), variables.size()));
        int keyLength = Math.max(keyLabel.length(), variables.stream().mapToInt(o -> o.getKey().length()).max().orElse(0));
        String pattern = "%1$-" + keyLength + "s # %2$s";

        writeComment(writer, String.format(pattern,
                                           keyLabel,
                                           descriptionLabel));
        for (OptionModel optionModel : variables) {
            writeComment(writer, String.format(pattern,
                                               optionModel.getKey(),
                                               getOptionDescription(optionModel)));
        }
        writer.newLine();
        writeSection(writer, getText("report.config.option.override"));


        for (OptionModel optionModel : variables) {
            String defaultValue = optionModel.getDefaultValue();
            writer.write(String.format("#%s=%s\n", optionModel.getKey(), defaultValue == null ? "" : defaultValue.replaceAll("\"", "\\\\\"")));
        }
    }

    private String getOptionDescription(OptionModel o) {
        return String.format("%s (%s)", getDescription(o.getDescription()), (o.isUseGson() ? "gson:" : "") + o.getRawType());
    }

    @Override
    public String getFormat() {
        return "example";
    }

    private void writeSection(BufferedWriter writer, String comment) throws IOException {
        writer.append("####");
        writer.newLine();
        writer.append("## ").append(comment);
        writer.newLine();
        writer.append("####");
        writer.newLine();
        writer.newLine();
    }

    private void writeComment(BufferedWriter writer, String comment) throws IOException {
        writer.append("# ").append(comment);
        writer.newLine();
    }

}
