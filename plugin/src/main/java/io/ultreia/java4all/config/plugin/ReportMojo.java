package io.ultreia.java4all.config.plugin;

/*
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ApplicationConfigProvider;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.util.Collections;

/**
 * Generates a report for declared application config via the
 * {@link ApplicationConfigProvider} mechanism.
 * <p>
 * For each configuration, you can find all his options and actions.
 *
 * @author Tony - dev@tchemit.fr
 * @since 3.0
 */
@Mojo(name = "report", requiresReports = true, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME, threadSafe = true)
public class ReportMojo extends ReportMojoSupport {

    @Override
    protected ClassLoader createClassLoader() {
        return initClassLoader(Collections.singleton(getProject()));
    }

}
