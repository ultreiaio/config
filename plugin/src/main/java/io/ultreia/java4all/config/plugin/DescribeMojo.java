package io.ultreia.java4all.config.plugin;

/*
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * Describe application config from java files to the description configuration format.
 * <p>
 * Created on 28/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@Mojo(name = "describe", defaultPhase = LifecyclePhase.COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
@Execute(phase = LifecyclePhase.COMPILE)
public class DescribeMojo extends ConfigFromJavaFileMojoSupport {

    @Override
    protected void doAction() throws Exception {
        getLog().info("Generate file to: " + getModelFile());
        if (Files.notExists(getModelFile().toPath().getParent())) {
            Files.createDirectories(getModelFile().toPath().getParent());
        }
        getConfigStorage().write(getConfigModel(), null, getModelFile().toPath(), StandardCharsets.UTF_8);

    }

}
