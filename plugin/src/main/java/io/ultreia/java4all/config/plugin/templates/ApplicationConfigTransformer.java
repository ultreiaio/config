package io.ultreia.java4all.config.plugin.templates;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import io.ultreia.java4all.config.ApplicationConfig;
import io.ultreia.java4all.config.GeneratedConfigSupport;
import io.ultreia.java4all.config.GeneratedConfigWithActionSupport;
import io.ultreia.java4all.config.OptionList;
import io.ultreia.java4all.config.spi.ActionModel;
import io.ultreia.java4all.config.spi.ApplicationConfigInit;
import io.ultreia.java4all.config.spi.ApplicationConfigProvider;
import io.ultreia.java4all.config.spi.ConfigActionDef;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.spi.ConfigOptionDef;
import io.ultreia.java4all.config.spi.OptionModel;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.RecursiveProperties;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * To generate configuration java files from the options enum file.
 * <p>
 * Created on 15/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@SuppressWarnings({"unused", "UnusedAssignment", "CommentedOutCode"})
public class ApplicationConfigTransformer extends ObjectModelTransformerToJava {

    public static final String PROP_CONFIG = "config";
    private static final Logger log = LogManager.getLogger(ApplicationConfigTransformer.class);
    private static final Set<String> KNOWN_TYPES = ImmutableSet.of("File", "Color", "KeyStroke", "URL", "Class", "Date", "Time", "Timestamp", "Locale", "Version", "String", "int", "Integer", "long", "Long", "float", "Float", "boolean", "Boolean", "byte", "Byte", "char", "Character", "double", "Double");
    private ApplicationConfigTransformerConfig config;
    private I18nKeySet getterFile;

    @Override
    public void transformFromModel(ObjectModel model) {
        super.transformFromModel(model);
        config = getConfiguration().getProperty(PROP_CONFIG, ApplicationConfigTransformerConfig.class);

        ConfigModel configModel = config.getConfigModel();

        boolean useNuitonI18n = config.isUseI18n();

        if (useNuitonI18n) {
            getterFile = getConfiguration().getI18nGetterFile();
        }
        String packageName = config.getPackageName();
        String configClassName = config.getConfigClassName();
        String configProviderClassName = config.getConfigProviderClassName();
        String optionsClassName = config.getOptionsClassName();
        List<OptionModel> options = configModel.getOptions();

        String actionsClassName = config.getActionsClassName();
        List<ActionModel> actions = configModel.getActions();
        boolean withActions = CollectionUtils.isNotEmpty(actions);

        String abstractClassName = "Generated" + configClassName;
        generateConfigAbstractClass(packageName, abstractClassName, optionsClassName, actionsClassName, options, withActions);
        if (canGenerate(packageName + "." + configClassName)) {
            generateConfigClass(packageName, configClassName, abstractClassName);
        } else {
            log.debug(String.format("Skip generation for %s", configClassName));
        }

        if (config.isGenerateProvider()) {
            String abstractProviderClassName = "Generated" + configProviderClassName;
            generateProviderAbstractClass(packageName, abstractProviderClassName, configClassName, optionsClassName, actionsClassName, useNuitonI18n, configModel.getDescription(), withActions);
            if (canGenerate(packageName + "." + configProviderClassName)) {
                generateProviderClass(packageName, abstractProviderClassName, configProviderClassName);
            }
        }

        if (canGenerate(packageName + "." + optionsClassName)) {
            generateOptionsClass(packageName, useNuitonI18n, optionsClassName, options);
        }

        if (withActions && canGenerate(packageName + "." + actionsClassName)) {
            generateActionsClass(packageName, actionsClassName, actions, useNuitonI18n);
        }
    }

    private String escape(String value) {
        return "\"" + value + "\"";
    }

    private void generateOptionsClass(String packageName, boolean useNuitonI18n, String optionsClassName, List<OptionModel> options) {

        ObjectModelEnumeration output = createEnumeration(optionsClassName, packageName);
        addInterface(output, ConfigOptionDef.class);
        log.info(String.format("Generate %s", output.getQualifiedName()));
        RecursiveProperties recursiveProperties = config.getProperties();
        for (OptionModel option : options) {
            String literalName = GeneratorUtil.convertVariableNameToConstantName(option.getName());
            String defaultValue = option.getDefaultValue();
            if (defaultValue != null) {
                if (recursiveProperties != null) {
                    defaultValue = recursiveProperties.resolve(defaultValue);
                }
                defaultValue = escape(defaultValue);
            }
            String description = option.getDescription();
            description = escape(description);
            String literal = ""/*{
    <%=literalName%>(
        <%=option.getType()%>.class,
        "<%=option.getKey()%>",
        <%=description%>,
        <%=defaultValue%>,
        <%=option.isTransient()%>,
        <%=option.isFinal()%>)}*/;
            addLiteral(output, literal);
        }

        addAttribute(output, "type", Class.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "key", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "description", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "defaultValue", String.class, null, ObjectModelJavaModifier.PRIVATE);
        addAttribute(output, "_transient", boolean.class, null, ObjectModelJavaModifier.PRIVATE);
        addAttribute(output, "_final", boolean.class, null, ObjectModelJavaModifier.PRIVATE);

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PACKAGE);
        addParameter(constructor, Class.class, "type");
        addParameter(constructor, String.class, "key");
        addParameter(constructor, String.class, "description");
        addParameter(constructor, String.class, "defaultValue");
        addParameter(constructor, boolean.class, "_transient");
        addParameter(constructor, boolean.class, "_final");
        setOperationBody(constructor, ""/*{
        this.type = type;
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this._final = _final;
        this._transient = _transient;
    }*/);

        ObjectModelOperation getKey = addOperation(output, "getKey", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getKey, Override.class);
        setOperationBody(getKey, ""/*{
        return key;
    }*/);
        ObjectModelOperation getType = addOperation(output, "getType", Class.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getType, Override.class);
        setOperationBody(getType, ""/*{
        return type;
    }*/);
        ObjectModelOperation getDescription = addOperation(output, "getDescription", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDescription, Override.class);
        String descriptionValue = "description";
        if (useNuitonI18n) {
            descriptionValue = "io.ultreia.java4all.i18n.I18n.t(" + descriptionValue + ")";
        }
        setOperationBody(getDescription, ""/*{
        return <%=descriptionValue%>;
    }*/);

        ObjectModelOperation getDefaultValue = addOperation(output, "getDefaultValue", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDefaultValue, Override.class);
        setOperationBody(getDefaultValue, ""/*{
        return defaultValue;
    }*/);

        ObjectModelOperation setDefaultValue = addOperation(output, "setDefaultValue", void.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(setDefaultValue, String.class, "defaultValue");
        addAnnotation(output, setDefaultValue, Override.class);
        setOperationBody(setDefaultValue, ""/*{
        this.defaultValue = defaultValue;
    }*/);

        ObjectModelOperation isTransient = addOperation(output, "isTransient", boolean.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, isTransient, Override.class);
        setOperationBody(isTransient, ""/*{
        return _transient;
    }*/);

        ObjectModelOperation setTransient = addOperation(output, "setTransient", void.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(setTransient, boolean.class, "_transient");
        addAnnotation(output, setTransient, Override.class);
        setOperationBody(setTransient, ""/*{
        this._transient = _transient;
    }*/);

        ObjectModelOperation isFinal = addOperation(output, "isFinal", boolean.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, isFinal, Override.class);
        setOperationBody(isFinal, ""/*{
        return _final;
    }*/);

        ObjectModelOperation setFinal = addOperation(output, "setFinal", void.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(setFinal, boolean.class, "_final");
        addAnnotation(output, setFinal, Override.class);
        setOperationBody(setFinal, ""/*{
        this._final = _final;
    }*/);
    }

    private void generateActionsClass(String packageName, String actionsClassName, List<ActionModel> actions, boolean useNuitonI18n) {

        ObjectModelEnumeration output = createEnumeration(actionsClassName, packageName);
        addInterface(output, ConfigActionDef.class);
        log.info(String.format("Generate %s", output.getQualifiedName()));
        for (ActionModel action : actions) {
            String literalName = GeneratorUtil.convertVariableNameToConstantName(action.getName());
            String aliases = "";
            if (action.getAliases().length > 0) {
                aliases = ", " + Arrays.stream(action.getAliases()).map(this::escape).collect(Collectors.joining(", "));
            }
            String description = action.getDescription();
            description = escape(description);
            String literal = ""/*{
    <%=literalName%> ( "<%=action.getAction()%>", <%=description%><%=aliases%>  )}*/;
            addLiteral(output, literal);
        }

        addAttribute(output, "action", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "description", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "aliases", "String[]", null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PACKAGE);
        addParameter(constructor, String.class, "action");
        addParameter(constructor, String.class, "description");
        addParameter(constructor, "String...", "aliases");
        setOperationBody(constructor, ""/*{
        this.action = action;
        this.description = description;
        this.aliases = aliases;
    }*/);

        ObjectModelOperation getAction = addOperation(output, "getAction", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getAction, Override.class);
        setOperationBody(getAction, ""/*{
        return action;
    }*/);
        ObjectModelOperation getAliases = addOperation(output, "getAliases", "String[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getAliases, Override.class);
        setOperationBody(getAliases, ""/*{
        return aliases;
    }*/);

        ObjectModelOperation getDescription = addOperation(output, "getDescription", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDescription, Override.class);
        String descriptionValue = "description";
        if (useNuitonI18n) {
            descriptionValue = "io.ultreia.java4all.i18n.I18n.t(" + descriptionValue + ")";
        }
        setOperationBody(getDescription, ""/*{
        return <%=descriptionValue%>;
    }*/);
    }

    private void generateProviderAbstractClass(String packageName, String providerClassName, String className, String optionClassName, String actionClassName, boolean useNuitonI18n, String description, boolean withActions) {

        ObjectModelClass output = createAbstractClass(providerClassName, packageName);
        addInterface(output, ApplicationConfigProvider.class);

        log.info(String.format("Generate %s", output.getQualifiedName()));
        ObjectModelOperation getName = addOperation(output, "getName", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getName, Override.class);
        setOperationBody(getName, ""/*{
        return "<%=className%>";
    }*/);

        if (useNuitonI18n) {
            addImport(output, I18n.class);
        }

        String optionClassSimpleName = GeneratorUtil.getSimpleName(optionClassName);

        ObjectModelOperation getOptions = addOperation(output, "getOptions", optionClassName + "[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getOptions, Override.class);
        setOperationBody(getOptions, ""/*{
        return <%=optionClassSimpleName%>.values();
    }*/);

        if (withActions) {

            String actionClassSimpleName = GeneratorUtil.getSimpleName(actionClassName);

            ObjectModelOperation getActions = addOperation(output, "getActions", actionClassName + "[]", ObjectModelJavaModifier.PUBLIC);
            addAnnotation(output, getActions, Override.class);
            setOperationBody(getActions, ""/*{
        return <%=actionClassSimpleName%>.values();
    }*/);
        } else {

            addImport(output, ConfigActionDef.class);
            ObjectModelOperation getActions = addOperation(output, "getActions", "ConfigActionDef[]", ObjectModelJavaModifier.PUBLIC);
            addAnnotation(output, getActions, Override.class);
            setOperationBody(getActions, ""/*{
        return new ConfigActionDef[0];
    }*/);
        }

        String descriptionValue = "\"" + description + "\"";
        if (useNuitonI18n) {
            descriptionValue = "I18n.l(locale, " + descriptionValue + ")";
            getterFile.addKey(description);
        }
        ObjectModelOperation getDescription = addOperation(output, "getDescription", String.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(getDescription, Locale.class, "locale");
        addAnnotation(output, getDescription, Override.class);

        setOperationBody(getDescription, ""/*{
        return <%=descriptionValue%>;
    }*/);
    }

    private void generateProviderClass(String packageName, String abstractProviderClassName, String providerClassName) {

        ObjectModelClass output = createClass(providerClassName, packageName);
        setSuperClass(output, abstractProviderClassName);
        log.info(String.format("Generate %s", output.getQualifiedName()));
    }

    private void generateConfigAbstractClass(String packageName, String abstractClassName, String optionClassName, String actionClassName, List<OptionModel> options, boolean withActions) {

        String optionClassSimpleName = GeneratorUtil.getSimpleName(optionClassName);
        String actionsClassSimpleName = withActions ? GeneratorUtil.getSimpleName(actionClassName) : null;

        ObjectModelClass output = createAbstractClass(abstractClassName, packageName);
        addImport(output, optionClassName);
        addImport(output, ApplicationConfigInit.class);
        if (withActions) {
            addImport(output, actionClassName);
        }
        String superclassQualifiedName = withActions ? String.format("%s<%s, %s>", GeneratedConfigWithActionSupport.class.getName(), optionClassSimpleName, actionsClassSimpleName) : String.format("%s<%s>", GeneratedConfigSupport.class.getName(), optionClassSimpleName);
        setSuperClass(output, superclassQualifiedName);

        log.info(String.format("Generate %s", output.getQualifiedName()));

        ObjectModelOperation publicConstructor = addConstructor(output, ObjectModelJavaModifier.PROTECTED);
        setOperationBody(publicConstructor, ""/*{
        super(ApplicationConfigInit.forAllScopes());
    }*/);

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PROTECTED);
        addParameter(constructor, ApplicationConfigInit.class.getSimpleName(), "init");
        setOperationBody(constructor, ""/*{
        super(init);
    }*/);
        ObjectModelOperation constructorWithFileName = addConstructor(output, ObjectModelJavaModifier.PROTECTED);
        addParameter(constructorWithFileName, String.class.getSimpleName(), "fileName");
        setOperationBody(constructorWithFileName, ""/*{
        super(fileName);
    }*/);
        if (config.isUseGson()) {
            addImport(output, Supplier.class);
            addImport(output, Gson.class);
            addOperation(output, "getGsonSupplier", "Supplier<Gson>", ObjectModelJavaModifier.PROTECTED, ObjectModelJavaModifier.ABSTRACT);
        }

        for (OptionModel option : options) {
            String attrName = option.getName();
            String attrType = option.getType();
            String simpleType = JavaGeneratorUtil.getSimpleName(attrType);
            String constantName = JavaGeneratorUtil.getSimpleName(optionClassName) + "." + JavaGeneratorUtil.convertVariableNameToConstantName(attrName);
            addImport(output, attrType);
            boolean useGson = option.isUseGson();
            boolean isArray = simpleType.endsWith("[]");
            if (isArray) {
                simpleType = simpleType.substring(0, simpleType.indexOf("[]")).trim();
                addImport(output, List.class);
                addImport(output, ApplicationConfig.class);
            }
            createGetMethod(output, attrName, simpleType, constantName, useGson, isArray);
            createSetMethod(output, attrName, simpleType, constantName, useGson, isArray);
        }

        ObjectModelOperation optionsMethod = addOperation(output, "options", String.format("%s[]", optionClassSimpleName), ObjectModelJavaModifier.PROTECTED);
        addAnnotation(output, optionsMethod, Override.class);
        setOperationBody(optionsMethod, ""/*{
        return <%=optionClassSimpleName%>.values();
    }*/);
        if (withActions) {
            ObjectModelOperation actionsMethod = addOperation(output, "actions", String.format("%s[]", actionsClassSimpleName), ObjectModelJavaModifier.PROTECTED);
            addAnnotation(output, actionsMethod, Override.class);
            setOperationBody(actionsMethod, ""/*{
        return <%=actionsClassSimpleName%>.values();
    }*/);
        }
    }

    private void generateConfigClass(String packageName, String className, String abstractClassName) {

        ObjectModelClass output = createClass(className, packageName);
        log.info(String.format("Generate %s", output.getQualifiedName()));
        setSuperClass(output, abstractClassName);

        ObjectModelOperation publicConstructor = addConstructor(output, ObjectModelJavaModifier.PUBLIC);
        setOperationBody(publicConstructor, ""/*{
    }*/);

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PUBLIC);
        addImport(output, ApplicationConfigInit.class);
        addParameter(constructor, ApplicationConfigInit.class.getSimpleName(), "init");
        setOperationBody(constructor, ""/*{
        super(init);
    }*/);
        ObjectModelOperation constructorWithFileName = addConstructor(output, ObjectModelJavaModifier.PUBLIC);
        addImport(output, ApplicationConfigInit.class);
        addParameter(constructorWithFileName, String.class.getSimpleName(), "fileName");
        setOperationBody(constructorWithFileName, ""/*{
        super(fileName);
    }*/);

    }

    private boolean canGenerate(String input) {
        return !getResourcesHelper().isJavaFileInClassPath(input);
    }

    private void createGetMethod(ObjectModelClass output, String attrName, String simpleType, String constantName, boolean useGson, boolean isArray) {
        boolean booleanProperty = GeneratorUtil.isBooleanPrimitive(simpleType);
        String methodPrefix = booleanProperty ? JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX : JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
        String operationType = simpleType;
        String methodName;
        if (isArray) {
            methodName = "getOptionAsList";
            operationType = simpleType+"[]";
        } else {
            methodName = "getOptionAs" + Strings.capitalize(simpleType);
            if (simpleType.equals("String")) {
                methodName = "getOption";
            } else if (simpleType.equals("Integer")) {
                methodName = "getOptionAsInt";
            }
        }
        ObjectModelOperation operation = addOperation(output, getJavaBeanMethodName(methodPrefix, attrName), operationType, ObjectModelJavaModifier.PUBLIC);

        if (useGson) {
            setOperationBody(operation, ""/*{
        String option = applicationConfig.getOption(<%=constantName%>.getKey());
        return getGsonSupplier().get().fromJson(option, <%=simpleType%>.class);
    }*/);
        } else if (isArray) {
            addImport(output, OptionList.class);
            String listMethodName = getJavaBeanMethodName(methodPrefix, attrName + "List");
            ObjectModelOperation   listOperation = addOperation(output, listMethodName, String.format("List<%s>", simpleType), ObjectModelJavaModifier.PUBLIC);
            setOperationBody(listOperation, ""/*{
        OptionList optionAsList = applicationConfig.<%=methodName%>(<%=constantName%>.getKey());
        return optionAsList.getOption(<%=simpleType%>.class);
    }*/);
            setOperationBody(operation, ""/*{
        List<<%=simpleType%>> option = <%=listMethodName%>();
        return option.toArray(new <%=simpleType%>[0]);
    }*/);
        } else if (!KNOWN_TYPES.contains(simpleType)) {
            methodName = "getOption";
            setOperationBody(operation, ""/*{
        return (<%=simpleType%>) applicationConfig.<%=methodName%>(<%=simpleType%>.class, <%=constantName%>.getKey());
    }*/);
        } else {
            setOperationBody(operation, ""/*{
        return applicationConfig.<%=methodName%>(<%=constantName%>.getKey());
    }*/);

            if ("Boolean".equals(simpleType)) {
                operation = addOperation(output, getJavaBeanMethodName(JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX, attrName), "boolean", ObjectModelJavaModifier.PUBLIC);

                setOperationBody(operation, ""/*{
        return applicationConfig.<%=methodName%>(<%=constantName%>.getKey());
    }*/);
            }
        }
    }

    @SuppressWarnings("StringOperationCanBeSimplified")
    private void createSetMethod(ObjectModelClass output, String attrName, String simpleType, String constantName, boolean useGson, boolean isArray) {
        boolean booleanProperty = GeneratorUtil.isBooleanPrimitive(simpleType);
        ObjectModelOperation operation = addOperation(output, getJavaBeanMethodName("set", attrName), "void", ObjectModelJavaModifier.PUBLIC);

        addParameter(operation, simpleType + (isArray ? "[]" : ""), attrName);

        String methodPrefix = booleanProperty ? JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX : JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
        String methodName = getJavaBeanMethodName(methodPrefix, attrName);
        StringBuilder bodyContent = new StringBuilder();
        if (useGson) {
            bodyContent.append(""/*{
        String option = getGsonSupplier().get().toJson(<%=attrName%>);
        setOption(<%=constantName%>.getKey(), option);
    }*/);
        } else if (isArray) {
            bodyContent.append(""/*{
        setOptionArray(<%=constantName%>.getKey(), <%=attrName%>);
    }*/);
        } else {
            bodyContent.append(""/*{
        setOption(<%=constantName%>.getKey(), <%=attrName%>);
    }*/);
        }
        setOperationBody(operation, bodyContent.toString());
        if (isArray) {
            operation = addOperation(output, getJavaBeanMethodName("set", attrName + "List"), "void", ObjectModelJavaModifier.PUBLIC);
            addParameter(operation, String.format("List<%s>", simpleType), attrName);
            setOperationBody(operation, ""/*{
        setOptionList(<%=constantName%>.getKey(), <%=attrName%>);
    }*/);
        } else if ("Boolean".equals(simpleType)) {
            operation = addOperation(output, getJavaBeanMethodName("set", attrName), "void", ObjectModelJavaModifier.PUBLIC);
            addParameter(operation, "boolean", attrName);

            setOperationBody(operation, ""/*{
        setOption(<%=constantName%>.getKey(), <%=attrName%>);
    }*/);
        }
    }

}
