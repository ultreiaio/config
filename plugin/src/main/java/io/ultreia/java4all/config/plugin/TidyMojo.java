package io.ultreia.java4all.config.plugin;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.io.SpiStorage;
import io.ultreia.java4all.config.spi.ActionModel;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.spi.OptionModel;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * To tidy a configuration.
 * <p>
 * Will rewrite configuration file in proper order.
 * <p>
 * Created on 15/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.19
 */
@Mojo(name = "tidy", defaultPhase = LifecyclePhase.INITIALIZE, threadSafe = true)
public class TidyMojo extends ConfigMojoSupport {
    /**
     * Model name (will prefix the generated files names with it).
     * <p>
     * By default, will use artifactId in camelCase format ( example: Model name for artifact id
     * {@code config-example} is {@code ConfigExample} ).
     * </p>
     */
    @Parameter(property = "config.modelName")
    private String modelName;

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    @Override
    protected void doAction() throws MojoExecutionException {
        initModelName(modelName, this::setModelName);

        SpiStorage storage = SpiStorage.get(getFormat());

        Path modelPath = getModelFile().toPath();

        getLog().info("Load config file: " + modelPath);
        ConfigModel model = storage.read(modelPath);

        LinkedList<OptionModel> options = model.getOptions();
        options.sort(Comparator.comparing(OptionModel::getKey));
        LinkedList<ActionModel> actions = model.getActions();
        actions.sort(Comparator.comparing(ActionModel::getName));
        getLog().info("Rewrite config file: " + modelPath);

        storage.write(model, null, modelPath, StandardCharsets.UTF_8);
    }
}
