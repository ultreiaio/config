package io.ultreia.java4all.config.plugin.documentation;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.spi.ActionModel;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.spi.OptionModel;
import org.codehaus.plexus.i18n.I18N;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created on 27/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.13
 */
public class ConfigModelWriterDocumentation extends ConfigModelWriterDocumentationSupport {

    public ConfigModelWriterDocumentation(I18N i18n, Locale locale, boolean useI18n) {
        super(i18n, locale, useI18n, "config-report");
    }

    @Override
    public void write(ConfigModel configModel, String comment, BufferedWriter writer) throws IOException {

        writeSection(writer, getText("report.overview.title"));
        writeText(writer, getText("report.overview.text"));
        writer.newLine();
        writeLink(writer, getText("report.overview.more.information"), "https://ultreiaio.gitlab.io/config/index.html");

        writeTableHeader(writer
                , getText("report.config.name")
                , getText("report.config.description")
                , getText("report.config.nbConstants")
                , getText("report.config.nbOptions")
                , getText("report.config.nbActions"));

        LinkedList<OptionModel> options = configModel.getOptions();

        List<OptionModel> constants = options.stream().filter(v -> v.isFinal() && v.isTransient()).collect(Collectors.toList());
        List<OptionModel> variables = options.stream().filter(v -> !(v.isFinal() && v.isTransient())).collect(Collectors.toList());

        writeTableRow(writer
                , boxLink(configModel.getName(), "#detail_" + configModel.getName())
                , getDescription(configModel.getDescription())
                , constants.size() + ""
                , variables.size() + ""
                , configModel.getActions().size() + "");

        writeSection(writer, getText("report.detail.title"));
        writeText(writer, getText("report.detail.text"));
        writer.newLine();

        writeSubSection(writer, getText("report.detail.configuration.title") + "   " + configModel.getName());
        writeAnchor(writer, "detail_" + configModel.getName());
        writeText(writer, String.format("%s : **%s**", getText("report.config.name"), configModel.getName()));
        writer.newLine();
        writeText(writer, String.format("%s : **%s**", getText("report.config.description"), getDescription(configModel.getDescription())));
        writer.newLine();

        writeSubSubSection(writer, getText("report.detail.constants.title"));

        if (constants.isEmpty()) {
            writeText(writer, getText("report.detail.options.noConstants"));
            writer.newLine();

        } else {

            writeTableHeader(writer
                    , getText("report.config.option.key")
                    , getText("report.config.option.description")
                    , getText("report.config.option.withDefaultValue")
                    , getText("report.config.option.type")
            );

            for (OptionModel optionModel : constants) {
                Optional<String> defaultValue = optionModel.getOptionalDefaultValue();
                writeTableRow(writer
                        , boxLink(optionModel.getKey(), "#detail_option_" + optionModel.getKey())
                        , getDescription(optionModel.getDescription())
                        , getText(defaultValue.isPresent())
                        , boxVerbatim((optionModel.isUseGson() ? "gson:" : "") + optionModel.getType())
                );
            }
            writer.newLine();

            for (OptionModel optionModel : constants) {
                writeAnchor(writer, "detail_option_" + optionModel.getKey());

                writeSubSubSubSection(writer, "Option " + optionModel.getKey());


                writeTableHeader(writer
                        , getText("report.config.option.field")
                        , getText("report.config.option.value")
                );

                writeTableRow(writer
                        , getText("report.config.option.key")
                        , optionModel.getKey()
                );
                writeTableRow(writer
                        , getText("report.config.option.description")
                        , getDescription(optionModel.getDescription())
                );

                writeTableRow(writer
                        , getText("report.config.option.type")
                        , boxVerbatim((optionModel.isUseGson() ? "gson:" : "") + optionModel.getType())
                );
                writeTableRow(writer
                        , getText("report.config.option.value")
                        , boxVerbatim(getDefaultValue(optionModel))
                );
                writer.newLine();
            }

            writer.newLine();
        }


        writeSubSubSection(writer, getText("report.detail.options.title"));

        if (variables.isEmpty()) {
            writeText(writer, getText("report.detail.options.noOptions"));
            writer.newLine();

        } else {

            writeTableHeader(writer
                    , getText("report.config.option.key")
                    , getText("report.config.option.description")
                    , getText("report.config.option.withDefaultValue")
                    , getText("report.config.option.type")
            );

            for (OptionModel optionModel : variables) {
                Optional<String> defaultValue = optionModel.getOptionalDefaultValue();
                writeTableRow(writer
                        , boxLink(optionModel.getKey(), "#detail_option_" + optionModel.getKey())
                        , getDescription(optionModel.getDescription())
                        , getText(defaultValue.isPresent())
                        , boxVerbatim((optionModel.isUseGson() ? "gson:" : "") + optionModel.getType())
                );
            }
            writer.newLine();

            for (OptionModel optionModel : variables) {
                writeAnchor(writer, "detail_option_" + optionModel.getKey());

                writeSubSubSubSection(writer, "Option " + optionModel.getKey());

                writeTableHeader(writer
                        , getText("report.config.option.field")
                        , getText("report.config.option.value")
                );

                writeTableRow(writer
                        , getText("report.config.option.key")
                        , optionModel.getKey()
                );
                writeTableRow(writer
                        , getText("report.config.option.description")
                        , getDescription(optionModel.getDescription())
                );
                writeTableRow(writer
                        , getText("report.config.option.type")
                        , boxVerbatim((optionModel.isUseGson() ? "gson:" : "") + optionModel.getType())
                );
                writeTableRow(writer
                        , getText("report.config.option.defaultValue")
                        , boxVerbatim(getDefaultValue(optionModel))
                );
                writer.newLine();
            }

            writer.newLine();
        }

        writeSubSubSection(writer, getText("report.detail.actions.title"));

        LinkedList<ActionModel> actions = configModel.getActions();
        if (actions.isEmpty()) {
            writeText(writer, getText("report.detail.actions.noActions"));
            writer.newLine();
        } else {

            writeTableHeader(writer
                    , getText("report.config.action.description")
                    , getText("report.config.action.aliases")
                    , getText("report.config.action.action"));

            for (ActionModel actionModel : actions) {
                writeTableRow(writer
                        , getDescription(actionModel.getDescription()).replaceAll("\n", " -- ")
                        , String.join(",", actionModel.getAliases())
                        , actionModel.getAction());
            }
            writer.newLine();
        }
    }

    @Override
    public String getFormat() {
        return "documentation";
    }

    void writeSection(BufferedWriter writer, String title) throws IOException {
        writer.newLine();
        writer.append("# ").append(title);
        writer.newLine();
    }


    void writeAnchor(BufferedWriter writer, String anchorName) throws IOException {
        writer.newLine();
        writer.append(String.format("### <a name=\"%s\"></a>", anchorName));
        writer.newLine();
    }


    void writeTableHeader(BufferedWriter writer, String... title) throws IOException {
        writer.newLine();
        writer.append("|");
        for (String columnName : title) {
            writer.append(" ").append(columnName).append(" |");
        }
        writer.newLine();
        writer.append("|");
        for (String ignored : title) {
            writer.append(" ").append("---").append(" |");
        }
        writer.newLine();
    }

    void writeTableRow(BufferedWriter writer, String... title) throws IOException {
        writer.append("|");
        for (String columnName : title) {
            writer.append(" ").append(columnName).append(" |");
        }
        writer.newLine();
    }

    void writeSubSection(BufferedWriter writer, String title) throws IOException {
        writer.newLine();
        writer.append("## ").append(title);
        writer.newLine();
    }

    void writeSubSubSection(BufferedWriter writer, String title) throws IOException {
        writer.newLine();
        writer.append("### ").append(title);
        writer.newLine();
    }

    void writeSubSubSubSection(BufferedWriter writer, String title) throws IOException {
        writer.newLine();
        writer.append("#### ").append(title);
        writer.newLine();
    }

    void writeText(BufferedWriter writer, String text) throws IOException {
        writer.newLine();
        writer.append(text);
        writer.newLine();
    }

    private String boxVerbatim(String content) {
        return "```" + content + "```";
    }

    void writeVerbatim(BufferedWriter writer, String content, String classifier) throws IOException {
        writer.append("```").append(classifier == null ? "" : classifier);
        writer.newLine();
        writer.append(content);
        writer.newLine();
        writer.append("```");
        writer.newLine();
    }

    void writeLink(BufferedWriter writer, String title, String link) throws IOException {
        writer.append(boxLink(title, link));
        writer.newLine();
    }

    String boxLink(String title, String link) {
        return String.format("[%s](%s)", title, link);
    }

}
