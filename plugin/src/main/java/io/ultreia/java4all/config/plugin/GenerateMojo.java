package io.ultreia.java4all.config.plugin;

/*
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import io.ultreia.java4all.config.plugin.templates.ApplicationConfigTransformer;
import io.ultreia.java4all.config.plugin.templates.ApplicationConfigTransformerConfig;
import io.ultreia.java4all.config.spi.ActionModel;
import io.ultreia.java4all.config.spi.ApplicationConfigProvider;
import io.ultreia.java4all.config.spi.ConfigModel;
import io.ultreia.java4all.config.spi.OptionModel;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import io.ultreia.java4all.util.RecursiveProperties;
import io.ultreia.java4all.util.Version;
import org.apache.maven.model.Resource;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.eugene.DefaultTemplateConfiguration;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

/**
 * Generate application config java files from the option definition class.
 * <p>
 * Created on 28/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES, threadSafe = true)
public class GenerateMojo extends ConfigMojoSupport implements ApplicationConfigTransformerConfig {

    /**
     * To set the package fully qualified name of the generated class.
     * <p>
     * By default, will use groupId.artifactId (with {@code -} replaced by {@code .}) plus {@code .config}.
     * </p>
     * <p>
     * A special case is designed to remove in artifactId the first component if it is the last one of groupId, for
     * example with gav
     * <pre>org.nuiton:config</pre>
     * <p>
     * the package name will be {@code org.nuiton.config} and not {@code org.nuiton.nuiton.config}.
     * </p>
     */
    @Parameter(property = "config.packageName")
    private String packageName;

    /**
     * Model name (will prefix the generated files names with it).
     * <p>
     * By default, will use artifactId in camelCase format ( example: Model name for artifact id
     * {@code config-example} is {@code ConfigExample} ).
     * </p>
     */
    @Parameter(property = "config.modelName")
    private String modelName;

    /**
     * The simple name of the options class.
     * <p>
     * By default, will use {@code config.modelName + ConfigOption}.
     * </p>
     */
    @Parameter(property = "config.optionsClassName")
    private String optionsClassName;

    /**
     * The simple name of the actions class.
     * <p>
     * By default, will use {@code config.modelName + ConfigAction}.
     * </p>
     */
    @Parameter(property = "config.actionsClassName")
    private String actionsClassName;

    /**
     * To generate configuration provider classes.
     */
    @Parameter(property = "config.generateProvider", defaultValue = "true")
    private boolean generateProvider;

    /**
     * To use nuiton-i18n API to translate descriptions in generate code.
     */
    @Parameter(property = "config.useI18n")
    private boolean useI18n;
    /**
     * To generate i18n keys (no need to declare them in model).
     *
     * <ul>
     *     <li>Option with key {@code XXX} is translated as {@code modelName.config.option.XXX} </li>
     *     <li>Action with name {@code XXX} is translated as {@code modelName.config.action.XXX} </li>
     * </ul>
     *
     * <b>Note:</b> Only works if {@link #useI18n} is {@code true}.
     */
    @Parameter(property = "config.generateI18n", defaultValue = "true")
    private boolean generateI18n;

    /**
     * To use Gson in generated code.
     */
    @Parameter(property = "config.useGson")
    private boolean useGson;

    /**
     * The source directory where to scan options java file.
     */
    @Parameter(property = "config.sourceDirectory", defaultValue = "${basedir}/src/main/java", required = true)
    private File sourceDirectory;

    /**
     * The root directory where to generated.
     */
    @Parameter(property = "config.outputDirectory", defaultValue = "${project.build.directory}/generated-sources/java", required = true)
    private File outputDirectory;

    /**
     * To skip generate.
     */
    @Parameter(property = "config.skip", defaultValue = "false")
    private boolean skip;

    /**
     * To filter options default value with project properties (delimiters are <code>@@{</code> and <code></code>).
     */
    @Parameter(property = "config.filter", defaultValue = "true")
    private boolean filter;

    private ApplicationConfigTransformer template;
    private String configClassName;
    private String configProviderClassName;
    private ConfigModel configModel;

    private I18nModule i18nModule;
    private I18nKeySet i18nKeySet;
    private RecursiveProperties recursiveProperties;

    @Override
    protected void init() throws Exception {

        if (skip) {
            return;
        }
        super.init();

        initModelName(modelName, this::setModelName);

        if (packageName == null) {

            List<String> groupIdPaths = new ArrayList<>(asList(getProject().getGroupId().split("\\.")));

            List<String> artifactIdPaths = new ArrayList<>(Arrays.asList(getProject().getArtifactId().replaceAll("-", ".").split("\\.")));
            if (Iterables.getLast(groupIdPaths).equals(Iterables.getFirst(artifactIdPaths, ""))) {
                artifactIdPaths.remove(0);
            }
            groupIdPaths.addAll(artifactIdPaths);

            packageName = Joiner.on(".").join(groupIdPaths);
            if (isVerbose()) {
                getLog().info("Use package name: " + packageName);
            }
        }

        if (optionsClassName == null) {
            optionsClassName = modelName + "ConfigOption";
            if (isVerbose()) {
                getLog().info("Configuration options class: " + optionsClassName);
            }
        }

        if (actionsClassName == null) {
            actionsClassName = modelName + "ConfigAction";
            if (isVerbose()) {
                getLog().info("Configuration actions class: " + actionsClassName);
            }
        }

        ClassLoader loader = this.initClassLoader(getProject(), sourceDirectory, true, false, false, true, true);

        configClassName = modelName + "Config";
        getLog().info("Config class name: " + configClassName);

        if (generateProvider) {
            configProviderClassName = configClassName + "Provider";
            if (isVerbose()) {
                getLog().info("Config provider class name: " + configClassName);
            }
        }

        configModel = getConfigStorage().read(getModelFile().toPath());

        Properties templateProperties = new Properties();

        templateProperties.put(Template.PROP_ENCODING, StandardCharsets.UTF_8.name());
        templateProperties.put(Template.PROP_VERBOSE, isVerbose());
        templateProperties.put(Template.PROP_OVERWRITE, true);
        templateProperties.put(Template.PROP_CLASS_LOADER, loader);
        templateProperties.put(ApplicationConfigTransformer.PROP_CONFIG, this);

        template = new ApplicationConfigTransformer();
        DefaultTemplateConfiguration configuration = new DefaultTemplateConfiguration(templateProperties);
        if (useI18n) {
            i18nModule = I18nModule.forGetter(getProject().getProperties());
            configuration.setI18nGetterFile(i18nKeySet = i18nModule.getModuleKeySet("config"));
            if (generateI18n) {
                generateI18n(configModel);
            }
        }
        template.setConfiguration(configuration);

        if (!getProject().getCompileSourceRoots().contains(outputDirectory.getPath())) {
            if (isVerbose()) {
                getLog().info("Add compile source root : " + outputDirectory);
            }
            getProject().addCompileSourceRoot(outputDirectory.getPath());
        }
        recursiveProperties = filter ? new RecursiveProperties(getProject().getProperties(), "@@{", "}") : null;
        if (recursiveProperties != null) {
            Version version = Version.valueOf(getProject().getVersion());
            recursiveProperties.setProperty("project.version", version.getVersion());
            recursiveProperties.setProperty("project.version.stable", (version.isSnapshot() ? Version.removeSnapshot(version) : version).getVersion());
        }
    }

    private void generateI18n(ConfigModel configModel) {

        String modelName = configModel.getName().toLowerCase();
        generateI18n(configModel.getDescription(), () -> String.format("%s.config.name", modelName), configModel::setDescription);

        for (OptionModel option : configModel.getOptions()) {
            generateI18n(option.getDescription(), () -> String.format("%s.config.option.%s", modelName, option.getKey()), option::setDescription);
        }

        for (ActionModel action : configModel.getActions()) {
            generateI18n(action.getDescription(), () -> String.format("%s.config.action.%s", modelName, action.getName()), action::setDescription);
        }

    }

    private void generateI18n(String description, Supplier<String> defaultValue, Consumer<String> defaultConsumer) {
        if (description == null || description.trim().isEmpty()) {
            description = defaultValue.get();
            defaultConsumer.accept(description);
        }
        i18nKeySet.addKey(description);
    }

    @Override
    public RecursiveProperties getProperties() {
        return recursiveProperties;
    }

    @Override
    protected void doAction() throws Exception {

        if (skip) {
            getLog().info("Generate mojo is skip.");
            return;
        }

        getLog().info("Generate file(s) to: " + outputDirectory);

        ObjectModelImpl objectModel = new ObjectModelImpl();
        objectModel.setName(modelName);

        template.applyTemplate(objectModel, outputDirectory);

        if (generateProvider) {

            String content = packageName + "." + configClassName + "Provider";
            Path serviceLoaderFile = PluginHelper.getFile(outputDirectory, "META-INF", "services", ApplicationConfigProvider.class.getName()).toPath();
            if (!Files.exists(serviceLoaderFile.getParent())) {
                Files.createDirectories(serviceLoaderFile.getParent());
            }
            Files.write(serviceLoaderFile, content.getBytes());
            if (isVerbose()) {
                getLog().info("Generate serviceLoader provider file: " + serviceLoaderFile);
            }
            Resource resource = new Resource();
            resource.setDirectory(outputDirectory.getAbsolutePath());
            resource.setIncludes(Collections.singletonList("**/" + ApplicationConfigProvider.class.getName()));
            getProject().addResource(resource);
        }

        if (useI18n) {
            i18nModule.storeModuleKeySet(i18nKeySet);
        }
    }

    @Override
    public String getPackageName() {
        return packageName;
    }

    @Override
    public String getModelName() {
        return modelName;
    }

    @Override
    public String getOptionsClassName() {
        return optionsClassName;
    }

    @Override
    public String getActionsClassName() {
        return actionsClassName;
    }

    @Override
    public boolean isGenerateProvider() {
        return generateProvider;
    }

    @Override
    public String getConfigProviderClassName() {
        return configProviderClassName;
    }

    @Override
    public String getConfigClassName() {
        return configClassName;
    }

    @Override
    public ConfigModel getConfigModel() {
        return configModel;
    }

    @Override
    public boolean isUseI18n() {
        return useI18n;
    }

    @Override
    public boolean isUseGson() {
        return useGson;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

}
