package io.ultreia.java4all.config.plugin;

/*-
 * #%L
 * Config :: Maven plugin
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.plugin.documentation.ConfigModelWriterDocumentationSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.i18n.runtime.boot.I18nBootLoader;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.i18n.I18N;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;

/**
 * Created on 27/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.13
 */
public abstract class GenerateDocumentationMojoSupport extends ConfigFromJavaFileMojoSupport {

    /**
     * The target directory where to generate files.
     */
    @Parameter(property = "config.targetDirectory", defaultValue = "${project.build.outputDirectory}/META-INF/configuration", required = true)
    protected File targetDirectory;
    /**
     * Target file name (without extension).
     * <p>
     * If not set, will use {@code modelName}.
     */
    @Parameter(property = "config.targetFileName")
    protected String targetFileName;

    /**
     * To use i18n API to translate descriptions in generate code.
     */
    @Parameter(property = "config.useI18n")
    private boolean useI18n;
    /**
     * Locale used to generate files.
     */
    @Parameter(property = "config.locale", defaultValue = "en", required = true)
    private String locale;
    /**
     * Internationalization component.
     */
    @Component
    private I18N i18n;
    private Locale localeObject;

    private Path targetFile;

    protected abstract String getTargetFileExtension();

    protected abstract ConfigModelWriterDocumentationSupport createTemplate(I18N i18n, Locale locale, boolean useI18n);

    @Override
    protected void init() throws Exception {
        super.init();
        localeObject = new Locale(locale);
        if (useI18n) {
            // get i18n initializer
            I18nConfiguration defaultConfiguration = I18nConfiguration.createDefaultConfiguration();
            defaultConfiguration.setClassLoader(classLoader);
            I18nBootLoader initializer = new DefaultI18nBootLoader(defaultConfiguration);
            // init i18n
            try {
                I18n.init(initializer, localeObject);
            } catch (Error e) {
                e.printStackTrace();
            }
        }
        if (targetFileName == null) {
            targetFileName = getProviderName();
        }
        targetFile = initTargetFile(targetDirectory.toPath(), targetFileName, getTargetFileExtension());
    }

    protected Path initTargetFile(Path targetPath, String targetFileName, String targetFileExtension) {
        return targetPath.resolve(targetFileName + "." + targetFileExtension);
    }

    @Override
    protected void doAction() throws Exception {
        getLog().info("Generate file to: " + targetFile);
        if (Files.notExists(targetFile.getParent())) {
            Files.createDirectories(targetFile.getParent());
        }
        createTemplate(i18n, localeObject, useI18n).write(getConfigModel(), null, targetFile, StandardCharsets.UTF_8);
        if (isVerbose()) {
            getLog().info(String.format("Content:\n%s", Files.readString(targetFile)));
        }
    }

}
