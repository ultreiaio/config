package io.ultreia.java4all.config.spi;

/*-
 * #%L
 * Config :: SPI
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Définit le scope d'un lecture/écriture d'une option dans application config.
 *
 * Created by tchemit on 30/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public enum ApplicationConfigScope {

    DEFAULTS, // les valeurs par défaut des options
    CLASS_PATH, // options contenu dans un fichier lu dans le class-path
    SYSTEM, // options contenues dans un fichier lu sur le système (/etc/)
    HOME, // options contenues dans un fichier lu dans la home utilisateur (~/.config/)
    CURRENT, // options contenues dans un fichier lu dans le répertoire courant
    EXTRA, // options contenues dans un fichier lu dans le répertoire supplémentaire (défini par l'application)
    ENV, // options contenues dans les variables d'environnements
    JVM, // options de la jvm
    LINE, // utilisé lors du parsing des options de la ligne de commande
    OPTIONS // options obtenues après lecture de la ligne de commande
}
