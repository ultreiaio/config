package io.ultreia.java4all.config.spi;

/*-
 * #%L
 * Config :: SPI
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * Created by tchemit on 30/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ApplicationConfigInit {

    protected final Set<ApplicationConfigScope> scopes;
    protected String configFilename;
    protected String extraConfigDirectory;
    protected Properties defaults;
    protected String readFormat = "properties";

    public static ApplicationConfigInit defaultInit() {
        return forAllScopes();
    }

    public static ApplicationConfigInit forAllScopes() {
        return new ApplicationConfigInit(ApplicationConfigScope.values());
    }

    public static ApplicationConfigInit forAllScopesWithout(ApplicationConfigScope... excludedScopes) {
        List<ApplicationConfigScope> allScopes = new LinkedList<>(Arrays.asList(ApplicationConfigScope.values()));
        for (ApplicationConfigScope scope : excludedScopes) {
            allScopes.remove(scope);
        }
        return new ApplicationConfigInit(allScopes.toArray(new ApplicationConfigScope[0]));
    }

    public static ApplicationConfigInit forScopes(ApplicationConfigScope... scopes) {
        return new ApplicationConfigInit(scopes);
    }

    protected ApplicationConfigInit(ApplicationConfigScope... scopes) {
        this.scopes = Collections.unmodifiableSet(EnumSet.copyOf(Arrays.asList(scopes)));
    }

    public ApplicationConfigInit setConfigFileName(String configFilename) {
        this.configFilename = configFilename;
        return this;
    }

    public ApplicationConfigInit setExtraConfigDirectory(String extraConfigDirectory) {
        this.extraConfigDirectory = extraConfigDirectory;
        return this;
    }

    public ApplicationConfigInit addDefaults(String key, String value) {
        if (defaults == null) {
            defaults = new Properties();
        }
        defaults.setProperty(key, value);
        return this;
    }

    public ApplicationConfigInit setDefaults(Properties defaults) {
        this.defaults = defaults;
        return this;
    }

    public String getConfigFilename() {
        return configFilename;
    }

    public String getExtraConfigDirectory() {
        return extraConfigDirectory;
    }

    public Properties getDefaults() {
        return defaults;
    }

    public Set<ApplicationConfigScope> getScopes() {
        return scopes;
    }

    public String getReadFormat() {
        return readFormat;
    }

    public ApplicationConfigInit useIniFormat() {
        this.readFormat = "ini";
        return this;
    }

    public ApplicationConfigInit useTomlFormat() {
        this.readFormat = "toml";
        return this;
    }

    public ApplicationConfigInit useYamlFormat() {
        this.readFormat = "yaml";
        return this;
    }
}
