package io.ultreia.java4all.config.spi;

/*-
 * #%L
 * Config :: SPI
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Provider of a {@code ApplicationConfig}.
 * <p>
 * Each library of application which use {@code ApplicationConfig} should
 * implements this and add the provider available via the
 * {@link ServiceLoader} mechanism.
 * <p>
 * Using such provider offers a nice way to find out what options can be loaded
 * in a application. It also offers a simply way to generate application
 * config report for documentation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.8
 */
public interface ApplicationConfigProvider {
    Logger log = LogManager.getLogger(ApplicationConfigProvider.class);

    /**
     * Obtain all providers on class-path.
     *
     * @param classLoader optional classLoader used to seek for providers
     * @param includes    optional includes providers to use (if none then accept all providers)
     * @param excludes    optional excludes providers (if none the no reject)
     * @param verbose     verbose flag
     * @return sets of providers
     */
    static Set<ApplicationConfigProvider> getProviders(ClassLoader classLoader, Set<String> includes, Set<String> excludes, boolean verbose) {
        ServiceLoader<ApplicationConfigProvider> loader;
        if (classLoader == null) {
            loader = ServiceLoader.load(ApplicationConfigProvider.class);
        } else {
            loader = ServiceLoader.load(ApplicationConfigProvider.class, classLoader);
        }
        Set<ApplicationConfigProvider> result =
                new HashSet<>();

        for (ApplicationConfigProvider configProvider : loader) {
            String name = configProvider.getName();
            if (includes != null && !includes.contains(name)) {
                // reject by include
                if (verbose) {
                    log.info(String.format("configuration named '%s' is rejected by includes.", name));
                }
                continue;
            }
            if (excludes != null && excludes.contains(name)) {
                // reject by exclude
                if (verbose) {
                    log.info(String.format("configuration named '%s' is rejected by excludes.", name));
                }
                continue;
            }
            if (verbose) {
                log.info(String.format("configuration named '%s' will be generated.", name));
            }
            result.add(configProvider);
        }
        return result;
    }

    static ApplicationConfigProvider getProvider(ClassLoader classLoader, String name) {
        Set<ApplicationConfigProvider> providers = getProviders(classLoader, null, null, false);
        ApplicationConfigProvider result = null;
        for (ApplicationConfigProvider provider : providers) {
            if (name.equals(provider.getName())) {
                result = provider;
                break;
            }
        }
        return result;
    }

    /**
     * Gets all transient options from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options that are transient
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.7
     */
    static Set<ConfigOptionDef> getTransientOptions(Set<ApplicationConfigProvider> providers) {
        Set<ConfigOptionDef> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isTransient()) {
                    result.add(def);
                }
            }
        }
        return result;
    }

    /**
     * Gets all final options from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options that are final
     * @see ConfigOptionDef#isFinal()
     * @since 2.6.7
     */
    static Set<ConfigOptionDef> getFinalOptions(Set<ApplicationConfigProvider> providers) {
        Set<ConfigOptionDef> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isFinal()) {
                    result.add(def);
                }
            }
        }
        return result;
    }

    /**
     * Get all option keys that should not be saved in the user config file
     * from the given options providers.
     * <p>
     * Such options are {@code transient} or {@code final}.
     *
     * @param providers providers to inspect
     * @return the set of options key not to store in the config file
     * @see ConfigOptionDef#isFinal()
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.11
     */
    static Set<String> getTransientOrFinalOptionKey(Set<ApplicationConfigProvider> providers) {
        Set<String> result = new HashSet<>();
        result.addAll(getTransientOptionKeys(providers));
        result.addAll(getFinalOptionKeys(providers));
        return result;
    }

    /**
     * Gets all transient options keys from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options key that are transient
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.11
     */
    static Set<String> getTransientOptionKeys(Set<ApplicationConfigProvider> providers) {
        Set<String> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isTransient()) {
                    result.add(def.getKey());
                }
            }
        }
        return result;
    }

    /**
     * Gets all final options keys from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options keys that are final
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.7
     */
    static Set<String> getFinalOptionKeys(Set<ApplicationConfigProvider> providers) {
        Set<String> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isFinal()) {
                    result.add(def.getKey());
                }
            }
        }
        return result;
    }

    /**
     * Returns the name of the provided application config.
     * <p>
     * This should be the name of the library or application which offers the configuration.
     *
     * @return the name of the provided application config
     */
    String getName();

    /**
     * Returns the localized description of the configuration.
     *
     * @param locale locale used to render description
     * @return the localized description of the configuration
     */
    String getDescription(Locale locale);

    /**
     * Returns all options offered by the configuration.
     *
     * @return all options offered by the configuration
     * @see ConfigOptionDef
     */
    ConfigOptionDef[] getOptions();

    /**
     * Returns all actions offered by the configuration.
     *
     * @return all actions offered by the configuration.
     * @see ConfigActionDef
     */
    ConfigActionDef[] getActions();

    default ConfigModel toModel(Locale locale) {
        ConfigModel configModel = new ConfigModel();
        configModel.setName(getName());
        configModel.setDescription(getDescription(locale));

        LinkedList<OptionModel> optionModels = new LinkedList<>();
        for (ConfigOptionDef configOptionDef : getOptions()) {
            optionModels.add(ConfigOptionDef.of(configOptionDef));
        }
        configModel.setOptions(optionModels);

        LinkedList<ActionModel> actionModels = new LinkedList<>();
        for (ConfigActionDef configActionDef : getActions()) {
            actionModels.add(ConfigActionDef.of(configActionDef));
        }
        configModel.setActions(actionModels);
        return configModel;
    }
}
