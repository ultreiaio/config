package io.ultreia.java4all.config.spi;

/*-
 * #%L
 * Config :: SPI
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * Le contrat de marquage des options, on utilise cette interface pour
 * caracteriser une option de configuration.
 *
 * <pre>
 *  public enum MyConfigOption implements ConfigOptionDef {
 *
 *   APP_CONFIG_FILE(
 *   ApplicationConfig.CONFIG_FILE_NAME,
 *   "Main configuration app file",
 *   "myApp-config.properties",
 *   String.class, true, true),
 *
 *   APP_NAME(
 *   ApplicationConfig.CONFIG_FILE_NAME,
 *   Application name,
 *   "MyApp",
 *   String.class, true, true);
 *
 *   public String key;
 *   public String description;
 *   public String defaultValue;
 *   public Class&lt;?&gt; type;
 *   public boolean isTransient;
 *   public boolean isFinal;
 *
 *   private WikittyConfigOption(String key, String description,
 *           String defaultValue, Class&lt;?&gt; type, boolean isTransient, boolean isFinal) {
 *       this.key = key;
 *       this.description = description;
 *       this.defaultValue = defaultValue;
 *       this.type = type;
 *       this.isTransient = isTransient;
 *       this.isFinal = isFinal;
 *   }
 *
 *   &#64;Override
 *   public boolean isFinal() {
 *       return isFinal;
 *   }
 *
 *   &#64;Override
 *   public boolean isTransient() {
 *       return isTransient;
 *   }
 *
 *   &#64;Override
 *   public String getDefaultValue() {
 *       return defaultValue;
 *   }
 *
 *   &#64;Override
 *   public String getDescription() {
 *       return description;
 *   }
 *
 *   &#64;Override
 *   public String getKey() {
 *       return key;
 *   }
 *
 *   &#64;Override
 *   public Class&lt;?&gt; getType() {
 *       return type;
 *   }
 *
 *   &#64;Override
 *   public void setDefaultValue(String defaultValue) {
 *       this.defaultValue = defaultValue;
 *   }
 *
 *   &#64;Override
 *   public void setTransient(boolean isTransient) {
 *       this.isTransient = isTransient;
 *   }
 *
 *   &#64;Override
 *   public void setFinal(boolean isFinal) {
 *       this.isFinal = isFinal;
 *   }
 * }
 * </pre>
 *
 * @since 1.0.0-rc-9
 */
public interface ConfigOptionDef extends Serializable {

    static OptionModel of(ConfigOptionDef configOptionDef) {
        OptionModel optionModel = new OptionModel();

        if (configOptionDef instanceof Enum) {
            Enum<?> optionDef = (Enum<?>) configOptionDef;
            optionModel.setName(convertConstantNameToVariableName(optionDef.name()));
        }
        optionModel.setKey(configOptionDef.getKey());
        optionModel.setType(configOptionDef.getType());
        optionModel.setDefaultValue(configOptionDef.getDefaultValue());
        optionModel.setDescription(configOptionDef.getDescription());
        optionModel.setTransient(configOptionDef.isTransient());
        optionModel.setFinal(configOptionDef.isFinal());
        return optionModel;
    }

    static String convertConstantNameToVariableName(String constantName) {
        StringBuilder buffer = new StringBuilder();
        boolean lastCarIsUp = false;
        for (int i = 0, j = constantName.length(); i < j; i++) {
            char c = constantName.charAt(i);
            if ('_' == c) {
                lastCarIsUp = true;
                continue;
            }
            if (lastCarIsUp) {
                c = Character.toUpperCase(c);
                lastCarIsUp = false;
            } else {
                c = Character.toLowerCase(c);
            }
            buffer.append(c);
        }
        return buffer.toString();
    }

    /**
     * @return la clef identifiant l'option
     */
    String getKey();

    /**
     * @return le type de l'option
     */
    Class<?> getType();

    /**
     * @return la clef i18n de description de l'option
     */
    String getDescription();

    /**
     * @return la valeur par defaut de l'option sous forme de chaine de
     * caracteres
     */
    String getDefaultValue();

    /**
     * Changes the default value of the option.
     *
     * @param defaultValue the new default value of the option
     */
    void setDefaultValue(String defaultValue);

    /**
     * @return <code>true</code> si l'option ne peut etre sauvegardee sur
     * disque (utile par exemple pour les mots de passe, ...)
     */
    boolean isTransient();

    /**
     * Changes the transient state of the option.
     *
     * @param isTransient the new value of the transient state
     */
    void setTransient(boolean isTransient);

    /**
     * @return <code>true</code> si l'option n'est pas modifiable (utilise
     * par exemple pour la version de l'application, ...)
     */
    boolean isFinal();

    /**
     * Changes the final state of the option.
     *
     * @param isFinal the new transient state value
     */
    void setFinal(boolean isFinal);
}
