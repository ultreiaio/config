package io.ultreia.java4all.config.spi;

/*-
 * #%L
 * Config :: SPI
 * %%
 * Copyright (C) 2016 - 2023 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;


/**
 * Created on 01/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class OptionModel {

    private String name;
    private String key;
    private String description;
    private String type;
    private String defaultValue;
    private boolean _transient;
    private boolean _final;
    private boolean useGson;

    /**
     * Verifie si la classe est de type primitif.
     *
     * @param clazz nom de la classe a tester
     * @return vrai si le classe est de type primitif faux sinon
     */
    public static boolean isPrimitive(Class<?> clazz) {
        return clazz.isPrimitive() || clazz == Boolean.class
                || clazz == Byte.class || clazz == Character.class
                || clazz == Short.class || clazz == Integer.class
                || clazz == Long.class || clazz == Float.class
                || clazz == Double.class;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public String getRawType() {
        switch (type) {
            case "java.lang.String":
                return "string";
            case "java.lang.Integer":
                return "Integer";
            case "java.lang.Float":
                return "Float";
            case "java.lang.Long":
                return "Long";
            case "java.lang.Double":
                return "Double";
            case "java.lang.Boolean":
                return "Boolean";
            case "java.lang.Byte":
                return "Byte";
            case "java.lang.Character":
                return "Character";
            case "java.io.File":
                return "file";
            case "io.ultreia.java4all.util.Version":
                return "version";
            case "java.awt.Color":
                return "color";
            case "javax.swing.KeyStroke":
                return "keystroke";
            case "java.util.Locale":
                return "locale";
            case "java.net.URL":
                return "url";
            case "java.util.Date":
                return "date";
            case "java.sql.Time":
                return "time";
            case "java.sql.Timestamp":
                return "timestamp";
            default:
                return type;
        }
    }

    public void setType(String type) {
        if (type.startsWith("gson:")) {
            setUseGson(true);
            type = type.substring(5);
        }
        switch (type) {
            case "string":
                this.type = String.class.getName();
                break;
            case "int":
                this.type = int.class.getName();
                break;
            case "Integer":
                this.type = Integer.class.getName();
                break;
            case "float":
                this.type = float.class.getName();
                break;
            case "Float":
                this.type = Float.class.getName();
                break;
            case "long":
                this.type = long.class.getName();
                break;
            case "Long":
                this.type = Long.class.getName();
                break;
            case "double":
                this.type = double.class.getName();
                break;
            case "Double":
                this.type = Double.class.getName();
                break;
            case "boolean":
                this.type = boolean.class.getName();
                break;
            case "Boolean":
                this.type = Boolean.class.getName();
                break;
            case "byte":
                this.type = byte.class.getName();
                break;
            case "Byte":
                this.type = Byte.class.getName();
                break;
            case "char":
                this.type = char.class.getName();
                break;
            case "Character":
                this.type = Character.class.getName();
                break;
            case "file":
                this.type = File.class.getName();
                break;
            case "version":
                this.type = Version.class.getName();
                break;
            case "color":
                this.type = Color.class.getName();
                break;
            case "keystroke":
                this.type = KeyStroke.class.getName();
                break;
            case "locale":
                this.type = Locale.class.getName();
                break;
            case "url":
                this.type = URL.class.getName();
                break;
            case "date":
                this.type = Date.class.getName();
                break;
            case "time":
                this.type = Time.class.getName();
                break;
            case "timestamp":
                this.type = Timestamp.class.getName();
                break;
            default:
                this.type = type;
                break;
        }
    }

    public void setType(Class<?> type) {
        if (type.equals(String.class)) {
            this.type = "string";
        } else if (type.isPrimitive()) {
            this.type = type.getSimpleName();
        } else if (isPrimitive(type)) {
            this.type = type.getSimpleName();
        } else if (type.equals(File.class)) {
            this.type = "file";
        } else if (type.equals(Version.class)) {
            this.type = "version";
        } else if (type.equals(Color.class)) {
            this.type = "color";
        } else if (type.equals(KeyStroke.class)) {
            this.type = "keystroke";
        } else if (type.equals(URL.class)) {
            this.type = "url";
        } else if (type.equals(Date.class)) {
            this.type = "date";
        } else if (type.equals(Time.class)) {
            this.type = "time";
        } else if (type.equals(Timestamp.class)) {
            this.type = "timestamp";
        } else if (type.isArray()) {
            this.type = type.getComponentType().getName() + "[]";
        } else {
            this.type = type.getName();
        }
    }

    public boolean isUseGson() {
        return useGson;
    }

    public void setUseGson(boolean useGson) {
        this.useGson = useGson;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public Optional<String> getOptionalDefaultValue() {
        return Optional.ofNullable(defaultValue);
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isTransient() {
        return _transient;
    }

    public boolean isFinal() {
        return _final;
    }

    public void setTransient(boolean _transient) {
        this._transient = _transient;
    }

    public void setFinal(boolean _final) {
        this._final = _final;
    }
}
