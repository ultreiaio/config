# Config project

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/config.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22config%22)
![Build Status](https://gitlab.com/ultreiaio/config/badges/develop/pipeline.svg)
[![The GNU General Public License, Version 3.0](https://img.shields.io/badge/license-GPL3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.txt)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/config/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/config)

# Community
* [Contact](mailto:dev@tchemit.fr)
