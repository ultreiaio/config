
# Préambule

Ce rapport décrit les configurations utilisables dans ce projet.

[Pour plus d'informations sur ApplicationConfig](https://ultreiaio.gitlab.io/config/index.html)

| Nom de la configuration | Description | Nombre de variables | Nombre d'options | Nombre d'actions |
| --- | --- | --- | --- | --- |
| [ConfigExampleConfig](#detail_ConfigExampleConfig) | Exemple de configuration | 0 | 6 | 1 |

# Détail des configurations

On détaille ici une par une les configurations définies pour le projet


## Configuration   ConfigExampleConfig

### <a name="detail_ConfigExampleConfig"></a>

Nom de la configuration : **ConfigExampleConfig**


Description : **Exemple de configuration**


### Variables de la configuration

Aucune variable définie pour cette configuration


### Options de la configuration

| Clef | Description | Avec valeur par défault | Type |
| --- | --- | --- | --- |
| [author](#detail_option_author) | Auteur | Oui | ```string``` |
| [identity.age](#detail_option_identity.age) | age de l'utilisateur | Oui | ```int``` |
| [identity.email](#detail_option_identity.email) | Courriel de l'utilisateur | Non | ```string``` |
| [identity.firstName](#detail_option_identity.firstName) | Prénom de l'utilisateur | Oui | ```string``` |
| [identity.lastName](#detail_option_identity.lastName) | Nom de l'utilisateur | Oui | ```string``` |
| [identity.twitter](#detail_option_identity.twitter) | Compte Twitter de l'utilisateur | Oui | ```string``` |


### <a name="detail_option_author"></a>

#### Option author

| Champs | Valeur |
| --- | --- |
| Clef | author |
| Description | Auteur |
| Type | ```string``` |
| Valeur par défaut | ```Tony Chemit (ultreiaio)``` |


### <a name="detail_option_identity.age"></a>

#### Option identity.age

| Champs | Valeur |
| --- | --- |
| Clef | identity.age |
| Description | age de l'utilisateur |
| Type | ```int``` |
| Valeur par défaut | ```56``` |


### <a name="detail_option_identity.email"></a>

#### Option identity.email

| Champs | Valeur |
| --- | --- |
| Clef | identity.email |
| Description | Courriel de l'utilisateur |
| Type | ```string``` |
| Valeur par défaut | ```Pas de valeur par défaut``` |


### <a name="detail_option_identity.firstName"></a>

#### Option identity.firstName

| Champs | Valeur |
| --- | --- |
| Clef | identity.firstName |
| Description | Prénom de l'utilisateur |
| Type | ```string``` |
| Valeur par défaut | ```Joshua``` |


### <a name="detail_option_identity.lastName"></a>

#### Option identity.lastName

| Champs | Valeur |
| --- | --- |
| Clef | identity.lastName |
| Description | Nom de l'utilisateur |
| Type | ```string``` |
| Valeur par défaut | ```Bloch``` |


### <a name="detail_option_identity.twitter"></a>

#### Option identity.twitter

| Champs | Valeur |
| --- | --- |
| Clef | identity.twitter |
| Description | Compte Twitter de l'utilisateur |
| Type | ```string``` |
| Valeur par défaut | ```jbloch``` |



### Actions de la configuration

| Description | Aliases | Action |
| --- | --- | --- |
| Pour afficher l'aide | -h,--help | io.ultreia.java4all.config.example.ConfigExample#help |

